package com.hipool.models;


import com.google.gson.annotations.SerializedName;


public class WalletHistory {

	@SerializedName("credit_history")
	private Double creditHistory;

	@SerializedName("orderId")
	private String orderId;

	@SerializedName("created_date")
	private String createdDate;

	@SerializedName("coming_from")
	private int comingFrom;

	@SerializedName("history_id")
	private int historyId;

	@SerializedName("cust_id")
	private int custId;

	@SerializedName("status")
	private int status;

	public void setCreditHistory(Double creditHistory){
		this.creditHistory = creditHistory;
	}

	public Double getCreditHistory(){
		return creditHistory;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		return orderId;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setComingFrom(int comingFrom){
		this.comingFrom = comingFrom;
	}

	public int getComingFrom(){
		return comingFrom;
	}

	public void setHistoryId(int historyId){
		this.historyId = historyId;
	}

	public int getHistoryId(){
		return historyId;
	}

	public void setCustId(int custId){
		this.custId = custId;
	}

	public int getCustId(){
		return custId;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"WalletHistory{" +
			"credit_history = '" + creditHistory + '\'' + 
			",orderId = '" + orderId + '\'' + 
			",created_date = '" + createdDate + '\'' + 
			",coming_from = '" + comingFrom + '\'' + 
			",history_id = '" + historyId + '\'' + 
			",cust_id = '" + custId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}