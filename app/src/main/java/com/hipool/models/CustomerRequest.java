package com.hipool.models;

import com.google.gson.annotations.SerializedName;

public class CustomerRequest{

	@SerializedName("current_lat")
	private String currentLat;

	@SerializedName("drop_address")
	private String dropAddress;

	@SerializedName("registartion_copy")
	private String registartionCopy;

	@SerializedName("cust_address_copy")
	private String custAddressCopy;

	@SerializedName("cust_gender")
	private int custGender;

	@SerializedName("password")
	private String password;

	@SerializedName("cust_type")
	private int custType;

	@SerializedName("total_distance")
	private int totalDistance;

	@SerializedName("estimated_fare")
	private double estimatedFare;

	@SerializedName("id")
	private int id;

	@SerializedName("cust_emergency_contact_2")
	private String custEmergencyContact2;

	@SerializedName("cust_emergency_contact_1")
	private String custEmergencyContact1;

	@SerializedName("cust_emergency_contact_4")
	private String custEmergencyContact4;

	@SerializedName("cust_emergency_contact_3")
	private String custEmergencyContact3;

	@SerializedName("customer_device_id")
	private String customerDeviceId;

	@SerializedName("customer_socket_id")
	private String customerSocketId;

	@SerializedName("estimated_tax")
	private Double estimatedTax;

	@SerializedName("otp")
	private int otp;

	@SerializedName("login_status")
	private int loginStatus;

	@SerializedName("cust_profile_pic")
	private String custProfilePic;

	@SerializedName("tax_on_coupon")
	private int taxOnCoupon;

	@SerializedName("pan_number")
	private String panNumber;

	@SerializedName("cust_email")
	private String custEmail;

	@SerializedName("cust_id")
	private int custId;

	@SerializedName("status")
	private int status;

	@SerializedName("estimated_time")
	private String estimatedTime;

	@SerializedName("coupon_applied")
	private int couponApplied;

	@SerializedName("drop_lon")
	private String dropLon;

	@SerializedName("cust_ride_status")
	private int custRideStatus;

	@SerializedName("verification_status")
	private int verificationStatus;

	@SerializedName("cust_dob")
	private String custDob;

	@SerializedName("vehicle_copy")
	private String vehicleCopy;

	@SerializedName("cust_status")
	private int custStatus;

	@SerializedName("current_lon")
	private String currentLon;

	@SerializedName("pick_lon")
	private String pickLon;

	@SerializedName("cust_pan_copy")
	private String custPanCopy;

	@SerializedName("coupon_amount")
	private int couponAmount;

	@SerializedName("cust_contact_no")
	private String custContactNo;

	@SerializedName("dl_no")
	private String dlNo;

	@SerializedName("dl_copy")
	private String dlCopy;

	@SerializedName("cust_first_name")
	private String custFirstName;

	@SerializedName("pick_lat")
	private String pickLat;

	@SerializedName("drop_lat")
	private String dropLat;

	@SerializedName("customer_status")
	private int customerStatus;

	@SerializedName("customer_access_token")
	private String customerAccessToken;

	@SerializedName("pick_address")
	private String pickAddress;

	@SerializedName("cust_alternate_no")
	private String custAlternateNo;

	@SerializedName("created_date")
	private String createdDate;

	@SerializedName("updated_date")
	private String updatedDate;

	@SerializedName("cust_last_name")
	private String custLastName;

	@SerializedName("insurance_copy")
	private String insuranceCopy;

	@SerializedName("cust_city")
	private String custCity;

	public void setCurrentLat(String currentLat){
		this.currentLat = currentLat;
	}

	public String getCurrentLat(){
		return currentLat;
	}

	public void setDropAddress(String dropAddress){
		this.dropAddress = dropAddress;
	}

	public String getDropAddress(){
		return dropAddress;
	}

	public void setRegistartionCopy(String registartionCopy){
		this.registartionCopy = registartionCopy;
	}

	public String getRegistartionCopy(){
		return registartionCopy;
	}

	public void setCustAddressCopy(String custAddressCopy){
		this.custAddressCopy = custAddressCopy;
	}

	public String getCustAddressCopy(){
		return custAddressCopy;
	}

	public void setCustGender(int custGender){
		this.custGender = custGender;
	}

	public int getCustGender(){
		return custGender;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setCustType(int custType){
		this.custType = custType;
	}

	public int getCustType(){
		return custType;
	}

	public void setTotalDistance(int totalDistance){
		this.totalDistance = totalDistance;
	}

	public int getTotalDistance(){
		return totalDistance;
	}

	public void setEstimatedFare(double estimatedFare){
		this.estimatedFare = estimatedFare;
	}

	public double getEstimatedFare(){
		return estimatedFare;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCustEmergencyContact2(String custEmergencyContact2){
		this.custEmergencyContact2 = custEmergencyContact2;
	}

	public String getCustEmergencyContact2(){
		return custEmergencyContact2;
	}

	public void setCustEmergencyContact1(String custEmergencyContact1){
		this.custEmergencyContact1 = custEmergencyContact1;
	}

	public String getCustEmergencyContact1(){
		return custEmergencyContact1;
	}

	public void setCustEmergencyContact4(String custEmergencyContact4){
		this.custEmergencyContact4 = custEmergencyContact4;
	}

	public String getCustEmergencyContact4(){
		return custEmergencyContact4;
	}

	public void setCustEmergencyContact3(String custEmergencyContact3){
		this.custEmergencyContact3 = custEmergencyContact3;
	}

	public String getCustEmergencyContact3(){
		return custEmergencyContact3;
	}

	public void setCustomerDeviceId(String customerDeviceId){
		this.customerDeviceId = customerDeviceId;
	}

	public String getCustomerDeviceId(){
		return customerDeviceId;
	}

	public void setCustomerSocketId(String customerSocketId){
		this.customerSocketId = customerSocketId;
	}

	public String getCustomerSocketId(){
		return customerSocketId;
	}

	public void setEstimatedTax(Double estimatedTax){
		this.estimatedTax = estimatedTax;
	}

	public Double getEstimatedTax(){
		return estimatedTax;
	}

	public void setOtp(int otp){
		this.otp = otp;
	}

	public int getOtp(){
		return otp;
	}

	public void setLoginStatus(int loginStatus){
		this.loginStatus = loginStatus;
	}

	public int getLoginStatus(){
		return loginStatus;
	}

	public void setCustProfilePic(String custProfilePic){
		this.custProfilePic = custProfilePic;
	}

	public String getCustProfilePic(){
		return custProfilePic;
	}

	public void setTaxOnCoupon(int taxOnCoupon){
		this.taxOnCoupon = taxOnCoupon;
	}

	public int getTaxOnCoupon(){
		return taxOnCoupon;
	}

	public void setPanNumber(String panNumber){
		this.panNumber = panNumber;
	}

	public String getPanNumber(){
		return panNumber;
	}

	public void setCustEmail(String custEmail){
		this.custEmail = custEmail;
	}

	public String getCustEmail(){
		return custEmail;
	}

	public void setCustId(int custId){
		this.custId = custId;
	}

	public int getCustId(){
		return custId;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public void setEstimatedTime(String estimatedTime){
		this.estimatedTime = estimatedTime;
	}

	public String getEstimatedTime(){
		return estimatedTime;
	}

	public void setCouponApplied(int couponApplied){
		this.couponApplied = couponApplied;
	}

	public int getCouponApplied(){
		return couponApplied;
	}

	public void setDropLon(String dropLon){
		this.dropLon = dropLon;
	}

	public String getDropLon(){
		return dropLon;
	}

	public void setCustRideStatus(int custRideStatus){
		this.custRideStatus = custRideStatus;
	}

	public int getCustRideStatus(){
		return custRideStatus;
	}

	public void setVerificationStatus(int verificationStatus){
		this.verificationStatus = verificationStatus;
	}

	public int getVerificationStatus(){
		return verificationStatus;
	}

	public void setCustDob(String custDob){
		this.custDob = custDob;
	}

	public String getCustDob(){
		return custDob;
	}

	public void setVehicleCopy(String vehicleCopy){
		this.vehicleCopy = vehicleCopy;
	}

	public String getVehicleCopy(){
		return vehicleCopy;
	}

	public void setCustStatus(int custStatus){
		this.custStatus = custStatus;
	}

	public int getCustStatus(){
		return custStatus;
	}

	public void setCurrentLon(String currentLon){
		this.currentLon = currentLon;
	}

	public String getCurrentLon(){
		return currentLon;
	}

	public void setPickLon(String pickLon){
		this.pickLon = pickLon;
	}

	public String getPickLon(){
		return pickLon;
	}

	public void setCustPanCopy(String custPanCopy){
		this.custPanCopy = custPanCopy;
	}

	public String getCustPanCopy(){
		return custPanCopy;
	}

	public void setCouponAmount(int couponAmount){
		this.couponAmount = couponAmount;
	}

	public int getCouponAmount(){
		return couponAmount;
	}

	public void setCustContactNo(String custContactNo){
		this.custContactNo = custContactNo;
	}

	public String getCustContactNo(){
		return custContactNo;
	}

	public void setDlNo(String dlNo){
		this.dlNo = dlNo;
	}

	public String getDlNo(){
		return dlNo;
	}

	public void setDlCopy(String dlCopy){
		this.dlCopy = dlCopy;
	}

	public String getDlCopy(){
		return dlCopy;
	}

	public void setCustFirstName(String custFirstName){
		this.custFirstName = custFirstName;
	}

	public String getCustFirstName(){
		return custFirstName;
	}

	public void setPickLat(String pickLat){
		this.pickLat = pickLat;
	}

	public String getPickLat(){
		return pickLat;
	}

	public void setDropLat(String dropLat){
		this.dropLat = dropLat;
	}

	public String getDropLat(){
		return dropLat;
	}

	public void setCustomerStatus(int customerStatus){
		this.customerStatus = customerStatus;
	}

	public int getCustomerStatus(){
		return customerStatus;
	}

	public void setCustomerAccessToken(String customerAccessToken){
		this.customerAccessToken = customerAccessToken;
	}

	public String getCustomerAccessToken(){
		return customerAccessToken;
	}

	public void setPickAddress(String pickAddress){
		this.pickAddress = pickAddress;
	}

	public String getPickAddress(){
		return pickAddress;
	}

	public void setCustAlternateNo(String custAlternateNo){
		this.custAlternateNo = custAlternateNo;
	}

	public String getCustAlternateNo(){
		return custAlternateNo;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setUpdatedDate(String updatedDate){
		this.updatedDate = updatedDate;
	}

	public String getUpdatedDate(){
		return updatedDate;
	}

	public void setCustLastName(String custLastName){
		this.custLastName = custLastName;
	}

	public String getCustLastName(){
		return custLastName;
	}

	public void setInsuranceCopy(String insuranceCopy){
		this.insuranceCopy = insuranceCopy;
	}

	public String getInsuranceCopy(){
		return insuranceCopy;
	}

	public void setCustCity(String custCity){
		this.custCity = custCity;
	}

	public String getCustCity(){
		return custCity;
	}

	@Override
 	public String toString(){
		return 
			"CustomerRequest{" + 
			"current_lat = '" + currentLat + '\'' + 
			",drop_address = '" + dropAddress + '\'' + 
			",registartion_copy = '" + registartionCopy + '\'' + 
			",cust_address_copy = '" + custAddressCopy + '\'' + 
			",cust_gender = '" + custGender + '\'' + 
			",password = '" + password + '\'' + 
			",cust_type = '" + custType + '\'' + 
			",total_distance = '" + totalDistance + '\'' + 
			",estimated_fare = '" + estimatedFare + '\'' + 
			",id = '" + id + '\'' + 
			",cust_emergency_contact_2 = '" + custEmergencyContact2 + '\'' + 
			",cust_emergency_contact_1 = '" + custEmergencyContact1 + '\'' + 
			",cust_emergency_contact_4 = '" + custEmergencyContact4 + '\'' + 
			",cust_emergency_contact_3 = '" + custEmergencyContact3 + '\'' + 
			",customer_device_id = '" + customerDeviceId + '\'' + 
			",customer_socket_id = '" + customerSocketId + '\'' + 
			",estimated_tax = '" + estimatedTax + '\'' + 
			",otp = '" + otp + '\'' + 
			",login_status = '" + loginStatus + '\'' + 
			",cust_profile_pic = '" + custProfilePic + '\'' + 
			",tax_on_coupon = '" + taxOnCoupon + '\'' + 
			",pan_number = '" + panNumber + '\'' + 
			",cust_email = '" + custEmail + '\'' + 
			",cust_id = '" + custId + '\'' + 
			",status = '" + status + '\'' + 
			",estimated_time = '" + estimatedTime + '\'' + 
			",coupon_applied = '" + couponApplied + '\'' + 
			",drop_lon = '" + dropLon + '\'' + 
			",cust_ride_status = '" + custRideStatus + '\'' + 
			",verification_status = '" + verificationStatus + '\'' + 
			",cust_dob = '" + custDob + '\'' + 
			",vehicle_copy = '" + vehicleCopy + '\'' + 
			",cust_status = '" + custStatus + '\'' + 
			",current_lon = '" + currentLon + '\'' + 
			",pick_lon = '" + pickLon + '\'' + 
			",cust_pan_copy = '" + custPanCopy + '\'' + 
			",coupon_amount = '" + couponAmount + '\'' + 
			",cust_contact_no = '" + custContactNo + '\'' + 
			",dl_no = '" + dlNo + '\'' + 
			",dl_copy = '" + dlCopy + '\'' + 
			",cust_first_name = '" + custFirstName + '\'' + 
			",pick_lat = '" + pickLat + '\'' + 
			",drop_lat = '" + dropLat + '\'' + 
			",customer_status = '" + customerStatus + '\'' + 
			",customer_access_token = '" + customerAccessToken + '\'' + 
			",pick_address = '" + pickAddress + '\'' + 
			",cust_alternate_no = '" + custAlternateNo + '\'' + 
			",created_date = '" + createdDate + '\'' + 
			",updated_date = '" + updatedDate + '\'' + 
			",cust_last_name = '" + custLastName + '\'' + 
			",insurance_copy = '" + insuranceCopy + '\'' + 
			",cust_city = '" + custCity + '\'' + 
			"}";
		}
}