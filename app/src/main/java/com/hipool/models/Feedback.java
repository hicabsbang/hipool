package com.hipool.models;

import com.google.gson.annotations.SerializedName;

public class Feedback{

	@SerializedName("feedback")
	private String feedback;

	@SerializedName("created")
	private String created;

	@SerializedName("feedback_id")
	private int feedbackId;

	@SerializedName("feedback_status")
	private int feedbackStatus;

	@SerializedName("updated")
	private String updated;

	public void setFeedback(String feedback){
		this.feedback = feedback;
	}

	public String getFeedback(){
		return feedback;
	}

	public void setCreated(String created){
		this.created = created;
	}

	public String getCreated(){
		return created;
	}

	public void setFeedbackId(int feedbackId){
		this.feedbackId = feedbackId;
	}

	public int getFeedbackId(){
		return feedbackId;
	}

	public void setFeedbackStatus(int feedbackStatus){
		this.feedbackStatus = feedbackStatus;
	}

	public int getFeedbackStatus(){
		return feedbackStatus;
	}

	public void setUpdated(String updated){
		this.updated = updated;
	}

	public String getUpdated(){
		return updated;
	}

	@Override
 	public String toString(){
		return 
			"Feedback{" + 
			"feedback = '" + feedback + '\'' + 
			",created = '" + created + '\'' + 
			",feedback_id = '" + feedbackId + '\'' + 
			",feedback_status = '" + feedbackStatus + '\'' + 
			",updated = '" + updated + '\'' + 
			"}";
		}
}