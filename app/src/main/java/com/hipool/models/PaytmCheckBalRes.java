package com.hipool.models;

/**
 * Created by Ashutosh on 19-05-2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaytmCheckBalRes {

    @SerializedName("type")
    @Expose
    private Object type;
    @SerializedName("requestGuid")
    @Expose
    private Object requestGuid;
    @SerializedName("orderId")
    @Expose
    private Object orderId;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("response")
    @Expose
    private ResponseCheckBalPaytm response;
    @SerializedName("metadata")
    @Expose
    private Object metadata;

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Object getRequestGuid() {
        return requestGuid;
    }

    public void setRequestGuid(Object requestGuid) {
        this.requestGuid = requestGuid;
    }

    public Object getOrderId() {
        return orderId;
    }

    public void setOrderId(Object orderId) {
        this.orderId = orderId;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ResponseCheckBalPaytm getResponse() {
        return response;
    }

    public void setResponse(ResponseCheckBalPaytm response) {
        this.response = response;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

}