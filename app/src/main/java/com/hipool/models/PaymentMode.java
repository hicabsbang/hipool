package com.hipool.models;

/**
 * Created by Ashutosh on 31-05-2017.
 */

public class PaymentMode {

    private int id;
    private String name;
    private int imgResource;

    public PaymentMode(int paymentModeId, String paymentModeName, int paymentModeImgResource) {
        this.id = paymentModeId;
        this.name = paymentModeName;
        this.imgResource = paymentModeImgResource;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImgResource() {
        return imgResource;
    }

    public void setImgResource(int imgResource) {
        this.imgResource = imgResource;
    }
}
