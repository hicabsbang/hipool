package com.hipool.models;

/**
 * Created by ashut on 12-10-2017.
 */

public class SupportItem {
    private String item;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}