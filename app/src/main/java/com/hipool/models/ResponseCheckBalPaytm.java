package com.hipool.models;

/**
 * Created by Ashutosh on 19-05-2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseCheckBalPaytm {

    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("currencyCode")
    @Expose
    private String currencyCode;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ownerGuid")
    @Expose
    private String ownerGuid;
    @SerializedName("walletType")
    @Expose
    private String walletType;
    @SerializedName("ssoId")
    @Expose
    private String ssoId;
    @SerializedName("txnPinRequired")
    @Expose
    private Boolean txnPinRequired;
    @SerializedName("otpPinRequired")
    @Expose
    private Boolean otpPinRequired;
    @SerializedName("walletGrade")
    @Expose
    private String walletGrade;
    @SerializedName("clwBalance")
    @Expose
    private Object clwBalance;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOwnerGuid() {
        return ownerGuid;
    }

    public void setOwnerGuid(String ownerGuid) {
        this.ownerGuid = ownerGuid;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getSsoId() {
        return ssoId;
    }

    public void setSsoId(String ssoId) {
        this.ssoId = ssoId;
    }

    public Boolean getTxnPinRequired() {
        return txnPinRequired;
    }

    public void setTxnPinRequired(Boolean txnPinRequired) {
        this.txnPinRequired = txnPinRequired;
    }

    public Boolean getOtpPinRequired() {
        return otpPinRequired;
    }

    public void setOtpPinRequired(Boolean otpPinRequired) {
        this.otpPinRequired = otpPinRequired;
    }

    public String getWalletGrade() {
        return walletGrade;
    }

    public void setWalletGrade(String walletGrade) {
        this.walletGrade = walletGrade;
    }

    public Object getClwBalance() {
        return clwBalance;
    }

    public void setClwBalance(Object clwBalance) {
        this.clwBalance = clwBalance;
    }

}