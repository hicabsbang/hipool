package com.hipool.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhinav on 18/09/2017.
 */

public class CustomerDetails {

    @SerializedName("cust_ride_status")
    private int custRideStatus;

    @SerializedName("verification_status")
    private int verificationStatus;

    @SerializedName("registartion_copy")
    private String registartionCopy;

    @SerializedName("cust_address_copy")
    private String custAddressCopy;

    @SerializedName("address_type")
    private int addressType;

    @SerializedName("cust_gender")
    private int custGender;

    @SerializedName("cust_dob")
    private String custDob;

    @SerializedName("password")
    private String password;

    @SerializedName("vehicle_copy")
    private String vehicleCopy;

    @SerializedName("cust_type")
    private int custType;

    @SerializedName("cust_status")
    private int custStatus;

    @SerializedName("cust_emergency_contact_2")
    private Object custEmergencyContact2;

    @SerializedName("cust_emergency_contact_1")
    private Object custEmergencyContact1;

    @SerializedName("cust_emergency_contact_4")
    private Object custEmergencyContact4;

    @SerializedName("cust_emergency_contact_3")
    private Object custEmergencyContact3;

    @SerializedName("cust_pan_copy")
    private String custPanCopy;

    @SerializedName("customer_device_id")
    private String customerDeviceId;

    @SerializedName("customer_socket_id")
    private String customerSocketId;

    @SerializedName("cust_contact_no")
    private String custContactNo;

    @SerializedName("otp")
    private int otp;

    @SerializedName("dl_no")
    private String dlNo;

    @SerializedName("dl_copy")
    private String dlCopy;

    @SerializedName("login_status")
    private int loginStatus;

    @SerializedName("cust_first_name")
    private String custFirstName;

    @SerializedName("cust_profile_pic")
    private String custProfilePic;

    @SerializedName("pan_number")
    private String panNumber;

    @SerializedName("cust_email")
    private String custEmail;

    @SerializedName("customer_status")
    private int customerStatus;

    @SerializedName("customer_access_token")
    private String customerAccessToken;

    @SerializedName("cust_alternate_no")
    private Object custAlternateNo;

    @SerializedName("created_date")
    private String createdDate;

    @SerializedName("updated_date")
    private String updatedDate;

    @SerializedName("cust_last_name")
    private Object custLastName;

    @SerializedName("insurance_copy")
    private String insuranceCopy;

    @SerializedName("cust_id")
    private int custId;

    @SerializedName("cust_city")
    private Object custCity;

    public void setCustRideStatus(int custRideStatus){
        this.custRideStatus = custRideStatus;
    }

    public int getCustRideStatus(){
        return custRideStatus;
    }

    public void setVerificationStatus(int verificationStatus){
        this.verificationStatus = verificationStatus;
    }

    public int getVerificationStatus(){
        return verificationStatus;
    }

    public void setRegistartionCopy(String registartionCopy){
        this.registartionCopy = registartionCopy;
    }

    public String getRegistartionCopy(){
        return registartionCopy;
    }

    public void setCustAddressCopy(String custAddressCopy){
        this.custAddressCopy = custAddressCopy;
    }

    public String getCustAddressCopy(){
        return custAddressCopy;
    }

    public void setCustGender(int custGender){
        this.custGender = custGender;
    }

    public int getCustGender(){
        return custGender;
    }

    public void setCustDob(String custDob){
        this.custDob = custDob;
    }

    public String getCustDob(){
        return custDob;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public String getPassword(){
        return password;
    }

    public void setVehicleCopy(String vehicleCopy){
        this.vehicleCopy = vehicleCopy;
    }

    public String getVehicleCopy(){
        return vehicleCopy;
    }

    public void setCustType(int custType){
        this.custType = custType;
    }

    public int getCustType(){
        return custType;
    }

    public void setCustStatus(int custStatus){
        this.custStatus = custStatus;
    }

    public int getCustStatus(){
        return custStatus;
    }

    public void setCustEmergencyContact2(Object custEmergencyContact2){
        this.custEmergencyContact2 = custEmergencyContact2;
    }

    public Object getCustEmergencyContact2(){
        return custEmergencyContact2;
    }

    public void setCustEmergencyContact1(Object custEmergencyContact1){
        this.custEmergencyContact1 = custEmergencyContact1;
    }

    public Object getCustEmergencyContact1(){
        return custEmergencyContact1;
    }

    public void setCustEmergencyContact4(Object custEmergencyContact4){
        this.custEmergencyContact4 = custEmergencyContact4;
    }

    public Object getCustEmergencyContact4(){
        return custEmergencyContact4;
    }

    public void setCustEmergencyContact3(Object custEmergencyContact3){
        this.custEmergencyContact3 = custEmergencyContact3;
    }

    public Object getCustEmergencyContact3(){
        return custEmergencyContact3;
    }

    public void setCustPanCopy(String custPanCopy){
        this.custPanCopy = custPanCopy;
    }

    public String getCustPanCopy(){
        return custPanCopy;
    }

    public void setCustomerDeviceId(String customerDeviceId){
        this.customerDeviceId = customerDeviceId;
    }

    public String getCustomerDeviceId(){
        return customerDeviceId;
    }

    public void setCustomerSocketId(String customerSocketId){
        this.customerSocketId = customerSocketId;
    }

    public String getCustomerSocketId(){
        return customerSocketId;
    }

    public void setCustContactNo(String custContactNo){
        this.custContactNo = custContactNo;
    }

    public String getCustContactNo(){
        return custContactNo;
    }

    public void setOtp(int otp){
        this.otp = otp;
    }

    public int getOtp(){
        return otp;
    }

    public void setDlNo(String dlNo){
        this.dlNo = dlNo;
    }

    public String getDlNo(){
        return dlNo;
    }

    public void setDlCopy(String dlCopy){
        this.dlCopy = dlCopy;
    }

    public String getDlCopy(){
        return dlCopy;
    }

    public void setLoginStatus(int loginStatus){
        this.loginStatus = loginStatus;
    }

    public int getLoginStatus(){
        return loginStatus;
    }

    public void setCustFirstName(String custFirstName){
        this.custFirstName = custFirstName;
    }

    public String getCustFirstName(){
        return custFirstName;
    }

    public void setCustProfilePic(String custProfilePic){
        this.custProfilePic = custProfilePic;
    }

    public String getCustProfilePic(){
        return custProfilePic;
    }

    public void setPanNumber(String panNumber){
        this.panNumber = panNumber;
    }

    public String getPanNumber(){
        return panNumber;
    }

    public void setCustEmail(String custEmail){
        this.custEmail = custEmail;
    }

    public String getCustEmail(){
        return custEmail;
    }

    public void setCustomerStatus(int customerStatus){
        this.customerStatus = customerStatus;
    }

    public int getCustomerStatus(){
        return customerStatus;
    }

    public void setCustomerAccessToken(String customerAccessToken){
        this.customerAccessToken = customerAccessToken;
    }

    public String getCustomerAccessToken(){
        return customerAccessToken;
    }

    public void setCustAlternateNo(Object custAlternateNo){
        this.custAlternateNo = custAlternateNo;
    }

    public Object getCustAlternateNo(){
        return custAlternateNo;
    }

    public void setCreatedDate(String createdDate){
        this.createdDate = createdDate;
    }

    public String getCreatedDate(){
        return createdDate;
    }

    public void setUpdatedDate(String updatedDate){
        this.updatedDate = updatedDate;
    }

    public String getUpdatedDate(){
        return updatedDate;
    }

    public void setCustLastName(Object custLastName){
        this.custLastName = custLastName;
    }

    public Object getCustLastName(){
        return custLastName;
    }

    public void setInsuranceCopy(String insuranceCopy){
        this.insuranceCopy = insuranceCopy;
    }

    public String getInsuranceCopy(){
        return insuranceCopy;
    }

    public void setCustId(int custId){
        this.custId = custId;
    }

    public int getCustId(){
        return custId;
    }

    public void setCustCity(Object custCity){
        this.custCity = custCity;
    }

    public Object getCustCity(){
        return custCity;
    }

    public int getAddressType() {
        return addressType;
    }

    public void setAddressType(int addressType) {
        this.addressType = addressType;
    }

    @Override
    public String toString(){
        return
                "DriverRequestList{" +
                        "cust_ride_status = '" + custRideStatus + '\'' +
                        ",verification_status = '" + verificationStatus + '\'' +
                        ",registartion_copy = '" + registartionCopy + '\'' +
                        ",cust_address_copy = '" + custAddressCopy + '\'' +
                        ",cust_gender = '" + custGender + '\'' +
                        ",cust_dob = '" + custDob + '\'' +
                        ",password = '" + password + '\'' +
                        ",vehicle_copy = '" + vehicleCopy + '\'' +
                        ",cust_type = '" + custType + '\'' +
                        ",cust_status = '" + custStatus + '\'' +
                        ",cust_emergency_contact_2 = '" + custEmergencyContact2 + '\'' +
                        ",cust_emergency_contact_1 = '" + custEmergencyContact1 + '\'' +
                        ",cust_emergency_contact_4 = '" + custEmergencyContact4 + '\'' +
                        ",cust_emergency_contact_3 = '" + custEmergencyContact3 + '\'' +
                        ",cust_pan_copy = '" + custPanCopy + '\'' +
                        ",customer_device_id = '" + customerDeviceId + '\'' +
                        ",customer_socket_id = '" + customerSocketId + '\'' +
                        ",cust_contact_no = '" + custContactNo + '\'' +
                        ",otp = '" + otp + '\'' +
                        ",dl_no = '" + dlNo + '\'' +
                        ",dl_copy = '" + dlCopy + '\'' +
                        ",login_status = '" + loginStatus + '\'' +
                        ",cust_first_name = '" + custFirstName + '\'' +
                        ",cust_profile_pic = '" + custProfilePic + '\'' +
                        ",pan_number = '" + panNumber + '\'' +
                        ",cust_email = '" + custEmail + '\'' +
                        ",customer_status = '" + customerStatus + '\'' +
                        ",customer_access_token = '" + customerAccessToken + '\'' +
                        ",cust_alternate_no = '" + custAlternateNo + '\'' +
                        ",created_date = '" + createdDate + '\'' +
                        ",updated_date = '" + updatedDate + '\'' +
                        ",cust_last_name = '" + custLastName + '\'' +
                        ",insurance_copy = '" + insuranceCopy + '\'' +
                        ",cust_id = '" + custId + '\'' +
                        ",cust_city = '" + custCity + '\'' +
                        "}";
    }
}