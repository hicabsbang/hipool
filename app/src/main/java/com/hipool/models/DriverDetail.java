package com.hipool.models;


import com.google.gson.annotations.SerializedName;

public class DriverDetail{

	@SerializedName("cust_pick_location_lat")
	private double custPickLocationLat;

	@SerializedName("start_index")
	private int startIndex;

	@SerializedName("end_index")
	private int endIndex;

	@SerializedName("otp")
	private String custOtp;

	@SerializedName("total_distance")
	private String custTotalDistance;

	@SerializedName("total_time")
	private String custTotalTime;

	@SerializedName("total_fare")
	private Double custTotalFare;

    @SerializedName("total_tax")
    private Double custTotalTAX;

	@SerializedName("current_lat")
	private String currentLat;

	@SerializedName("vichile_no")
	private String bikeNo;

	@SerializedName("starttime")
	private String starttime;

	@SerializedName("registartion_copy")
	private String registartionCopy;

	@SerializedName("cust_drop_location")
	private String custDropLocation;

	@SerializedName("cust_address_copy")
	private String custAddressCopy;

	@SerializedName("cust_gender")
	private int custGender;

	@SerializedName("password")
	private String password;

	@SerializedName("cust_type")
	private int custType;

	@SerializedName("driver_drop_lon")
	private double driverDropLon;

	@SerializedName("driver_pick_lon")
	private double driverPickLon;

	@SerializedName("id")
	private int id;

	@SerializedName("request_id")
	private int cust_request_id;

	@SerializedName("cust_emergency_contact_2")
	private Object custEmergencyContact2;

	@SerializedName("cust_emergency_contact_1")
	private Object custEmergencyContact1;

	@SerializedName("cust_emergency_contact_4")
	private Object custEmergencyContact4;

	@SerializedName("cust_emergency_contact_3")
	private Object custEmergencyContact3;

	@SerializedName("customer_device_id")
	private String customerDeviceId;

	@SerializedName("customer_socket_id")
	private String customerSocketId;



	@SerializedName("driver_drop_lat")
	private double driverDropLat;

	@SerializedName("driver_pick_lat")
	private double driverPickLat;

	@SerializedName("login_status")
	private int loginStatus;

	@SerializedName("cust_profile_pic")
	private String custProfilePic;

	@SerializedName("pan_number")
	private String panNumber;

	@SerializedName("cust_pick_location_lon")
	private double custPickLocationLon;

	@SerializedName("cust_email")
	private String custEmail;

	@SerializedName("cust_drop_location_lat")
	private double custDropLocationLat;

	@SerializedName("cust_id")
	private int custId;

	@SerializedName("status")
	private int status;

	@SerializedName("drop_lon")
	private String dropLon;

	@SerializedName("cust_ride_status")
	private int custRideStatus;

	@SerializedName("verification_status")
	private int verificationStatus;

	@SerializedName("cust_dob")
	private String custDob;

	@SerializedName("vehicle_copy")
	private String vehicleCopy;

	@SerializedName("full_route")
	private String fullRoute;

	@SerializedName("cust_status")
	private int custStatus;

	@SerializedName("time_status")
	private int timeStatus;

	@SerializedName("current_lon")
	private String currentLon;

	@SerializedName("pick_lon")
	private String pickLon;

	@SerializedName("cust_pan_copy")
	private String custPanCopy;

	@SerializedName("cust_contact_no")
	private String custContactNo;

	@SerializedName("cust_drop_location_lon")
	private double custDropLocationLon;

	@SerializedName("dl_no")
	private String dlNo;

	@SerializedName("dl_copy")
	private String dlCopy;

	@SerializedName("cust_first_name")
	private String custFirstName;

	@SerializedName("cust_pick_location")
	private String custPickLocation;

	@SerializedName("pick_lat")
	private String pickLat;

	@SerializedName("drop_lat")
	private String dropLat;

	@SerializedName("customer_status")
	private int customerStatus;

	@SerializedName("customer_access_token")
	private String customerAccessToken;

	@SerializedName("cust_alternate_no")
	private Object custAlternateNo;

	@SerializedName("created_date")
	private String createdDate;

	@SerializedName("updated_date")
	private String updatedDate;

	@SerializedName("cust_last_name")
	private Object custLastName;

	@SerializedName("insurance_copy")
	private String insuranceCopy;

	@SerializedName("cust_city")
	private Object custCity;

	@SerializedName("reach_time")
	private Double reachTime;

	@SerializedName("vehicle_name")
	private String vehicle_name;

	public void setStartIndex(int startIndex)
	{
		this.startIndex=startIndex;
	}
	public int getStartIndex()
	{
		return startIndex;
	}
	public void setEndIndex(int endIndex)
	{
		this.endIndex=endIndex;
	}
	public int getEndIndex()
	{
		return endIndex;
	}

	public void setVehicle_name(String vehicle_name){this.vehicle_name=vehicle_name;}
	public String getVehicle_name(){return vehicle_name;}

	public void setCustPickLocationLat(double custPickLocationLat){
		this.custPickLocationLat = custPickLocationLat;
	}

	public void setBikeNo(String bikeNo)
	{
		this.bikeNo=bikeNo;
	}
	public String getBikeNo()
	{
		return bikeNo;
	}

	public void setCustOtp(String custOtp1)
	{
		this.custOtp=custOtp1;
	}
	public String getCustOtp(){return  custOtp;}

	public double getCustPickLocationLat(){
		return custPickLocationLat;
	}

	public void setCustTotalTime(String custTotalTime){
		this.custTotalTime = custTotalTime;
	}

	public String getCustTotalTime(){
		return custTotalTime;
	}

	public void setCustTotalFare(Double custTotalFare){
		this.custTotalFare = custTotalFare;
	}

	public Double getCustTotalFare(){
		return custTotalFare;
	}

    public void setCustTotalTAX (Double custTotalTAX){
        this.custTotalTAX = custTotalTAX;
    }

    public Double getCustTotalTAX(){
        return custTotalTAX;
    }

	public void setReachTime (Double reachTime){
		this.reachTime = reachTime;
	}

	public Double getReachTime(){
		return reachTime;
	}


    public void setCustTotalDistance(String custTotalDistance){
		this.custTotalDistance = custTotalDistance;
	}

	public String getCustTotalDistance(){
		return custTotalDistance;
	}


	public void setCurrentLat(String currentLat){
		this.currentLat = currentLat;
	}

	public String getCurrentLat(){
		return currentLat;
	}

	public void setStarttime(String starttime){
		this.starttime = starttime;
	}

	public String getStarttime(){
		return starttime;
	}

	public void setRegistartionCopy(String registartionCopy){
		this.registartionCopy = registartionCopy;
	}

	public String getRegistartionCopy(){
		return registartionCopy;
	}

	public void setCustDropLocation(String custDropLocation){
		this.custDropLocation = custDropLocation;
	}

	public String getCustDropLocation(){
		return custDropLocation;
	}

	public void setCustAddressCopy(String custAddressCopy){
		this.custAddressCopy = custAddressCopy;
	}

	public String getCustAddressCopy(){
		return custAddressCopy;
	}

	public void setCustGender(int custGender){
		this.custGender = custGender;
	}

	public int getCustGender(){
		return custGender;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setCustType(int custType){
		this.custType = custType;
	}

	public int getCustType(){
		return custType;
	}

	public void setDriverDropLon(double driverDropLon){
		this.driverDropLon = driverDropLon;
	}

	public double getDriverPickLon(){
		return driverPickLon;
	}

	public void setDriverPickLon(double driverPickLon){
		this.driverPickLon = driverPickLon;
	}

	public double getDriverDropLon(){
		return driverDropLon;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCust_request_id(int id){
		this.cust_request_id = id;
	}

	public int getCust_request_id(){
		return cust_request_id;
	}


	public void setCustEmergencyContact2(Object custEmergencyContact2){
		this.custEmergencyContact2 = custEmergencyContact2;
	}

	public Object getCustEmergencyContact2(){
		return custEmergencyContact2;
	}

	public void setCustEmergencyContact1(Object custEmergencyContact1){
		this.custEmergencyContact1 = custEmergencyContact1;
	}

	public Object getCustEmergencyContact1(){
		return custEmergencyContact1;
	}

	public void setCustEmergencyContact4(Object custEmergencyContact4){
		this.custEmergencyContact4 = custEmergencyContact4;
	}

	public Object getCustEmergencyContact4(){
		return custEmergencyContact4;
	}

	public void setCustEmergencyContact3(Object custEmergencyContact3){
		this.custEmergencyContact3 = custEmergencyContact3;
	}

	public Object getCustEmergencyContact3(){
		return custEmergencyContact3;
	}

	public void setCustomerDeviceId(String customerDeviceId){
		this.customerDeviceId = customerDeviceId;
	}

	public String getCustomerDeviceId(){
		return customerDeviceId;
	}

	public void setCustomerSocketId(String customerSocketId){
		this.customerSocketId = customerSocketId;
	}

	public String getCustomerSocketId(){
		return customerSocketId;
	}


	public void setDriverDropLat(double driverDropLat){
		this.driverDropLat = driverDropLat;
	}

	public double getDriverDropLat(){
		return driverDropLat;
	}

	public double getDriverPickLat(){
		return driverPickLat;
	}

	public void setDriverPickLat(double driverPickLat){
		this.driverPickLat = driverPickLat;
	}





	public void setLoginStatus(int loginStatus){
		this.loginStatus = loginStatus;
	}

	public int getLoginStatus(){
		return loginStatus;
	}

	public void setCustProfilePic(String custProfilePic){
		this.custProfilePic = custProfilePic;
	}

	public String getCustProfilePic(){
		return custProfilePic;
	}

	public void setPanNumber(String panNumber){
		this.panNumber = panNumber;
	}

	public String getPanNumber(){
		return panNumber;
	}

	public void setCustPickLocationLon(double custPickLocationLon){
		this.custPickLocationLon = custPickLocationLon;
	}

	public double getCustPickLocationLon(){
		return custPickLocationLon;
	}

	public void setCustEmail(String custEmail){
		this.custEmail = custEmail;
	}

	public String getCustEmail(){
		return custEmail;
	}

	public void setCustDropLocationLat(double custDropLocationLat){
		this.custDropLocationLat = custDropLocationLat;
	}

	public double getCustDropLocationLat(){
		return custDropLocationLat;
	}

	public void setCustId(int custId){
		this.custId = custId;
	}

	public int getCustId(){
		return custId;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public void setDropLon(String dropLon){
		this.dropLon = dropLon;
	}

	public String getDropLon(){
		return dropLon;
	}

	public void setCustRideStatus(int custRideStatus){
		this.custRideStatus = custRideStatus;
	}

	public int getCustRideStatus(){
		return custRideStatus;
	}

	public void setVerificationStatus(int verificationStatus){
		this.verificationStatus = verificationStatus;
	}

	public int getVerificationStatus(){
		return verificationStatus;
	}

	public void setCustDob(String custDob){
		this.custDob = custDob;
	}

	public String getCustDob(){
		return custDob;
	}

	public void setVehicleCopy(String vehicleCopy){
		this.vehicleCopy = vehicleCopy;
	}

	public String getVehicleCopy(){
		return vehicleCopy;
	}

	public void setFullRoute(String fullRoute){
		this.fullRoute = fullRoute;
	}

	public String getFullRoute(){
		return fullRoute;
	}

	public void setCustStatus(int custStatus){
		this.custStatus = custStatus;
	}

	public int getCustStatus(){
		return custStatus;
	}

	public void setTimeStatus(int timeStatus){
		this.timeStatus = timeStatus;
	}

	public int getTimeStatus(){
		return timeStatus;
	}

	public void setCurrentLon(String currentLon){
		this.currentLon = currentLon;
	}

	public String getCurrentLon(){
		return currentLon;
	}

	public void setPickLon(String pickLon){
		this.pickLon = pickLon;
	}

	public String getPickLon(){
		return pickLon;
	}

	public void setCustPanCopy(String custPanCopy){
		this.custPanCopy = custPanCopy;
	}

	public String getCustPanCopy(){
		return custPanCopy;
	}

	public void setCustContactNo(String custContactNo){
		this.custContactNo = custContactNo;
	}

	public String getCustContactNo(){
		return custContactNo;
	}

	public void setCustDropLocationLon(double custDropLocationLon){
		this.custDropLocationLon = custDropLocationLon;
	}

	public double getCustDropLocationLon(){
		return custDropLocationLon;
	}

	public void setDlNo(String dlNo){
		this.dlNo = dlNo;
	}

	public String getDlNo(){
		return dlNo;
	}

	public void setDlCopy(String dlCopy){
		this.dlCopy = dlCopy;
	}

	public String getDlCopy(){
		return dlCopy;
	}

	public void setCustFirstName(String custFirstName){
		this.custFirstName = custFirstName;
	}

	public String getCustFirstName(){
		return custFirstName;
	}

	public void setCustPickLocation(String custPickLocation){
		this.custPickLocation = custPickLocation;
	}

	public String getCustPickLocation(){
		return custPickLocation;
	}

	public void setPickLat(String pickLat){
		this.pickLat = pickLat;
	}

	public String getPickLat(){
		return pickLat;
	}

	public void setDropLat(String dropLat){
		this.dropLat = dropLat;
	}

	public String getDropLat(){
		return dropLat;
	}

	public void setCustomerStatus(int customerStatus){
		this.customerStatus = customerStatus;
	}

	public int getCustomerStatus(){
		return customerStatus;
	}

	public void setCustomerAccessToken(String customerAccessToken){
		this.customerAccessToken = customerAccessToken;
	}

	public String getCustomerAccessToken(){
		return customerAccessToken;
	}

	public void setCustAlternateNo(Object custAlternateNo){
		this.custAlternateNo = custAlternateNo;
	}

	public Object getCustAlternateNo(){
		return custAlternateNo;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setUpdatedDate(String updatedDate){
		this.updatedDate = updatedDate;
	}

	public String getUpdatedDate(){
		return updatedDate;
	}

	public void setCustLastName(Object custLastName){
		this.custLastName = custLastName;
	}

	public Object getCustLastName(){
		return custLastName;
	}

	public void setInsuranceCopy(String insuranceCopy){
		this.insuranceCopy = insuranceCopy;
	}

	public String getInsuranceCopy(){
		return insuranceCopy;
	}

	public void setCustCity(Object custCity){
		this.custCity = custCity;
	}

	public Object getCustCity(){
		return custCity;
	}

	@Override
 	public String toString(){
		return 
			"DriverDetail{" + 
			"cust_pick_location_lat = '" + custPickLocationLat + '\'' + 
			",current_lat = '" + currentLat + '\'' + 
			",starttime = '" + starttime + '\'' + 
			",registartion_copy = '" + registartionCopy + '\'' + 
			",cust_drop_location = '" + custDropLocation + '\'' + 
			",cust_address_copy = '" + custAddressCopy + '\'' + 
			",cust_gender = '" + custGender + '\'' + 
			",password = '" + password + '\'' + 
			",cust_type = '" + custType + '\'' + 
			",driver_drop_lon = '" + driverDropLon + '\'' + 
			",id = '" + id + '\'' + 
			",cust_emergency_contact_2 = '" + custEmergencyContact2 + '\'' + 
			",cust_emergency_contact_1 = '" + custEmergencyContact1 + '\'' + 
			",cust_emergency_contact_4 = '" + custEmergencyContact4 + '\'' + 
			",cust_emergency_contact_3 = '" + custEmergencyContact3 + '\'' + 
			",customer_device_id = '" + customerDeviceId + '\'' + 
			",customer_socket_id = '" + customerSocketId + '\'' + 
			",otp = '" + custOtp + '\'' +
			",driver_drop_lat = '" + driverDropLat + '\'' + 
			",login_status = '" + loginStatus + '\'' + 
			",cust_profile_pic = '" + custProfilePic + '\'' + 
			",pan_number = '" + panNumber + '\'' + 
			",cust_pick_location_lon = '" + custPickLocationLon + '\'' + 
			",cust_email = '" + custEmail + '\'' + 
			",cust_drop_location_lat = '" + custDropLocationLat + '\'' + 
			",cust_id = '" + custId + '\'' + 
			",status = '" + status + '\'' + 
			",drop_lon = '" + dropLon + '\'' + 
			",cust_ride_status = '" + custRideStatus + '\'' + 
			",verification_status = '" + verificationStatus + '\'' + 
			",cust_dob = '" + custDob + '\'' + 
			",vehicle_copy = '" + vehicleCopy + '\'' + 
			",full_route = '" + fullRoute + '\'' + 
			",cust_status = '" + custStatus + '\'' + 
			",time_status = '" + timeStatus + '\'' + 
			",current_lon = '" + currentLon + '\'' + 
			",pick_lon = '" + pickLon + '\'' + 
			",cust_pan_copy = '" + custPanCopy + '\'' + 
			",cust_contact_no = '" + custContactNo + '\'' + 
			",cust_drop_location_lon = '" + custDropLocationLon + '\'' + 
			",dl_no = '" + dlNo + '\'' + 
			",dl_copy = '" + dlCopy + '\'' + 
			",cust_first_name = '" + custFirstName + '\'' + 
			",cust_pick_location = '" + custPickLocation + '\'' + 
			",pick_lat = '" + pickLat + '\'' + 
			",drop_lat = '" + dropLat + '\'' + 
			",customer_status = '" + customerStatus + '\'' + 
			",customer_access_token = '" + customerAccessToken + '\'' + 
			",cust_alternate_no = '" + custAlternateNo + '\'' + 
			",created_date = '" + createdDate + '\'' + 
			",updated_date = '" + updatedDate + '\'' + 
			",cust_last_name = '" + custLastName + '\'' + 
			",insurance_copy = '" + insuranceCopy + '\'' + 
			",cust_city = '" + custCity + '\'' + 
			"}";
		}
}