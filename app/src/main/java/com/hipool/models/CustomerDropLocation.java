package com.hipool.models;


import com.google.gson.annotations.SerializedName;


public class CustomerDropLocation {

	@SerializedName("DriverDetail")
	private DriverDetail driverDetail;

	public void setDriverDetail(DriverDetail driverDetail){
		this.driverDetail = driverDetail;
	}

	public DriverDetail getDriverDetail(){
		return driverDetail;
	}

	@Override
 	public String toString(){
		return 
			"CustomerDropLocation{" +
			"driverDetail = '" + driverDetail + '\'' + 
			"}";
		}
}