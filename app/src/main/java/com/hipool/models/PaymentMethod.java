package com.hipool.models;

/**
 * Created by Ashutosh on 27-05-2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentMethod {

    @SerializedName("customer_pay_id")
    @Expose
    private Integer customerPayId;
    @SerializedName("cust_id")
    @Expose
    private Integer custId;
    @SerializedName("payment_method_type")
    @Expose
    private Integer paymentMethodType;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getCustomerPayId() {
        return customerPayId;
    }

    public void setCustomerPayId(Integer customerPayId) {
        this.customerPayId = customerPayId;
    }

    public Integer getCustId() {
        return custId;
    }

    public void setCustId(Integer custId) {
        this.custId = custId;
    }

    public Integer getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(Integer paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}