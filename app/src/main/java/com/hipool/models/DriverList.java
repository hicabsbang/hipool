package com.hipool.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class DriverList {

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private List<CustomerDropLocation> data;

	@SerializedName("message")
	private String message;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(List<CustomerDropLocation> data){
		this.data = data;
	}

	public List<CustomerDropLocation> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"DriverList{" +
			"code = '" + code + '\'' + 
			",data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}