package com.hipool.models;

import com.google.gson.annotations.SerializedName;

public class RideGiven{

	@SerializedName("driver_id")
	private int driverId;

	@SerializedName("orderId")
	private Object orderId;

	@SerializedName("ride_endtime")
	private Object rideEndtime;

	@SerializedName("registartion_copy")
	private String registartionCopy;

	@SerializedName("driver_payment")
	private Double driverPayment;

	@SerializedName("cust_address_copy")
	private String custAddressCopy;

	@SerializedName("cust_gender")
	private int custGender;

	@SerializedName("password")
	private String password;

	@SerializedName("start_index")
	private int startIndex;

	@SerializedName("cust_type")
	private int custType;

	@SerializedName("cust_enter_pick_lan")
	private double custEnterPickLan;

	@SerializedName("total_distance")
	private int totalDistance;

	@SerializedName("estimated_fare")
	private double estimatedFare;

	@SerializedName("cust_emergency_contact_2")
	private Object custEmergencyContact2;

	@SerializedName("total_time")
	private int totalTime;

	@SerializedName("cust_emergency_contact_1")
	private Object custEmergencyContact1;

	@SerializedName("cust_emergency_contact_4")
	private Object custEmergencyContact4;

	@SerializedName("payment_method")
	private int paymentMethod;

	@SerializedName("cust_emergency_contact_3")
	private Object custEmergencyContact3;

	@SerializedName("customer_device_id")
	private String customerDeviceId;

	@SerializedName("customer_socket_id")
	private String customerSocketId;

	@SerializedName("driver_starting_lon")
	private double driverStartingLon;

	@SerializedName("cust_drop_address")
	private String custDropAddress;

	@SerializedName("customer_request_id")
	private int customerRequestId;

	@SerializedName("coupon_amount_tax")
	private int couponAmountTax;

	@SerializedName("driver_request_id")
	private int driverRequestId;

	@SerializedName("end_index")
	private int endIndex;

	@SerializedName("otp")
	private int otp;

	@SerializedName("booking_status")
	private int bookingStatus;

	@SerializedName("driver_ending_lon")
	private double driverEndingLon;

	@SerializedName("estimated_fare_tax")
	private double estimatedFareTax;

	@SerializedName("ride_starttime")
	private Object rideStarttime;

	@SerializedName("cust_pick_lon")
	private double custPickLon;

	@SerializedName("cust_enter_drop_lon")
	private double custEnterDropLon;

	@SerializedName("login_status")
	private int loginStatus;

	@SerializedName("cust_profile_pic")
	private String custProfilePic;

	@SerializedName("actual_drop_lon")
	private double actualDropLon;

	@SerializedName("pan_number")
	private String panNumber;

	@SerializedName("cust_email")
	private String custEmail;

	@SerializedName("cust_drop_lat")
	private double custDropLat;

	@SerializedName("cust_id")
	private int custId;

	@SerializedName("driver_ride_start_time")
	private String driverRideStartTime;

	@SerializedName("coupon_type")
	private int couponType;

	@SerializedName("coupon_applied")
	private int couponApplied;

	@SerializedName("driver_ending_address")
	private Object driverEndingAddress;

	@SerializedName("cust_ride_status")
	private int custRideStatus;

	@SerializedName("driver_payment_tax")
	private Double driverPaymentTax;

	@SerializedName("verification_status")
	private int verificationStatus;

	@SerializedName("actual_pick_lon")
	private double actualPickLon;

	@SerializedName("booking_id")
	private int bookingId;

	@SerializedName("cust_dob")
	private String custDob;

	@SerializedName("vehicle_copy")
	private String vehicleCopy;

	@SerializedName("cust_enter_pick_lon")
	private double custEnterPickLon;

	@SerializedName("full_route")
	private String fullRoute;

	@SerializedName("cust_pick_lat")
	private double custPickLat;

	@SerializedName("cust_status")
	private int custStatus;

	@SerializedName("actual_drop_lat")
	private double actualDropLat;

	@SerializedName("cust_pan_copy")
	private String custPanCopy;

	@SerializedName("coupon_amount")
	private int couponAmount;

	@SerializedName("actual_pick_lat")
	private double actualPickLat;

	@SerializedName("cust_drop_lon")
	private double custDropLon;

	@SerializedName("cust_contact_no")
	private String custContactNo;

	@SerializedName("dl_no")
	private String dlNo;

	@SerializedName("dl_copy")
	private String dlCopy;

	@SerializedName("cust_first_name")
	private String custFirstName;

	@SerializedName("customer_status")
	private int customerStatus;

	@SerializedName("driver_starting_lat")
	private double driverStartingLat;

	@SerializedName("driver_starting_address")
	private Object driverStartingAddress;

	@SerializedName("customer_access_token")
	private String customerAccessToken;

	@SerializedName("driver_ending_lat")
	private double driverEndingLat;

	@SerializedName("cust_alternate_no")
	private Object custAlternateNo;

	@SerializedName("cust_pick_address")
	private String custPickAddress;

	@SerializedName("created_date")
	private String createdDate;

	@SerializedName("updated_date")
	private String updatedDate;

	@SerializedName("cust_last_name")
	private Object custLastName;

	@SerializedName("insurance_copy")
	private String insuranceCopy;

	@SerializedName("cust_enter_drop_lan")
	private double custEnterDropLan;

	@SerializedName("cust_city")
	private Object custCity;

	public void setDriverId(int driverId){
		this.driverId = driverId;
	}

	public int getDriverId(){
		return driverId;
	}

	public void setOrderId(Object orderId){
		this.orderId = orderId;
	}

	public Object getOrderId(){
		return orderId;
	}

	public void setRideEndtime(Object rideEndtime){
		this.rideEndtime = rideEndtime;
	}

	public Object getRideEndtime(){
		return rideEndtime;
	}

	public void setRegistartionCopy(String registartionCopy){
		this.registartionCopy = registartionCopy;
	}

	public String getRegistartionCopy(){
		return registartionCopy;
	}

	public void setDriverPayment(Double driverPayment){
		this.driverPayment = driverPayment;
	}

	public Double getDriverPayment(){
		return driverPayment;
	}

	public void setCustAddressCopy(String custAddressCopy){
		this.custAddressCopy = custAddressCopy;
	}

	public String getCustAddressCopy(){
		return custAddressCopy;
	}

	public void setCustGender(int custGender){
		this.custGender = custGender;
	}

	public int getCustGender(){
		return custGender;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setStartIndex(int startIndex){
		this.startIndex = startIndex;
	}

	public int getStartIndex(){
		return startIndex;
	}

	public void setCustType(int custType){
		this.custType = custType;
	}

	public int getCustType(){
		return custType;
	}

	public void setCustEnterPickLan(double custEnterPickLan){
		this.custEnterPickLan = custEnterPickLan;
	}

	public double getCustEnterPickLan(){
		return custEnterPickLan;
	}

	public void setTotalDistance(int totalDistance){
		this.totalDistance = totalDistance;
	}

	public int getTotalDistance(){
		return totalDistance;
	}

	public void setEstimatedFare(double estimatedFare){
		this.estimatedFare = estimatedFare;
	}

	public double getEstimatedFare(){
		return estimatedFare;
	}

	public void setCustEmergencyContact2(Object custEmergencyContact2){
		this.custEmergencyContact2 = custEmergencyContact2;
	}

	public Object getCustEmergencyContact2(){
		return custEmergencyContact2;
	}

	public void setTotalTime(int totalTime){
		this.totalTime = totalTime;
	}

	public int getTotalTime(){
		return totalTime;
	}

	public void setCustEmergencyContact1(Object custEmergencyContact1){
		this.custEmergencyContact1 = custEmergencyContact1;
	}

	public Object getCustEmergencyContact1(){
		return custEmergencyContact1;
	}

	public void setCustEmergencyContact4(Object custEmergencyContact4){
		this.custEmergencyContact4 = custEmergencyContact4;
	}

	public Object getCustEmergencyContact4(){
		return custEmergencyContact4;
	}

	public void setPaymentMethod(int paymentMethod){
		this.paymentMethod = paymentMethod;
	}

	public int getPaymentMethod(){
		return paymentMethod;
	}

	public void setCustEmergencyContact3(Object custEmergencyContact3){
		this.custEmergencyContact3 = custEmergencyContact3;
	}

	public Object getCustEmergencyContact3(){
		return custEmergencyContact3;
	}

	public void setCustomerDeviceId(String customerDeviceId){
		this.customerDeviceId = customerDeviceId;
	}

	public String getCustomerDeviceId(){
		return customerDeviceId;
	}

	public void setCustomerSocketId(String customerSocketId){
		this.customerSocketId = customerSocketId;
	}

	public String getCustomerSocketId(){
		return customerSocketId;
	}

	public void setDriverStartingLon(double driverStartingLon){
		this.driverStartingLon = driverStartingLon;
	}

	public double getDriverStartingLon(){
		return driverStartingLon;
	}

	public void setCustDropAddress(String custDropAddress){
		this.custDropAddress = custDropAddress;
	}

	public String getCustDropAddress(){
		return custDropAddress;
	}

	public void setCustomerRequestId(int customerRequestId){
		this.customerRequestId = customerRequestId;
	}

	public int getCustomerRequestId(){
		return customerRequestId;
	}

	public void setCouponAmountTax(int couponAmountTax){
		this.couponAmountTax = couponAmountTax;
	}

	public int getCouponAmountTax(){
		return couponAmountTax;
	}

	public void setDriverRequestId(int driverRequestId){
		this.driverRequestId = driverRequestId;
	}

	public int getDriverRequestId(){
		return driverRequestId;
	}

	public void setEndIndex(int endIndex){
		this.endIndex = endIndex;
	}

	public int getEndIndex(){
		return endIndex;
	}

	public void setOtp(int otp){
		this.otp = otp;
	}

	public int getOtp(){
		return otp;
	}

	public void setBookingStatus(int bookingStatus){
		this.bookingStatus = bookingStatus;
	}

	public int getBookingStatus(){
		return bookingStatus;
	}

	public void setDriverEndingLon(double driverEndingLon){
		this.driverEndingLon = driverEndingLon;
	}

	public double getDriverEndingLon(){
		return driverEndingLon;
	}

	public void setEstimatedFareTax(double estimatedFareTax){
		this.estimatedFareTax = estimatedFareTax;
	}

	public double getEstimatedFareTax(){
		return estimatedFareTax;
	}

	public void setRideStarttime(Object rideStarttime){
		this.rideStarttime = rideStarttime;
	}

	public Object getRideStarttime(){
		return rideStarttime;
	}

	public void setCustPickLon(double custPickLon){
		this.custPickLon = custPickLon;
	}

	public double getCustPickLon(){
		return custPickLon;
	}

	public void setCustEnterDropLon(double custEnterDropLon){
		this.custEnterDropLon = custEnterDropLon;
	}

	public double getCustEnterDropLon(){
		return custEnterDropLon;
	}

	public void setLoginStatus(int loginStatus){
		this.loginStatus = loginStatus;
	}

	public int getLoginStatus(){
		return loginStatus;
	}

	public void setCustProfilePic(String custProfilePic){
		this.custProfilePic = custProfilePic;
	}

	public String getCustProfilePic(){
		return custProfilePic;
	}

	public void setActualDropLon(double actualDropLon){
		this.actualDropLon = actualDropLon;
	}

	public double getActualDropLon(){
		return actualDropLon;
	}

	public void setPanNumber(String panNumber){
		this.panNumber = panNumber;
	}

	public String getPanNumber(){
		return panNumber;
	}

	public void setCustEmail(String custEmail){
		this.custEmail = custEmail;
	}

	public String getCustEmail(){
		return custEmail;
	}

	public void setCustDropLat(double custDropLat){
		this.custDropLat = custDropLat;
	}

	public double getCustDropLat(){
		return custDropLat;
	}

	public void setCustId(int custId){
		this.custId = custId;
	}

	public int getCustId(){
		return custId;
	}

	public void setDriverRideStartTime(String driverRideStartTime){
		this.driverRideStartTime = driverRideStartTime;
	}

	public String getDriverRideStartTime(){
		return driverRideStartTime;
	}

	public void setCouponType(int couponType){
		this.couponType = couponType;
	}

	public int getCouponType(){
		return couponType;
	}

	public void setCouponApplied(int couponApplied){
		this.couponApplied = couponApplied;
	}

	public int getCouponApplied(){
		return couponApplied;
	}

	public void setDriverEndingAddress(Object driverEndingAddress){
		this.driverEndingAddress = driverEndingAddress;
	}

	public Object getDriverEndingAddress(){
		return driverEndingAddress;
	}

	public void setCustRideStatus(int custRideStatus){
		this.custRideStatus = custRideStatus;
	}

	public int getCustRideStatus(){
		return custRideStatus;
	}

	public void setDriverPaymentTax(Double driverPaymentTax){
		this.driverPaymentTax = driverPaymentTax;
	}

	public Double getDriverPaymentTax(){
		return driverPaymentTax;
	}

	public void setVerificationStatus(int verificationStatus){
		this.verificationStatus = verificationStatus;
	}

	public int getVerificationStatus(){
		return verificationStatus;
	}

	public void setActualPickLon(double actualPickLon){
		this.actualPickLon = actualPickLon;
	}

	public double getActualPickLon(){
		return actualPickLon;
	}

	public void setBookingId(int bookingId){
		this.bookingId = bookingId;
	}

	public int getBookingId(){
		return bookingId;
	}

	public void setCustDob(String custDob){
		this.custDob = custDob;
	}

	public String getCustDob(){
		return custDob;
	}

	public void setVehicleCopy(String vehicleCopy){
		this.vehicleCopy = vehicleCopy;
	}

	public String getVehicleCopy(){
		return vehicleCopy;
	}

	public void setCustEnterPickLon(double custEnterPickLon){
		this.custEnterPickLon = custEnterPickLon;
	}

	public double getCustEnterPickLon(){
		return custEnterPickLon;
	}

	public void setFullRoute(String fullRoute){
		this.fullRoute = fullRoute;
	}

	public String getFullRoute(){
		return fullRoute;
	}

	public void setCustPickLat(double custPickLat){
		this.custPickLat = custPickLat;
	}

	public double getCustPickLat(){
		return custPickLat;
	}

	public void setCustStatus(int custStatus){
		this.custStatus = custStatus;
	}

	public int getCustStatus(){
		return custStatus;
	}

	public void setActualDropLat(double actualDropLat){
		this.actualDropLat = actualDropLat;
	}

	public double getActualDropLat(){
		return actualDropLat;
	}

	public void setCustPanCopy(String custPanCopy){
		this.custPanCopy = custPanCopy;
	}

	public String getCustPanCopy(){
		return custPanCopy;
	}

	public void setCouponAmount(int couponAmount){
		this.couponAmount = couponAmount;
	}

	public int getCouponAmount(){
		return couponAmount;
	}

	public void setActualPickLat(double actualPickLat){
		this.actualPickLat = actualPickLat;
	}

	public double getActualPickLat(){
		return actualPickLat;
	}

	public void setCustDropLon(double custDropLon){
		this.custDropLon = custDropLon;
	}

	public double getCustDropLon(){
		return custDropLon;
	}

	public void setCustContactNo(String custContactNo){
		this.custContactNo = custContactNo;
	}

	public String getCustContactNo(){
		return custContactNo;
	}

	public void setDlNo(String dlNo){
		this.dlNo = dlNo;
	}

	public String getDlNo(){
		return dlNo;
	}

	public void setDlCopy(String dlCopy){
		this.dlCopy = dlCopy;
	}

	public String getDlCopy(){
		return dlCopy;
	}

	public void setCustFirstName(String custFirstName){
		this.custFirstName = custFirstName;
	}

	public String getCustFirstName(){
		return custFirstName;
	}

	public void setCustomerStatus(int customerStatus){
		this.customerStatus = customerStatus;
	}

	public int getCustomerStatus(){
		return customerStatus;
	}

	public void setDriverStartingLat(double driverStartingLat){
		this.driverStartingLat = driverStartingLat;
	}

	public double getDriverStartingLat(){
		return driverStartingLat;
	}

	public void setDriverStartingAddress(Object driverStartingAddress){
		this.driverStartingAddress = driverStartingAddress;
	}

	public Object getDriverStartingAddress(){
		return driverStartingAddress;
	}

	public void setCustomerAccessToken(String customerAccessToken){
		this.customerAccessToken = customerAccessToken;
	}

	public String getCustomerAccessToken(){
		return customerAccessToken;
	}

	public void setDriverEndingLat(double driverEndingLat){
		this.driverEndingLat = driverEndingLat;
	}

	public double getDriverEndingLat(){
		return driverEndingLat;
	}

	public void setCustAlternateNo(Object custAlternateNo){
		this.custAlternateNo = custAlternateNo;
	}

	public Object getCustAlternateNo(){
		return custAlternateNo;
	}

	public void setCustPickAddress(String custPickAddress){
		this.custPickAddress = custPickAddress;
	}

	public String getCustPickAddress(){
		return custPickAddress;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setUpdatedDate(String updatedDate){
		this.updatedDate = updatedDate;
	}

	public String getUpdatedDate(){
		return updatedDate;
	}

	public void setCustLastName(Object custLastName){
		this.custLastName = custLastName;
	}

	public Object getCustLastName(){
		return custLastName;
	}

	public void setInsuranceCopy(String insuranceCopy){
		this.insuranceCopy = insuranceCopy;
	}

	public String getInsuranceCopy(){
		return insuranceCopy;
	}

	public void setCustEnterDropLan(double custEnterDropLan){
		this.custEnterDropLan = custEnterDropLan;
	}

	public double getCustEnterDropLan(){
		return custEnterDropLan;
	}

	public void setCustCity(Object custCity){
		this.custCity = custCity;
	}

	public Object getCustCity(){
		return custCity;
	}

	@Override
 	public String toString(){
		return 
			"RideGiven{" + 
			"driver_id = '" + driverId + '\'' + 
			",orderId = '" + orderId + '\'' + 
			",ride_endtime = '" + rideEndtime + '\'' + 
			",registartion_copy = '" + registartionCopy + '\'' + 
			",driver_payment = '" + driverPayment + '\'' + 
			",cust_address_copy = '" + custAddressCopy + '\'' + 
			",cust_gender = '" + custGender + '\'' + 
			",password = '" + password + '\'' + 
			",start_index = '" + startIndex + '\'' + 
			",cust_type = '" + custType + '\'' + 
			",cust_enter_pick_lan = '" + custEnterPickLan + '\'' + 
			",total_distance = '" + totalDistance + '\'' + 
			",estimated_fare = '" + estimatedFare + '\'' + 
			",cust_emergency_contact_2 = '" + custEmergencyContact2 + '\'' + 
			",total_time = '" + totalTime + '\'' + 
			",cust_emergency_contact_1 = '" + custEmergencyContact1 + '\'' + 
			",cust_emergency_contact_4 = '" + custEmergencyContact4 + '\'' + 
			",payment_method = '" + paymentMethod + '\'' + 
			",cust_emergency_contact_3 = '" + custEmergencyContact3 + '\'' + 
			",customer_device_id = '" + customerDeviceId + '\'' + 
			",customer_socket_id = '" + customerSocketId + '\'' + 
			",driver_starting_lon = '" + driverStartingLon + '\'' + 
			",cust_drop_address = '" + custDropAddress + '\'' + 
			",customer_request_id = '" + customerRequestId + '\'' + 
			",coupon_amount_tax = '" + couponAmountTax + '\'' + 
			",driver_request_id = '" + driverRequestId + '\'' + 
			",end_index = '" + endIndex + '\'' + 
			",otp = '" + otp + '\'' + 
			",booking_status = '" + bookingStatus + '\'' + 
			",driver_ending_lon = '" + driverEndingLon + '\'' + 
			",estimated_fare_tax = '" + estimatedFareTax + '\'' + 
			",ride_starttime = '" + rideStarttime + '\'' + 
			",cust_pick_lon = '" + custPickLon + '\'' + 
			",cust_enter_drop_lon = '" + custEnterDropLon + '\'' + 
			",login_status = '" + loginStatus + '\'' + 
			",cust_profile_pic = '" + custProfilePic + '\'' + 
			",actual_drop_lon = '" + actualDropLon + '\'' + 
			",pan_number = '" + panNumber + '\'' + 
			",cust_email = '" + custEmail + '\'' + 
			",cust_drop_lat = '" + custDropLat + '\'' + 
			",cust_id = '" + custId + '\'' + 
			",driver_ride_start_time = '" + driverRideStartTime + '\'' + 
			",coupon_type = '" + couponType + '\'' + 
			",coupon_applied = '" + couponApplied + '\'' + 
			",driver_ending_address = '" + driverEndingAddress + '\'' + 
			",cust_ride_status = '" + custRideStatus + '\'' + 
			",driver_payment_tax = '" + driverPaymentTax + '\'' + 
			",verification_status = '" + verificationStatus + '\'' + 
			",actual_pick_lon = '" + actualPickLon + '\'' + 
			",booking_id = '" + bookingId + '\'' + 
			",cust_dob = '" + custDob + '\'' + 
			",vehicle_copy = '" + vehicleCopy + '\'' + 
			",cust_enter_pick_lon = '" + custEnterPickLon + '\'' + 
			",full_route = '" + fullRoute + '\'' + 
			",cust_pick_lat = '" + custPickLat + '\'' + 
			",cust_status = '" + custStatus + '\'' + 
			",actual_drop_lat = '" + actualDropLat + '\'' + 
			",cust_pan_copy = '" + custPanCopy + '\'' + 
			",coupon_amount = '" + couponAmount + '\'' + 
			",actual_pick_lat = '" + actualPickLat + '\'' + 
			",cust_drop_lon = '" + custDropLon + '\'' + 
			",cust_contact_no = '" + custContactNo + '\'' + 
			",dl_no = '" + dlNo + '\'' + 
			",dl_copy = '" + dlCopy + '\'' + 
			",cust_first_name = '" + custFirstName + '\'' + 
			",customer_status = '" + customerStatus + '\'' + 
			",driver_starting_lat = '" + driverStartingLat + '\'' + 
			",driver_starting_address = '" + driverStartingAddress + '\'' + 
			",customer_access_token = '" + customerAccessToken + '\'' + 
			",driver_ending_lat = '" + driverEndingLat + '\'' + 
			",cust_alternate_no = '" + custAlternateNo + '\'' + 
			",cust_pick_address = '" + custPickAddress + '\'' + 
			",created_date = '" + createdDate + '\'' + 
			",updated_date = '" + updatedDate + '\'' + 
			",cust_last_name = '" + custLastName + '\'' + 
			",insurance_copy = '" + insuranceCopy + '\'' + 
			",cust_enter_drop_lan = '" + custEnterDropLan + '\'' + 
			",cust_city = '" + custCity + '\'' + 
			"}";
		}
}