package com.hipool.models;

/**
 * Created by Ashutosh on 19-05-2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaytmGetTokenRes {

    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("expires")
    @Expose
    private String expires;
    @SerializedName("scope")
    @Expose
    private String scope;
    @SerializedName("resourceOwnerId")
    @Expose
    private String resourceOwnerId;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getResourceOwnerId() {
        return resourceOwnerId;
    }

    public void setResourceOwnerId(String resourceOwnerId) {
        this.resourceOwnerId = resourceOwnerId;
    }

}