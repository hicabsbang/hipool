package com.hipool.riderrequests;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.adapters.CustomerRequestsRecyclerAdapter;
import com.hipool.models.CustomerRequest;
import com.hipool.utils.HiPoolSocketUtils;
import com.hipool.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.hipool.service.BookingService.SOCKET_DATA;
import static com.hipool.service.BookingService.SOCKET_DATA_RECIEVED_EVENT;
import static com.hipool.service.BookingService.SOCKET_EVENT;
import static com.hipool.utils.Constants.IS_DEBUG;

public class RiderRequestsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private CustomerRequestsRecyclerAdapter adapter;
    private List<CustomerRequest> customerRequestList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider_requests);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_activity_rider_requests);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Requests");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        initViews();

    }

    private void initViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_content_rider_requests);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new CustomerRequestsRecyclerAdapter(null, customerRequestList, getApplicationContext());
        recyclerView.setAdapter(adapter);

        getRequestList();
    }

    private void getRequestList() {
        String prefString = Prefs.getCustomerRequests(getApplicationContext());
        try {
            JSONArray requestArray = new JSONArray(prefString);
            customerRequestList.clear();
            for (int i = 0 ; i < requestArray.length() ; i++) {
                CustomerRequest customerRequest = new Gson().fromJson(requestArray.getString(i), CustomerRequest.class);

                for (int j = 0; j < customerRequestList.size(); j++) {
                    if (customerRequestList.get(j).getCustId() == customerRequest.getCustId()) {
                        customerRequestList.remove(j);
                    }
                }

                customerRequestList.add(customerRequest);
            }
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(
                socketDataReciever, new IntentFilter(SOCKET_DATA_RECIEVED_EVENT));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(
                socketDataReciever);
        super.onPause();
    }

    private BroadcastReceiver socketDataReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                String eventName = intent.getExtras().getString(SOCKET_EVENT);
                if (eventName != null) {
                    String data = intent.getExtras().getString(SOCKET_DATA);
                    if (IS_DEBUG) {
                        Log.v("recieved_event_data", data);
                        Log.v("recieved_event_name", eventName);

                        if (eventName.equals(HiPoolSocketUtils.EVENT_CONFIRM_CUSTOMER_BOOKING)) {
                            if (data.contains("Customer Notified")) {
                                try {
                                    Prefs.setLiveBookingId(getApplicationContext(), new JSONObject(data).getJSONObject("data").getJSONObject("content").getString("insertId"));
                                    finish();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "failed!", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            }
        }
    };
}
