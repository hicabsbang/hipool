package com.hipool.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.app.BaseFragment;
import com.hipool.models.RideGiven;
import com.hipool.models.RideTaken;
import com.hipool.ridehistory.RideGivenHistoryFragment;
import com.hipool.ridehistory.RideTakenHistoryFragment;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ashut on 03-10-2017.
 */

public class RideHistoryRecyclerAdapter extends RecyclerView.Adapter<RideHistoryRecyclerAdapter.ViewHolder> {

    private List<RideTaken> rideTakenList = new ArrayList<>();
    private List<RideGiven> rideGivenList = new ArrayList<>();
    private RideHistoryRecyclerAdapterCommunicator communicator;
    private Context context;

    public interface RideHistoryRecyclerAdapterCommunicator {
        public void onRideClicked(String rideData, String rideType);
    }

    public RideHistoryRecyclerAdapter(List<RideTaken> ridesTaken, List<RideGiven> ridesGiven, BaseFragment fragment) {
        this.rideTakenList = ridesTaken;
        this.rideGivenList = ridesGiven;
        if (fragment instanceof RideTakenHistoryFragment) {
            this.communicator = (RideTakenHistoryFragment) fragment;
        } else if (fragment instanceof RideGivenHistoryFragment) {
            this.communicator = (RideGivenHistoryFragment) fragment;
        }
        this.context = fragment.getContext();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ride_taken_history, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (rideTakenList != null) {
            final RideTaken rideTaken = rideTakenList.get(position);
            Picasso.with(context).load(rideTaken.getCustProfilePic()).error(R.drawable.userimage).placeholder(R.drawable.userimage).memoryPolicy(MemoryPolicy.NO_CACHE).into(holder.image);
            holder.pickLocation.setText(rideTaken.getCustPickAddress());
            holder.dropLocation.setText(rideTaken.getCustDropAddress());
            holder.rootCabNo.setVisibility(View.GONE);
            holder.dateTime.setText(rideTaken.getDriverRideStartTime());
            holder.fare.setText(String.format("%.2f",rideTaken.getEstimatedFare()+rideTaken.getEstimatedFareTax()));

            holder.relativeLayoutRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (communicator != null) {
                        communicator.onRideClicked(new Gson().toJson(rideTaken), "ridetaken");
                    }
                }
            });




        } else {

            final RideGiven rideGiven = rideGivenList.get(position);
            Log.v("dsfdsds", rideGiven.getCustProfilePic());
            Picasso.with(context).load(rideGiven.getCustProfilePic()).error(R.drawable.userimage).placeholder(R.drawable.userimage).memoryPolicy(MemoryPolicy.NO_CACHE).into(holder.image);
            holder.pickLocation.setText(rideGiven.getCustPickAddress());
            holder.dropLocation.setText(rideGiven.getCustDropAddress());
            holder.rootCabNo.setVisibility(View.GONE);
            holder.dateTime.setText(rideGiven.getDriverRideStartTime());
            holder.fare.setText(String.format("%.2f",rideGiven.getDriverPayment()-rideGiven.getDriverPaymentTax()));

            holder.relativeLayoutRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (communicator != null) {
                        communicator.onRideClicked(new Gson().toJson(rideGiven), "ridegiven");
                    }
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (rideTakenList != null) {
            count = rideTakenList.size();
        } else {
            count = rideGivenList.size();
        }
        return count;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
//        private TextView tripDistance;
        private TextView pickLocation;
        private TextView dropLocation;
        private TextView rideId;
        private TextView dateTime;
        private TextView fare;
        private RelativeLayout relativeLayoutRoot;
        private ImageView imageViewPaidLogo;
        private LinearLayout rootCabNo;

        public ViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.imageview_map_item_ride_history);
//            tripDistance = (TextView) itemView.findViewById(R.id.textview_distance_item_ride_history);
            pickLocation = (TextView) itemView.findViewById(R.id.textview_pick_location_item_ride_history);
            dropLocation = (TextView) itemView.findViewById(R.id.textview_drop_location_item_ride_history);
            rideId = (TextView) itemView.findViewById(R.id.textview_ride_id_item_ride_history);
            dateTime = (TextView) itemView.findViewById(R.id.textview_date_time_item_ride_history);
            fare = (TextView) itemView.findViewById(R.id.textview_fare_item_ride_history);
            relativeLayoutRoot = (RelativeLayout) itemView.findViewById(R.id.relative_root_ride_history_item);
            imageViewPaidLogo = (ImageView) itemView.findViewById(R.id.imageview_paid_logo_item_ride_history);
            rootCabNo = (LinearLayout) itemView.findViewById(R.id.root_cab_no_item_ride_taken_history);
        }
    }
}
