package com.hipool.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.hipool.utils.PlaceAPIRequest;

import java.util.ArrayList;

/**
 * Created by ashut on 29-08-2016.
 */
public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

    public ArrayList<String> resultArrayList;

    Context context;
    int resource;

    PlaceAPIRequest placeAPIRequest = new PlaceAPIRequest();

    public PlacesAutoCompleteAdapter(Context context1, int resource1){
        super(context1, resource1);
        this.context = context1;
        this.resource = resource1;
    }

    @Override
    public int getCount() {
        return resultArrayList.size();
    }

    @Override
    public String getItem(int position) {
        return resultArrayList.get(position);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint!=null){
                    if (constraint.toString().length() > 3){
                        resultArrayList = placeAPIRequest.autoComplete(constraint.toString());
                        filterResults.values = resultArrayList;
                        filterResults.count = resultArrayList.size();
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0){
                    notifyDataSetChanged();
                } else
                    notifyDataSetInvalidated();
            }
        };
        return filter;
    }

}
