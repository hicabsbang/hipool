package com.hipool.adapters;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;


import com.hipool.R;
import com.hipool.app.BaseFragment;
import com.hipool.fragment.DropLocationFragment;
import com.hipool.models.PaymentMode;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ashutosh on 31-05-2017.
 */

public class PaymentModeRecyclerAdapter extends RecyclerView.Adapter<PaymentModeRecyclerAdapter.ViewHolder> {

    private List<PaymentMode> paymentModeList = new ArrayList<>();
    private PaymentModeRecyclerCommunicator communicator;

    public interface PaymentModeRecyclerCommunicator {
        public void onPaymentModeSelected(int position, int paymentModeId);
    }

    public PaymentModeRecyclerAdapter(List<PaymentMode> paymentModeList, BaseFragment activity) {
        this.paymentModeList = paymentModeList;
        if (activity instanceof DropLocationFragment){
            communicator = (PaymentModeRecyclerCommunicator) activity;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_mode, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final PaymentMode paymentMode = paymentModeList.get(position);
        holder.circleImageViewPaymentMode.setImageResource(paymentMode.getImgResource());
        holder.relativeLayoutRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (communicator != null){
                    communicator.onPaymentModeSelected(holder.getAdapterPosition(), paymentMode.getId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentModeList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView circleImageViewPaymentMode;
        private RelativeLayout relativeLayoutRoot;

        public ViewHolder(View itemView) {
            super(itemView);

            circleImageViewPaymentMode = (CircleImageView) itemView.findViewById(R.id.image_payment_mode_item_payment_mode);
            relativeLayoutRoot = (RelativeLayout) itemView.findViewById(R.id.relative_root_item_payment_mode);
        }
    }
}
