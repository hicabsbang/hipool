package com.hipool.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hipool.R;
import com.hipool.app.BaseActivity;
import com.hipool.models.SupportItem;
import com.hipool.support.SupportActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashut on 12-10-2017.
 */

public class SupportRecyclerAdapter extends RecyclerView.Adapter<SupportRecyclerAdapter.ViewHolder> {

    private SupportRecyclerAdapterCommunicator communicator;
    private List<SupportItem> supportItems = new ArrayList<>();
    private int selected = 0;

    public interface SupportRecyclerAdapterCommunicator {
        public void onItemSelected(int position);
    }

    public SupportRecyclerAdapter(List<SupportItem> supportItemList, SupportActivity supportActivity) {
        this.supportItems = supportItemList;
        this.communicator = supportActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_support, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        SupportItem supportItem = supportItems.get(position);
        holder.text.setText(supportItem.getItem());
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (communicator != null) {
                    selected = position;
                    communicator.onItemSelected(position);
                }
            }
        });

        if (selected == position) {
            holder.root.setBackgroundColor(Color.parseColor("#BDBDBD"));
        } else {
            holder.root.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
    }

    @Override
    public int getItemCount() {
        return supportItems.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView text;
        public RelativeLayout root;

        public ViewHolder(View itemView) {
            super(itemView);

            text = (TextView) itemView.findViewById(R.id.text_item_support);
            root = (RelativeLayout) itemView.findViewById(R.id.root_item_support);
        }
    }
}
