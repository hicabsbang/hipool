package com.hipool.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.github.aakira.expandablelayout.Utils;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.animation.ShowAnimation;
import com.hipool.models.CustomerRequest;
import com.hipool.models.DriverDetail;
import com.hipool.service.BookingService;
import com.hipool.utils.Constants;
import com.hipool.utils.Prefs;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ashut on 20-09-2017.
 */

public class CustomerRequestsRecyclerAdapter extends RecyclerView.Adapter<CustomerRequestsRecyclerAdapter.ViewHolder> {

    private List<DriverDetail> driverDetailses = new ArrayList<>();
    private List<CustomerRequest> customerRequests = new ArrayList<>();
    private Context mContext;


    public CustomerRequestsRecyclerAdapter(List<DriverDetail> driverDetails, List<CustomerRequest> customerRequestList, Context context) {
        this.driverDetailses = driverDetails;
        this.customerRequests = customerRequestList;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_customer_request, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (driverDetailses != null){
            if (driverDetailses.size() > 0) {
                holder.expandableLinearLayout.collapse();
                holder.rootNotExpandable.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (holder.expandableLinearLayout.isExpanded()) {
                            holder.expandableLinearLayout.collapse();
                        } else {
                            holder.expandableLinearLayout.expand();
                        }
                    }
                });
                holder.textViewBikeNo.setVisibility(View.VISIBLE);
                final DriverDetail driverDetail = driverDetailses.get(position);
                holder.textViewName.setText(driverDetail.getCustFirstName());
                holder.textViewAge.setText(driverDetail.getVehicle_name());
                holder.textViewBikeNo.setText(driverDetail.getBikeNo());
                holder.textViewConfirmButton.setText("Send Request");
                holder.textViewPickup.setText(driverDetail.getCustPickLocation());
                holder.textViewDrop.setText(driverDetail.getCustDropLocation());
                holder.textViewReachTime.setText("Estimated time: "+Math.round(driverDetail.getReachTime()+1)+" mins");
                holder.textViewDistance.setText("Estimated Distance: " + Double.valueOf(driverDetail.getCustTotalDistance().trim()) / 1000 + "Km");
                holder.textViewFare.setText("Estimated Fare: ₹" + Math.round(Double.valueOf(driverDetail.getCustTotalFare())));
                holder.textViewDriverRequestID.setText(String.valueOf(driverDetail.getId()));
                holder.textViewDriverID.setText(String.valueOf(driverDetail.getCustId()));
                holder.textViewCustRequestID.setText(String.valueOf(driverDetail.getCust_request_id()));
                Picasso.with(mContext).load(driverDetail.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(holder.circleImageViewProfilePic);
                holder.textViewConfirmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            holder.textViewConfirmButton.setText("Request Sent");
                            holder.textViewConfirmButton.setClickable(false);
                            jsonObject.put("cust_request_id", holder.textViewCustRequestID.getText());
                            jsonObject.put("driver_request_id", holder.textViewDriverRequestID.getText());
                            Prefs.setDriverRequests(mContext,driverDetail);
                            BookingService.getInstance().bookCab(jsonObject.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } else if (customerRequests != null) {
            if (customerRequests.size() > 0) {
                holder.expandableLinearLayout.collapse();
                holder.rootNotExpandable.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (holder.expandableLinearLayout.isExpanded()) {
                            holder.expandableLinearLayout.collapse();
                        } else {
                            holder.expandableLinearLayout.expand();
                        }
                    }
                });

                final CustomerRequest customerRequest = customerRequests.get(position);
                holder.textViewName.setText(customerRequest.getCustFirstName());
                holder.textViewAge.setText("");
                holder.textViewConfirmButton.setText("Accept Ride");
                holder.textViewPickup.setText(customerRequest.getPickAddress());
                holder.textViewDrop.setText(customerRequest.getDropAddress());
                holder.textViewDistance.setText("Distance: " + com.hipool.utils.Utils.round(Double.valueOf(customerRequest.getTotalDistance() / 1000d), 1) + " Km");
                holder.textViewFare.setText("Earning: ₹" + Math.round(Double.valueOf(customerRequest.getEstimatedFare())));
                Picasso.with(mContext).load(customerRequest.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(holder.circleImageViewProfilePic);
                holder.textViewConfirmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("cust_request_id", customerRequest.getId());
                            jsonObject.put("driver_request_id", Prefs.getDriverRequestId(mContext));
                            BookingService.getInstance().confirmCustomerBooking(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }




    }

    @Override
    public int getItemCount() {
        if (driverDetailses != null){
            return driverDetailses.size();
        } else {
            return customerRequests.size();
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ExpandableRelativeLayout expandableLinearLayout;
        LinearLayout rootNotExpandable;
        TextView textViewPickup, textViewDrop, textViewDistance, textViewFare, textViewName, textViewAge,
                textViewCustID, textViewCustRequestID, textViewDriverRequestID, textViewDriverID, textViewConfirmButton,textViewReachTime,textViewBikeNo;
        CircleImageView circleImageViewProfilePic;

        public ViewHolder(View itemView) {
            super(itemView);

            rootNotExpandable = (LinearLayout) itemView.findViewById(R.id.root_non_expandable_item_customer_request);
            expandableLinearLayout = (ExpandableRelativeLayout) itemView.findViewById(R.id.root_expandable_item_customer_request);
            textViewAge = (TextView) itemView.findViewById(R.id.driver_age);
            textViewDistance = (TextView) itemView.findViewById(R.id.distance_show);
            textViewDrop = (TextView) itemView.findViewById(R.id.drop_address);
            textViewPickup = (TextView) itemView.findViewById(R.id.pick_up_address);
            textViewName = (TextView) itemView.findViewById(R.id.driver_name);
            textViewFare = (TextView) itemView.findViewById(R.id.earning_show);
            circleImageViewProfilePic = (CircleImageView) itemView.findViewById(R.id.driver_profile_pic);
            textViewCustRequestID = (TextView) itemView.findViewById(R.id.cust_request_id);
            textViewConfirmButton = (TextView) itemView.findViewById(R.id.confirm_button);
            textViewDriverID = (TextView) itemView.findViewById(R.id.driver_id);
            textViewDriverRequestID = (TextView) itemView.findViewById(R.id.driver_request_id);
            textViewReachTime=(TextView)itemView.findViewById(R.id.textView_reachtime);
            textViewBikeNo=(TextView)itemView.findViewById(R.id.textViewBikeNo_request_adapter);


        }
    }
}
