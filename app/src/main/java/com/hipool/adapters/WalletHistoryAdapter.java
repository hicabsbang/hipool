package com.hipool.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hipool.R;
import com.hipool.models.WalletHistory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Abhinav on 29/09/2017.
 */

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.ViewHolder> {
    private List<WalletHistory> walletHistories=new ArrayList<>();
    Context context;

    public WalletHistoryAdapter(List<WalletHistory> walletHistories, Context context)
    {
        this.walletHistories=walletHistories;
        this.context=context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.content_show_trans_history,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM, yyyy HH:mm");

        WalletHistory walletHistory=walletHistories.get(position);

        holder.textViewOrderId.setText(walletHistory.getOrderId());
        if(walletHistory.getStatus()==2)
        {
            holder.textViewStatus.setText("CR");
            holder.textViewFrom.setText("Added from Trip");
        }

        else
        {
            holder.textViewStatus.setText("DR");
            holder.textViewFrom.setText("Debit for Trip");
        }


        holder.textViewAmount.setText("₹"+String.format("%.2f",walletHistory.getCreditHistory()));
        try {
            Date date = dateFormat.parse(walletHistory.getCreatedDate());
            holder.textViewDate.setText(date.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(walletHistory.getComingFrom()==1)
            holder.textViewFrom.setText("Added from PayTM");
        else if(walletHistory.getComingFrom()==2)
            holder.textViewFrom.setText("Added from PhonePe");
        else if(walletHistory.getComingFrom()==1)
            holder.textViewFrom.setText("Added from Credit/Debit card");




    }

    @Override
    public int getItemCount() {
        return walletHistories.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewOrderId, textViewStatus,textViewAmount,textViewDate,textViewFrom;
        public ViewHolder(View itemView) {
            super(itemView);
            textViewOrderId=(TextView)itemView.findViewById(R.id.textViewShoworderID);
            textViewStatus=(TextView)itemView.findViewById(R.id.textViewShowStatus);
            textViewAmount=(TextView)itemView.findViewById(R.id.textViewShowAmounts);
            textViewDate=(TextView)itemView.findViewById(R.id.textViewDateTime);
            textViewFrom=(TextView)itemView.findViewById(R.id.textViewFrom);
        }
    }
}
