package com.hipool.settings;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hipool.R;
import com.hipool.SplashActivity;
import com.hipool.service.BookingService;

import static com.hipool.utils.Constants.CUST_ID;
import static com.hipool.utils.Constants.KEY_IS_LOGGED_IN;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_activity_settings);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ((RelativeLayout) findViewById(R.id.vehicle_info_content_settings)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), CarDetailsActivity.class));
            }
        });

        ((RelativeLayout) findViewById(R.id.personal_info_content_settings)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PersonalInfoActivity.class));
            }
        });

        TextView btnSignOut = (TextView) findViewById(R.id.btn_signout_content_settings);
        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BookingService.getInstance().updateCustomerLoginStatus(0);
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putBoolean(KEY_IS_LOGGED_IN, false).apply();
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putInt(CUST_ID, 0).apply();
                startActivity(new Intent(getApplicationContext(), SplashActivity.class));
                finishAffinity();
            }
        });


    }

}
