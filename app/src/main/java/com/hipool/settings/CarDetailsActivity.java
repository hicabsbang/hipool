package com.hipool.settings;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.app.BaseActivity;
import com.hipool.app.MyApp;
import com.hipool.login.LoginActivity;
import com.hipool.models.BankDetail;
import com.hipool.models.CustomerDetails;
import com.hipool.utils.Prefs;
import com.hipool.utils.Utils;
import com.hipool.utils.VolleyMultipartRequest;
import com.iceteck.silicompressorr.SiliCompressor;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.hipool.utils.APIConstants.ADD_CAR_DRIVER_DETAIL_URL;
import static com.hipool.utils.APIConstants.BASE_URL;
import static com.hipool.utils.Constants.APP_PREFS;
import static com.hipool.utils.Constants.CUST_DETAILS;
import static com.hipool.utils.Constants.IS_DEBUG;

public class CarDetailsActivity extends BaseActivity {


    private static final String TAG = CarDetailsActivity.class.getSimpleName();
    private ImageView insuranceCopy, rcCopy, dlCopy, vehicleCopy, cancelledChequeCopy, panCopy;
    private EditText inputIfsc, inputBankName, inputBankBranch, inputAccountNumber, inputPanNumber;
    private TextView btnSubmit;
    private ArrayList<Image> images = new ArrayList<>();

    private static final int REQUEST_CODE_PICK_IMAGE_INSURANCE = 201;
    private static final int REQUEST_CODE_PICK_IMAGE_RC = 202;
    private static final int REQUEST_CODE_PICK_IMAGE_DL = 203;
    private static final int REQUEST_CODE_PICK_IMAGE_VEHICLE = 204;
    private static final int REQUEST_CODE_PICK_IMAGE_CHEQUE = 205;
    private static final int REQUEST_CODE_PICK_IMAGE_PAN = 206;
    private Uri insuranceCopyUri, rcCopyUri, dlCopyUri, vehicleCopyUri, cancelledChequeCopyUri, panCopyUri;
    private CustomerDetails customerDetails;
    private boolean isInsuranceCopyChanged, isRCCopyChange, isDLCopyChanged, isVehicleCopyChanged, isCancelledChequeCopyChanged, isPANChanged, isIFSCChanged, isAccountNoChanged, isBankNameChanged, isBranchNameChanged, isPanNoCahnged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_car_details);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Car Details");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        initViews();

        customerDetails = new Gson().fromJson(getSharedPreferences(APP_PREFS, MODE_PRIVATE).getString(CUST_DETAILS, ""), CustomerDetails.class);



        insuranceCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage(REQUEST_CODE_PICK_IMAGE_INSURANCE);
            }
        });

        Picasso.with(getApplicationContext()).load(customerDetails.getInsuranceCopy()).placeholder(R.drawable.insurance).error(R.drawable.insurance).into(insuranceCopy);

        rcCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pickImage(REQUEST_CODE_PICK_IMAGE_RC);
            }
        });

        Picasso.with(getApplicationContext()).load(customerDetails.getRegistartionCopy()).placeholder(R.drawable.registration).error(R.drawable.registration).into(rcCopy);


        dlCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pickImage(REQUEST_CODE_PICK_IMAGE_DL);
            }
        });

        Picasso.with(getApplicationContext()).load(customerDetails.getDlCopy()).placeholder(R.drawable.dlimage).error(R.drawable.dlimage).into(dlCopy);


        vehicleCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pickImage(REQUEST_CODE_PICK_IMAGE_VEHICLE);
            }
        });

        Picasso.with(getApplicationContext()).load(customerDetails.getVehicleCopy()).placeholder(R.drawable.carimage).error(R.drawable.carimage).into(vehicleCopy);


        cancelledChequeCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pickImage(REQUEST_CODE_PICK_IMAGE_CHEQUE);
            }
        });

//        Picasso.with(getApplicationContext()).load(customerDetails.getC()).placeholder(R.drawable.ch).error(R.drawable.dlimage).into(dlCopy);


        panCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pickImage(REQUEST_CODE_PICK_IMAGE_PAN);
            }
        });

        Picasso.with(getApplicationContext()).load(customerDetails.getCustPanCopy()).placeholder(R.drawable.pancard).error(R.drawable.pancard).into(panCopy);


        inputIfsc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                String ifsc = inputIfsc.getText().toString();
                if (!b) {
                    if (ifsc.length() == 11) {
                        getBankDetails(ifsc);
                    } else {
                        Toast.makeText(getApplicationContext(), "enterl valid 11 digit IFSC..", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

//        inputIfsc.setText(customerDetails.getIFSC);

        inputIfsc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                isIFSCChanged = true;
            }
        });

        inputAccountNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                isAccountNoChanged = true;
            }
        });

//        inputAccountNumber.setText(customerDetails.getA);

        inputBankName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                isBankNameChanged = true;
            }
        });

        inputBankBranch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                isBranchNameChanged = true;
            }
        });

        inputPanNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                isPanNoCahnged = true;
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (insuranceCopyUri == null) {
                    Toast.makeText(getApplicationContext(), "select insurance copy..", Toast.LENGTH_LONG).show();
                } else if (rcCopyUri == null) {
                    Toast.makeText(getApplicationContext(), "select rc copy..", Toast.LENGTH_LONG).show();
                } else if (dlCopyUri == null) {
                    Toast.makeText(getApplicationContext(), "select dl copy..", Toast.LENGTH_LONG).show();
                } else if (vehicleCopyUri == null) {
                    Toast.makeText(getApplicationContext(), "select vehicle copy..", Toast.LENGTH_LONG).show();
                } else if (cancelledChequeCopyUri == null) {
                    Toast.makeText(getApplicationContext(), "select cancelled cheque copy..", Toast.LENGTH_LONG).show();
                } else if (panCopyUri == null) {
                    Toast.makeText(getApplicationContext(), "select PAN Card copy..", Toast.LENGTH_LONG).show();
                } else if (inputIfsc.getText().toString().length() != 11) {
                    Toast.makeText(getApplicationContext(), "enter valid IFSC..", Toast.LENGTH_LONG).show();
                } else if (inputBankName.getText().toString().length() < 1) {
                    Toast.makeText(getApplicationContext(), "enter valid Bank Name..", Toast.LENGTH_LONG).show();
                } else if (inputBankBranch.getText().toString().length() < 1) {
                    Toast.makeText(getApplicationContext(), "enter valid Branch Name..", Toast.LENGTH_LONG).show();
                } else if (inputAccountNumber.getText().toString().length() < 1) {
                    Toast.makeText(getApplicationContext(), "enter valid Account Number..", Toast.LENGTH_LONG).show();
                } else if (inputPanNumber.getText().toString().length() != 10) {
                    Toast.makeText(getApplicationContext(), "enter valid PAN..", Toast.LENGTH_LONG).show();
                } else {
                    sendDataToServer();
                }
            }
        });
    }

    private void initViews() {
        insuranceCopy = (ImageView) findViewById(R.id.image_insurance_content_car_details);
        rcCopy = (ImageView) findViewById(R.id.image_rc_content_car_details);
        dlCopy = (ImageView) findViewById(R.id.image_dl_content_car_details);
        vehicleCopy = (ImageView) findViewById(R.id.image_vehicle_content_car_details);
        cancelledChequeCopy = (ImageView) findViewById(R.id.image_cancelled_cheque_content_car_details);
        panCopy = (ImageView) findViewById(R.id.image_pan_content_car_details);
        btnSubmit = (TextView) findViewById(R.id.btn_submit_content_car_details);
        inputIfsc = (EditText) findViewById(R.id.input_ifsc_content_car_details);
        inputBankName = (EditText) findViewById(R.id.input_bank_name_content_car_details);
        inputBankBranch = (EditText) findViewById(R.id.input_branch_name_content_car_details);
        inputAccountNumber = (EditText) findViewById(R.id.input_account_number_content_car_details);
        inputPanNumber = (EditText) findViewById(R.id.input_pan_number_content_car_details);
    }

    private void pickImage(int requestCode) {
        ImagePicker imagePicker = ImagePicker.create(this)
                .theme(R.style.ImagePickerTheme)
                .returnAfterFirst(true) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                .folderMode(false) // set folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select"); // image selection title

        imagePicker.single();

        images.clear();

        imagePicker.limit(1) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .origin(images) // original selected images, used in multi mode
                .start(requestCode); // start image picker activity with request code
    }

    private void getBankDetails(String ifsc){

        showProgressDialog();

        StringRequest request = new StringRequest(Request.Method.GET, "https://ifsc.razorpay.com/" + ifsc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("Not Found")){
                    Toast.makeText(getApplicationContext(), "invalid IFSC code..", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        final BankDetail bankDetail = new Gson().fromJson(response, BankDetail.class);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                inputBankName.setText(bankDetail.getBANK());
                                inputBankBranch.setText(bankDetail.getBRANCH() + " " + bankDetail.getCITY());
                                hideProgressDialog();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });
            }
        });

        request.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(request);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_PICK_IMAGE_INSURANCE:
                if (resultCode == RESULT_OK && data != null) {
                    isInsuranceCopyChanged = true;
                    images = (ArrayList<Image>) ImagePicker.getImages(data);
                    Log.v(TAG, "images:" + images.get(0).getName() + ":" + images.get(0).getPath());
                    insuranceCopyUri = Uri.fromFile(new File(images.get(0).getPath()));
                    try {
                        String filePath = SiliCompressor.with(getApplicationContext()).compress(insuranceCopyUri.toString(), new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getPackageName() + "/media/images"));
//                        Log.v(TAG, "dsds:" + filePath);
                        if (filePath != null) {
                            insuranceCopyUri = Uri.fromFile(new File(filePath));
                            if (insuranceCopyUri != null) {
                                insuranceCopy.setImageURI(insuranceCopyUri);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case REQUEST_CODE_PICK_IMAGE_RC:
                if (resultCode == RESULT_OK && data != null) {
                    isRCCopyChange = true;
                    images = (ArrayList<Image>) ImagePicker.getImages(data);
                    Log.v(TAG, "images:" + images.get(0).getName() + ":" + images.get(0).getPath());
                    rcCopyUri = Uri.fromFile(new File(images.get(0).getPath()));
                    try {
                        String filePath = SiliCompressor.with(getApplicationContext()).compress(insuranceCopyUri.toString(), new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getPackageName() + "/media/images"));
//                        Log.v(TAG, "dsds:" + filePath);
                        if (filePath != null) {
                            rcCopyUri = Uri.fromFile(new File(filePath));
                            if (rcCopyUri != null) {
                                rcCopy.setImageURI(rcCopyUri);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;


            case REQUEST_CODE_PICK_IMAGE_DL:
                if (resultCode == RESULT_OK && data != null) {
                    isDLCopyChanged = true;
                    images = (ArrayList<Image>) ImagePicker.getImages(data);
                    Log.v(TAG, "images:" + images.get(0).getName() + ":" + images.get(0).getPath());
                    dlCopyUri = Uri.fromFile(new File(images.get(0).getPath()));
                    try {
                        String filePath = SiliCompressor.with(getApplicationContext()).compress(insuranceCopyUri.toString(), new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getPackageName() + "/media/images"));
//                        Log.v(TAG, "dsds:" + filePath);
                        if (filePath != null) {
                            dlCopyUri = Uri.fromFile(new File(filePath));
                            if (dlCopyUri != null) {
                                dlCopy.setImageURI(dlCopyUri);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case REQUEST_CODE_PICK_IMAGE_VEHICLE:
                if (resultCode == RESULT_OK && data != null) {
                    isVehicleCopyChanged = true;
                    images = (ArrayList<Image>) ImagePicker.getImages(data);
                    Log.v(TAG, "images:" + images.get(0).getName() + ":" + images.get(0).getPath());
                    vehicleCopyUri = Uri.fromFile(new File(images.get(0).getPath()));
                    try {
                        String filePath = SiliCompressor.with(getApplicationContext()).compress(insuranceCopyUri.toString(), new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getPackageName() + "/media/images"));
//                        Log.v(TAG, "dsds:" + filePath);
                        if (filePath != null) {
                            vehicleCopyUri = Uri.fromFile(new File(filePath));

                            if (vehicleCopyUri != null) {
                                vehicleCopy.setImageURI(vehicleCopyUri);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;


            case REQUEST_CODE_PICK_IMAGE_CHEQUE:
                if (resultCode == RESULT_OK && data != null) {
                    isCancelledChequeCopyChanged = true;
                    images = (ArrayList<Image>) ImagePicker.getImages(data);
                    Log.v(TAG, "images:" + images.get(0).getName() + ":" + images.get(0).getPath());
                    cancelledChequeCopyUri = Uri.fromFile(new File(images.get(0).getPath()));

                    try {
                        String filePath = SiliCompressor.with(getApplicationContext()).compress(insuranceCopyUri.toString(), new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getPackageName() + "/media/images"));
//                        Log.v(TAG, "dsds:" + filePath);
                        if (filePath != null) {
                            cancelledChequeCopyUri = Uri.fromFile(new File(filePath));

                            if (cancelledChequeCopyUri != null) {
                                cancelledChequeCopy.setImageURI(cancelledChequeCopyUri);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case REQUEST_CODE_PICK_IMAGE_PAN:
                if (resultCode == RESULT_OK && data != null) {
                    isPANChanged = true;
                    images = (ArrayList<Image>) ImagePicker.getImages(data);
                    Log.v(TAG, "images:" + images.get(0).getName() + ":" + images.get(0).getPath());
                    panCopyUri = Uri.fromFile(new File(images.get(0).getPath()));
                    try {
                        String filePath = SiliCompressor.with(getApplicationContext()).compress(insuranceCopyUri.toString(), new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getPackageName() + "/media/images"));
//                        Log.v(TAG, "dsds:" + filePath);
                        if (filePath != null) {
                            panCopyUri = Uri.fromFile(new File(filePath));
                            if (panCopyUri != null) {
                                panCopy.setImageURI(panCopyUri);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            default:
                break;
        }


    }

    private void sendDataToServer() {
        showProgressDialog();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, BASE_URL + ADD_CAR_DRIVER_DETAIL_URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                if (IS_DEBUG) {
                    Log.v(TAG, "cardetails:" + resultResponse);
                }

                if (resultResponse.toLowerCase().contains("success")){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            Toast.makeText(getApplicationContext(), "success!, please login..", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            finish();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            Toast.makeText(getApplicationContext(), "failed!", Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (IS_DEBUG) {
                    Log.e(TAG, "cardetails:" + error.toString());
                }


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                Map<String, DataPart> params = new HashMap<>();
                try {

                    if (isInsuranceCopyChanged){
                        params.put("insurance", new DataPart("insurance.jpg", Utils.getBytes(getApplicationContext(), insuranceCopyUri), "image/jpg"));

                    }

                    if (isRCCopyChange) {
                        params.put("registration", new DataPart("registration.jpg", Utils.getBytes(getApplicationContext(), rcCopyUri), "image/jpg"));

                    }

                    if (isDLCopyChanged) {
                        params.put("dl_copy", new DataPart("dl_copy.jpg", Utils.getBytes(getApplicationContext(), dlCopyUri), "image/jpg"));

                    }

                    if (isVehicleCopyChanged) {
                        params.put("vehicle_copy", new DataPart("vehicle_copy.jpg", Utils.getBytes(getApplicationContext(), vehicleCopyUri), "image/jpg"));

                    }

                    if (isPANChanged) {
                        params.put("pan_card", new DataPart("pan_card.jpg", Utils.getBytes(getApplicationContext(), panCopyUri), "image/jpg"));

                    }

                    if (isCancelledChequeCopyChanged) {
                        params.put("cheque", new DataPart("cheque.jpg", Utils.getBytes(getApplicationContext(), cancelledChequeCopyUri), "image/jpg"));

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_contact_no", Prefs.getMobile(getApplicationContext()));
                params.put("cust_id", Prefs.getUserId(getApplicationContext()));
                if (isPanNoCahnged) {
                    params.put("pan_number", inputPanNumber.getText().toString());

                }

                if (isBankNameChanged) {
                    params.put("bank_name", inputBankName.getText().toString());

                }

                if (isIFSCChanged) {
                    params.put("bank_ifsc_code", inputIfsc.getText().toString());

                }

                if (isBranchNameChanged) {
                    params.put("bank_branch", inputBankBranch.getText().toString());

                }

                if (isAccountNoChanged) {
                    params.put("account_no", inputAccountNumber.getText().toString());

                }
                return params;
            }
        };

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                -1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        volleyMultipartRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(volleyMultipartRequest);
    }

}
