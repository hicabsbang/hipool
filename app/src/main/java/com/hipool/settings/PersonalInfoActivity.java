package com.hipool.settings;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.app.BaseActivity;
import com.hipool.app.MyApp;
import com.hipool.login.LoginActivity;
import com.hipool.models.CustomerDetails;
import com.hipool.utils.CalendarUtils;
import com.hipool.utils.Prefs;
import com.hipool.utils.Utils;
import com.hipool.utils.VolleyMultipartRequest;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.hipool.utils.APIConstants.ADD_PROFILE_URL;
import static com.hipool.utils.APIConstants.BASE_URL;
import static com.hipool.utils.Constants.APP_PREFS;
import static com.hipool.utils.Constants.CUST_DETAILS;
import static com.hipool.utils.Constants.IS_DEBUG;

public class PersonalInfoActivity extends BaseActivity {

    private static final String TAG = PersonalInfoActivity.class.getSimpleName();
    private static final int REQUEST_CODE_PICK_IMAGE = 201;
    private static final int REQUEST_CODE_PICK_IMAGE_ID = 202;
    private ArrayList<Image> images = new ArrayList<>();
    private ArrayList<Image> imagesId = new ArrayList<>();
    private Uri finalImageUri;
    private CircleImageView profileImage;
    private RadioGroup radioGroupGender,radioGroupAddress;
    private String gender = "";
    private EditText inputName, inputEmail, inputDOB;
//    private CheckBox toAddCar;
    private TextView btnSubmit;
    private SimpleDateFormat dateFormat;
    private DatePickerDialog datePickerDialog;
    private ImageView imageViewId;
    private CustomerDetails customerDetails;
    private boolean isProfilePickChanged, isGenderChanged, isNameChanged, isIdChanged, isEmailChange, isDOBChanged;
    private ImageView btnEditName, btnEditEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info);

        dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        customerDetails = new Gson().fromJson(getSharedPreferences(APP_PREFS, MODE_PRIVATE).getString(CUST_DETAILS, ""), CustomerDetails.class);


        initViews();


        inputName.setText(customerDetails.getCustFirstName());
        inputName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                isNameChanged = true;
            }
        });

        inputEmail.setText(customerDetails.getCustEmail());
        inputEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                isEmailChange = true;
            }
        });



        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage();
            }
        });

        Picasso.with(getApplicationContext()).load(customerDetails.getCustProfilePic()).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(profileImage);


        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int radioButtonID = radioGroup.getCheckedRadioButtonId();
                switch (radioButtonID) {
                    case R.id.radio_btn_male_gender_content_personal_details: gender = "Mr.";

                        isGenderChanged = true;
                        break;

                    case R.id.radio_btn_female_gender_content_personal_details: gender = "Mrs.";

                        isGenderChanged = true;
                        break;

                    default: gender = "";
                        break;
                }
            }
        });

        if (customerDetails.getCustGender() == 0) {
            ((RadioButton) findViewById(R.id.radio_btn_male_gender_content_personal_details)).setChecked(true);
        } else {
            ((RadioButton) findViewById(R.id.radio_btn_female_gender_content_personal_details)).setChecked(true);
        }


        radioGroupAddress.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int id=radioGroup.getCheckedRadioButtonId();
                switch (id)
                {
                    case R.id.addharcard:
                        imageViewId.setImageResource(R.drawable.addharcard);
                        break;
                    case R.id.votercard:
                        imageViewId.setImageResource(R.drawable.voterid);
                        break;
                    case R.id.dlcopy:
                        imageViewId.setImageResource(R.drawable.dlimage);
                        break;

                }
            }
        });

        if (customerDetails.getAddressType() == 0){
            ((RadioButton) findViewById(R.id.addharcard)).setChecked(true);
        } else if (customerDetails.getAddressType() == 1){
            ((RadioButton) findViewById(R.id.votercard)).setChecked(true);
        } else if (customerDetails.getAddressType() == 2){
            ((RadioButton) findViewById(R.id.dlcopy)).setChecked(true);
        }



        inputDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDOBClicked();
            }
        });

        inputDOB.setText(customerDetails.getCustDob());

        imageViewId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImageId();
            }
        });

        Picasso.with(getApplicationContext()).load(customerDetails.getCustAddressCopy()).into(imageViewId);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (images.size() < 1) {
                    Toast.makeText(getApplicationContext(), "select profile image", Toast.LENGTH_LONG).show();
                } else if (gender.equals("")) {
                    Toast.makeText(getApplicationContext(), "select gender", Toast.LENGTH_LONG).show();
                } else if (inputName.getText().toString().length() < 2) {
                    Toast.makeText(getApplicationContext(), "enter valid name", Toast.LENGTH_LONG).show();
                } else if (inputEmail.getText().toString().length() < 4 || !inputEmail.getText().toString().contains("@") || !inputEmail.getText().toString().contains(".")) {
                    Toast.makeText(getApplicationContext(), "enter valid email", Toast.LENGTH_LONG).show();
                } else if (inputDOB.getText().toString().length() < 5) {
                    Toast.makeText(getApplicationContext(), "enter dob", Toast.LENGTH_LONG).show();
                } else if (imagesId.size() < 1) {
                    Toast.makeText(getApplicationContext(), "select id proof image", Toast.LENGTH_LONG).show();
                } else {
                    sendDataToServer();
                }
            }
        });

    }

    private void initViews() {
        profileImage = (CircleImageView) findViewById(R.id.profile_image_content_personal_details);
        radioGroupGender = (RadioGroup) findViewById(R.id.radiogroup_gender_content_personal_details);
        inputName = (EditText) findViewById(R.id.input_name_content_personal_details);
        inputEmail = (EditText) findViewById(R.id.input_email_content_personal_details);
        inputDOB = (EditText) findViewById(R.id.input_dob_content_personal_details);
        imageViewId = (ImageView) findViewById(R.id.image_idproof_content_personal_details);
//        toAddCar = (CheckBox) findViewById(R.id.checkbox_add_car_content_personal_details);
        btnSubmit = (TextView) findViewById(R.id.btn_submit_content_personal_details);
        radioGroupAddress=(RadioGroup)findViewById(R.id.radio_group_address);
        Utils.addSpaceFilter(inputDOB);
        Utils.addSpaceFilter(inputEmail);
        inputName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(25)});
        btnEditName = (ImageView) findViewById(R.id.btn_edit_name_content_personal_info);
        btnEditName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputName.setEnabled(true);
            }
        });
        btnEditEmail = (ImageView) findViewById(R.id.btn_edit_email_content_personal_info);
        btnEditEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputEmail.setEnabled(true);
            }
        });
    }

    private void pickImage() {
        ImagePicker imagePicker = ImagePicker.create(this)
                .theme(R.style.ImagePickerTheme)
                .returnAfterFirst(true) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                .folderMode(false) // set folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select"); // image selection title

        imagePicker.single();

        imagePicker.limit(1) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .origin(images) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICK_IMAGE); // start image picker activity with request code
    }

    private void pickImageId() {
        ImagePicker imagePicker = ImagePicker.create(this)
                .theme(R.style.ImagePickerTheme)
                .returnAfterFirst(true) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                .folderMode(false) // set folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select"); // image selection title

        imagePicker.single();

        imagePicker.limit(1) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .origin(imagesId) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICK_IMAGE_ID); // start image picker activity with request code
    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = "tnh_" + Utils.getUnixTime() + ".png";

//        Log.e(TAG, "uris:" + uri + ":" + destinationFileName);

        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop.withAspectRatio(1, 1);
        uCrop.withMaxResultSize(500, 500);

        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.PNG);

        options.setCompressionQuality(100);

        uCrop.start(this);
    }


    private void onDOBClicked() {
        Calendar calendar = Calendar.getInstance();
        if (inputDOB.getText().toString().length() == 10) {
            try {
                calendar.setTime(dateFormat.parse(inputDOB.getText().toString()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        datePickerDialog = new DatePickerDialog(PersonalInfoActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar date = Calendar.getInstance();
                date.set(i, i1, i2);
                if (CalendarUtils.isPastDate(dateFormat.format(date.getTime()))) {
                    inputDOB.setText(dateFormat.format(date.getTime()));

                    isDOBChanged = true;
                } else {
                    Toast.makeText(PersonalInfoActivity.this, "enter valid birthdate..", Toast.LENGTH_LONG).show();
                }
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case UCrop.REQUEST_CROP:
                if (resultCode == RESULT_OK && data != null) {
                    finalImageUri = UCrop.getOutput(data);
                    Log.v(TAG, "imageURI : " + finalImageUri);
                    profileImage.setImageURI(finalImageUri);
                } else if (resultCode == UCrop.RESULT_ERROR) {
                    final Throwable cropError = UCrop.getError(data);
                    Toast.makeText(getApplicationContext(), cropError.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
                break;

            case REQUEST_CODE_PICK_IMAGE:
                if (resultCode == RESULT_OK && data != null) {
                    isProfilePickChanged = true;
                    images = (ArrayList<Image>) ImagePicker.getImages(data);
                    Log.v(TAG, "images:" + images.get(0).getName() + ":" + images.get(0).getPath());
                    final Uri selectedUri = Uri.fromFile(new File(images.get(0).getPath()));
                    if (selectedUri != null) {
                        startCropActivity(selectedUri);
                    }
                }
                break;

            case REQUEST_CODE_PICK_IMAGE_ID:
                if (resultCode == RESULT_OK && data != null) {
                    isIdChanged = true;
                    imagesId = (ArrayList<Image>) ImagePicker.getImages(data);
                    Log.v(TAG, "images:" + imagesId.get(0).getName() + ":" + imagesId.get(0).getPath());
                    final Uri selectedUri = Uri.fromFile(new File(imagesId.get(0).getPath()));
                    if (selectedUri != null) {
                        imageViewId.setImageURI(selectedUri);
                    }
                }
                break;

            default:
                break;
        }


    }

    private void sendDataToServer() {
        showProgressDialog();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, BASE_URL + ADD_PROFILE_URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                if (IS_DEBUG) {
                    Log.v(TAG, "profile:" + resultResponse);
                }

                if (resultResponse.contains("Successfully")){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
//                            if (toAddCar.isChecked()){
//                                startActivity(new Intent(getApplicationContext(), CarDetailsActivity.class));
//                            } else {
//                                Toast.makeText(getApplicationContext(), "success!, please login..", Toast.LENGTH_LONG).show();
//                                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
//                            }
                            finish();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            Toast.makeText(getApplicationContext(), "failed!", Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (IS_DEBUG) {
                    Log.e(TAG, "profile:" + error.toString());
                }


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                Map<String, DataPart> params = new HashMap<>();
                try {
                    if (isProfilePickChanged) {
                        params.put("profile_pic", new DataPart("profile_pic.jpg", Utils.getBytes(getApplicationContext(), Uri.fromFile(new File(images.get(0).getPath()))), "image/jpg"));
                    }

                    if (isIdChanged){
                        params.put("address", new DataPart("address.jpg", Utils.getBytes(getApplicationContext(), Uri.fromFile(new File(imagesId.get(0).getPath()))), "image/jpg"));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_contact_no", Prefs.getMobile(getApplicationContext()));
                params.put("cust_id", Prefs.getUserId(getApplicationContext()));
                if (isNameChanged) {
                    params.put("customer_name", inputName.getText().toString());

                }

                if (isEmailChange) {

                    params.put("email_id", inputEmail.getText().toString());
                }


                if (isDOBChanged) {
                    params.put("dob", inputDOB.getText().toString());

                }

                if (isGenderChanged) {

                    params.put("gender", gender);
                }
                return params;
            }
        };

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                -1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        volleyMultipartRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(volleyMultipartRequest);
    }
}
