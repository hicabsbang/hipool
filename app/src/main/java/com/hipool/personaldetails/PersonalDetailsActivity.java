package com.hipool.personaldetails;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.hipool.imagePicker.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.app.BaseActivity;
import com.hipool.app.MyApp;
import com.hipool.login.LoginActivity;
import com.hipool.models.BankDetail;
import com.hipool.utils.CalendarUtils;
import com.hipool.utils.ImageUtils;
import com.hipool.utils.PermissionUtils;
import com.hipool.utils.Prefs;
import com.hipool.utils.Utils;
import com.hipool.utils.VolleyMultipartRequest;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.hipool.utils.APIConstants.ADD_CAR_DRIVER_DETAIL_URL;
import static com.hipool.utils.APIConstants.ADD_PROFILE_URL;
import static com.hipool.utils.APIConstants.BASE_URL;
import static com.hipool.utils.Constants.IS_DEBUG;

public class PersonalDetailsActivity extends BaseActivity {

    private static final String TAG = PersonalDetailsActivity.class.getSimpleName();
    private static final int REQUEST_CODE_PICK_IMAGE = 200;
    private static final int REQUEST_CODE_PICK_IMAGE_ID = 199;
    private ArrayList<Image> images = new ArrayList<>();
    private ArrayList<Image> imagesId = new ArrayList<>();
    private Uri finalImageUri;
    private int whichImage=0;

    private CircleImageView profileImage;
    private RadioGroup radioGroupGender, radioGroupAddress;
    private String gender = "";
    private EditText inputName, inputEmail, inputDOB;
    private CheckBox toAddCar;
    private TextView btnSubmit;
    private SimpleDateFormat dateFormat;
    private DatePickerDialog datePickerDialog;
    private ImageView imageViewId, btnBack;
    private RelativeLayout relativeLayoutShowPersonalList, relativeLayoutShowVecihleList, relativeLayoutBankList
            ,relativeLayoutShowDL,relativeLayoutShowInsurance,relativeLayoutShowRest,relativeLayoutShowBIke,
            relativeLayoutIDUPLOAD,relativeLayoutShowPan,relativeLayoutshowCheque;
    private ImageView imageViewShowPersonal, imageViewShowvehile, imageViewShowBank,imageViewShowDL,imageViewShowInsurance,
            imageViewShowBike,imageViewShowregist,imageViewShowPan,imageViewShowCheque;
    private net.cachapa.expandablelayout.ExpandableLayout expandableLayoutPersonal,expandableLayoutPan,expandableLayoutCheque,
            expandableLayoutInsurance, expandableLayoutDL, expandableLayoutRegistration, expandableLayoutBIkeImage, expandableLayoutBank, expandableLayoutVehicle, expandableLayoutIDupload;

    private ImageView insuranceCopy, rcCopy, dlCopy, vehicleCopy, cancelledChequeCopy, panCopy, imageViewExpandIdUpload;
    private EditText inputIfsc, inputBankName, inputBankBranch, inputAccountNumber, inputPanNumber;
    private TextView btnSubmit1;
    private ArrayList<Image> images1 = new ArrayList<>();

    private static final int REQUEST_CODE_PICK_IMAGE_INSURANCE = 201;
    private static final int REQUEST_CODE_PICK_IMAGE_RC = 202;
    private static final int REQUEST_CODE_PICK_IMAGE_DL = 203;
    private static final int REQUEST_CODE_PICK_IMAGE_VEHICLE = 204;
    private static final int REQUEST_CODE_PICK_IMAGE_CHEQUE = 205;
    private static final int REQUEST_CODE_PICK_IMAGE_PAN = 206;
    private Bitmap insuranceCopyUri=null, rcCopyUri=null, dlCopyUri=null, vehicleCopyUri=null, cancelledChequeCopyUri=null, panCopyUri=null,addressProofBitmap=null,profilePicBitmap=null;

    EditText editTextDLNO, editTextBIKENO;
    private LinearLayout linearLayout_upload_id, linearLayoutPadding;
    private RelativeLayout progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_details);


        dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        initViews();

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage(REQUEST_CODE_PICK_IMAGE,"Choose Profile Pic");
            }
        });

        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int radioButtonID = radioGroup.getCheckedRadioButtonId();
                switch (radioButtonID) {
                    case R.id.radio_btn_male_gender_content_personal_details:
                        gender = "Mr.";
                        break;

                    case R.id.radio_btn_female_gender_content_personal_details:
                        gender = "Ms.";
                        break;

                    default:
                        gender = "";
                        break;
                }
            }
        });

        radioGroupAddress.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int id = radioGroup.getCheckedRadioButtonId();
                switch (id) {
                    case R.id.addharcard:
                        imageViewId.setImageResource(R.drawable.addharcard);
                        imageViewId.setVisibility(View.VISIBLE);
                        break;
                    case R.id.votercard:
                        imageViewId.setImageResource(R.drawable.voterid);
                        imageViewId.setVisibility(View.VISIBLE);
                        break;
                    case R.id.dlcopy:
                        imageViewId.setImageResource(R.drawable.dlimage);
                        imageViewId.setVisibility(View.VISIBLE);
                        break;

                }
            }
        });

        inputDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDOBClicked();
            }
        });

        imageViewId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //pickImageId();
                pickImage(REQUEST_CODE_PICK_IMAGE_ID,"Choose Address Proof");
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (profilePicBitmap==null) {
                    Toast.makeText(getApplicationContext(), "select profile image", Toast.LENGTH_LONG).show();
                    expandableLayoutVehicle.collapse();
                    expandableLayoutBank.collapse();
                    expandableLayoutPersonal.expand();
                    expandableLayoutIDupload.collapse();
                } else if (gender.equals("")) {
                    expandableLayoutVehicle.collapse();
                    expandableLayoutBank.collapse();
                    expandableLayoutPersonal.expand();
                    expandableLayoutIDupload.collapse();
                    Toast.makeText(getApplicationContext(), "select gender", Toast.LENGTH_LONG).show();
                } else if (inputName.getText().toString().length() < 2) {
                    expandableLayoutVehicle.collapse();
                    expandableLayoutBank.collapse();
                    expandableLayoutPersonal.expand();
                    expandableLayoutIDupload.collapse();
                    Toast.makeText(getApplicationContext(), "enter valid name", Toast.LENGTH_LONG).show();
                } else if (inputEmail.getText().toString().length() < 4 || !inputEmail.getText().toString().contains("@") || !inputEmail.getText().toString().contains(".")) {
                    Toast.makeText(getApplicationContext(), "enter valid email", Toast.LENGTH_LONG).show();
                    expandableLayoutVehicle.collapse();
                    expandableLayoutBank.collapse();
                    expandableLayoutPersonal.expand();
                    expandableLayoutIDupload.collapse();
                } else if (inputDOB.getText().toString().length() < 5) {
                    expandableLayoutVehicle.collapse();
                    expandableLayoutBank.collapse();
                    expandableLayoutPersonal.expand();
                    expandableLayoutIDupload.collapse();
                    Toast.makeText(getApplicationContext(), "enter dob", Toast.LENGTH_LONG).show();
                } else if (addressProofBitmap==null) {
                    expandableLayoutVehicle.collapse();
                    expandableLayoutBank.collapse();
                    expandableLayoutPersonal.expand();
                    expandableLayoutIDupload.expand();
                    Toast.makeText(getApplicationContext(), "select id proof image", Toast.LENGTH_LONG).show();
                } else {
                    if (toAddCar.isChecked()) {
                        if(editTextBIKENO.getText().toString().length()<9)
                        {
                            Toast.makeText(getApplicationContext(), "enter valid Bike Number..", Toast.LENGTH_LONG).show();
                            expandableLayoutVehicle.expand();
                            expandableLayoutBank.collapse();
                            expandableLayoutInsurance.collapse();
                            expandableLayoutDL.collapse();
                            expandableLayoutBIkeImage.collapse();
                            expandableLayoutRegistration.collapse();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();

                        }
                        else if(editTextDLNO.getText().toString().length()<14)
                        {
                            Toast.makeText(getApplicationContext(), "enter valid DL Number..", Toast.LENGTH_LONG).show();
                            expandableLayoutVehicle.expand();
                            expandableLayoutBank.collapse();
                            expandableLayoutInsurance.collapse();
                            expandableLayoutDL.collapse();
                            expandableLayoutBIkeImage.collapse();
                            expandableLayoutRegistration.collapse();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();
                        }
                        else if (insuranceCopyUri == null) {
                            Toast.makeText(getApplicationContext(), "select insurance copy..", Toast.LENGTH_LONG).show();
                            expandableLayoutVehicle.expand();
                            expandableLayoutInsurance.expand();
                            expandableLayoutDL.collapse();
                            expandableLayoutBIkeImage.collapse();
                            expandableLayoutRegistration.collapse();
                            expandableLayoutBank.collapse();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();
                        } else if (rcCopyUri == null) {
                            Toast.makeText(getApplicationContext(), "select rc copy..", Toast.LENGTH_LONG).show();
                            expandableLayoutVehicle.expand();
                            expandableLayoutInsurance.collapse();
                            expandableLayoutDL.collapse();
                            expandableLayoutBIkeImage.collapse();
                            expandableLayoutRegistration.expand();
                            expandableLayoutBank.collapse();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();
                        } else if (dlCopyUri == null) {
                            Toast.makeText(getApplicationContext(), "select dl copy..", Toast.LENGTH_LONG).show();
                            expandableLayoutVehicle.expand();
                            expandableLayoutInsurance.collapse();
                            expandableLayoutDL.expand();
                            expandableLayoutBIkeImage.collapse();
                            expandableLayoutRegistration.collapse();
                            expandableLayoutBank.collapse();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();
                        } else if (vehicleCopyUri == null) {
                            Toast.makeText(getApplicationContext(), "select vehicle copy..", Toast.LENGTH_LONG).show();
                            expandableLayoutVehicle.expand();
                            expandableLayoutInsurance.collapse();
                            expandableLayoutDL.collapse();
                            expandableLayoutBIkeImage.expand();
                            expandableLayoutRegistration.collapse();
                            expandableLayoutBank.collapse();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();
                        } else if (cancelledChequeCopyUri == null) {
                            Toast.makeText(getApplicationContext(), "select cancelled cheque copy..", Toast.LENGTH_LONG).show();
                            expandableLayoutVehicle.collapse();
                            expandableLayoutBank.expand();
                            expandableLayoutCheque.expand();
                            expandableLayoutPan.collapse();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();
                        } else if (panCopyUri == null) {
                            Toast.makeText(getApplicationContext(), "select PAN Card copy..", Toast.LENGTH_LONG).show();
                            expandableLayoutVehicle.collapse();
                            expandableLayoutBank.expand();
                            expandableLayoutCheque.collapse();
                            expandableLayoutPan.expand();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();
                        } else if (inputIfsc.getText().toString().length() != 11) {
                            expandableLayoutVehicle.collapse();
                            expandableLayoutBank.expand();
                            expandableLayoutCheque.collapse();
                            expandableLayoutPan.collapse();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();
                            Toast.makeText(getApplicationContext(), "enter valid IFSC..", Toast.LENGTH_LONG).show();
                        } else if (inputBankName.getText().toString().length() < 1) {
                            expandableLayoutVehicle.collapse();
                            expandableLayoutBank.expand();
                            expandableLayoutCheque.collapse();
                            expandableLayoutPan.collapse();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();
                            Toast.makeText(getApplicationContext(), "enter valid Bank Name..", Toast.LENGTH_LONG).show();
                        } else if (inputBankBranch.getText().toString().length() < 1) {
                            expandableLayoutVehicle.collapse();
                            expandableLayoutBank.expand();
                            expandableLayoutCheque.collapse();
                            expandableLayoutPan.collapse();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();
                            Toast.makeText(getApplicationContext(), "enter valid Branch Name..", Toast.LENGTH_LONG).show();
                        } else if (inputAccountNumber.getText().toString().length() < 1) {
                            expandableLayoutVehicle.collapse();
                            expandableLayoutBank.expand();
                            expandableLayoutCheque.collapse();
                            expandableLayoutPan.collapse();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();
                            Toast.makeText(getApplicationContext(), "enter valid Account Number..", Toast.LENGTH_LONG).show();
                        } else if (inputPanNumber.getText().toString().length() != 10) {
                            expandableLayoutVehicle.collapse();
                            expandableLayoutBank.expand();
                            expandableLayoutCheque.collapse();
                            expandableLayoutPan.collapse();
                            expandableLayoutPersonal.collapse();
                            expandableLayoutIDupload.collapse();
                            Toast.makeText(getApplicationContext(), "enter valid PAN..", Toast.LENGTH_LONG).show();
                        } else {
                            sendDataToServer();
                        }
                    } else {
                        sendDataToServer();
                    }

                }
            }
        });

        toAddCar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (toAddCar.isChecked()) {
                    if (profilePicBitmap==null) {
                        Toast.makeText(getApplicationContext(), "select profile image", Toast.LENGTH_LONG).show();
                        toAddCar.setChecked(false);
                    } else if (gender.equals("")) {
                        Toast.makeText(getApplicationContext(), "select gender", Toast.LENGTH_LONG).show();
                        toAddCar.setChecked(false);
                    } else if (inputName.getText().toString().length() < 2) {
                        Toast.makeText(getApplicationContext(), "enter valid name", Toast.LENGTH_LONG).show();
                        toAddCar.setChecked(false);
                    } else if (inputEmail.getText().toString().length() < 4 || !inputEmail.getText().toString().contains("@") || !inputEmail.getText().toString().contains(".")) {
                        Toast.makeText(getApplicationContext(), "enter valid email", Toast.LENGTH_LONG).show();
                        toAddCar.setChecked(false);
                    } else if (inputDOB.getText().toString().length() < 5) {
                        Toast.makeText(getApplicationContext(), "enter dob", Toast.LENGTH_LONG).show();
                        toAddCar.setChecked(false);
                    } else if (addressProofBitmap==null) {
                        expandableLayoutIDupload.expand();
                        Toast.makeText(getApplicationContext(), "select id proof image", Toast.LENGTH_LONG).show();
                        toAddCar.setChecked(false);
                    } else {
                        linearLayoutPadding.setPadding(0, 0, 0, 0);
                        relativeLayoutShowVecihleList.setVisibility(View.VISIBLE);
                        relativeLayoutShowPersonalList.setVisibility(View.VISIBLE);
                        imageViewShowPersonal.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                        imageViewExpandIdUpload.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                        imageViewShowvehile.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                        expandableLayoutPersonal.collapse();
                        expandableLayoutIDupload.collapse();
                        expandableLayoutVehicle.expand();
                        toAddCar.setVisibility(View.GONE);

                    }

                } else {
                    relativeLayoutShowVecihleList.setVisibility(View.GONE);
                    relativeLayoutShowPersonalList.setVisibility(View.GONE);
                    relativeLayoutBankList.setVisibility(View.GONE);
                    imageViewShowPersonal.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowvehile.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                    expandableLayoutPersonal.expand();
                    expandableLayoutVehicle.collapse();
                    expandableLayoutBank.collapse();
                    linearLayoutPadding.setPadding(11, 0, 11, 0);

                }
            }
        });

        relativeLayoutShowPersonalList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayoutPersonal.isExpanded()) {
                    expandableLayoutPersonal.collapse();
                    imageViewShowPersonal.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                } else {
                    expandableLayoutPersonal.expand();
                    expandableLayoutBank.collapse();
                    expandableLayoutVehicle.collapse();
                    expandableLayoutIDupload.collapse();
                    imageViewExpandIdUpload.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowPersonal.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                    imageViewShowvehile.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowBank.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                }
            }
        });
        relativeLayoutBankList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayoutBank.isExpanded()) {
                    expandableLayoutBank.collapse();
                    imageViewShowBank.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                } else {
                    expandableLayoutPersonal.collapse();
                    expandableLayoutBank.expand();
                    expandableLayoutVehicle.collapse();
                    expandableLayoutIDupload.collapse();
                    imageViewExpandIdUpload.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowPersonal.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowvehile.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowBank.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                }
            }
        });

        relativeLayoutShowVecihleList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayoutVehicle.isExpanded()) {
                    expandableLayoutVehicle.collapse();
                    imageViewShowvehile.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                } else {
                    expandableLayoutPersonal.collapse();
                    expandableLayoutBank.collapse();
                    expandableLayoutVehicle.expand();
                    expandableLayoutIDupload.collapse();
                    imageViewExpandIdUpload.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowPersonal.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowvehile.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                    imageViewShowBank.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                }
            }
        });
        insuranceCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage(REQUEST_CODE_PICK_IMAGE_INSURANCE,"Choose Insurance Copy");


            }
        });

        rcCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pickImage(REQUEST_CODE_PICK_IMAGE_RC,"Choose Registration Copy");

            }
        });

        dlCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pickImage(REQUEST_CODE_PICK_IMAGE_DL,"Choose DL Copy");

            }
        });

        vehicleCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pickImage(REQUEST_CODE_PICK_IMAGE_VEHICLE,"Choose Bike Copy");

            }
        });

        cancelledChequeCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pickImage(REQUEST_CODE_PICK_IMAGE_CHEQUE,"Choose Cancel Cheque Copy");
            }
        });

        panCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pickImage(REQUEST_CODE_PICK_IMAGE_PAN,"Choose Pan Copy");
            }
        });

        inputIfsc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                String ifsc = inputIfsc.getText().toString();
                if (!b) {
                    if (ifsc.length() == 11) {
                        getBankDetails(ifsc);
                    } else {
                        Toast.makeText(getApplicationContext(), "enterl valid 11 digit IFSC..", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        });
        editTextDLNO.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    if (editTextDLNO.getText().toString().length() < 15)
                        Toast.makeText(getApplicationContext(), "Please Enter Correct DL number", Toast.LENGTH_SHORT).show();
                    else {
                        if (!editTextBIKENO.getText().toString().equals("") && !editTextDLNO.getText().toString().equals("") && insuranceCopyUri != null
                                && rcCopyUri != null && dlCopyUri != null && vehicleCopyUri != null) {
                            relativeLayoutBankList.setVisibility(View.VISIBLE);
                            expandableLayoutVehicle.collapse();
                            expandableLayoutBank.expand();
                            imageViewShowvehile.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                            imageViewShowBank.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                        }
                    }
                }

            }
        });
        editTextBIKENO.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    if (editTextBIKENO.getText().toString().length() <= 10)
                        Toast.makeText(getApplicationContext(), "Please Enter Correct BikeNO number", Toast.LENGTH_SHORT).show();
                    else {
                        if (!editTextBIKENO.getText().toString().equals("") && !editTextDLNO.getText().toString().equals("") && insuranceCopyUri != null
                                && rcCopyUri != null && dlCopyUri != null && vehicleCopyUri != null) {
                            relativeLayoutBankList.setVisibility(View.VISIBLE);
                            expandableLayoutVehicle.collapse();
                            expandableLayoutBank.expand();
                            imageViewShowvehile.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                            imageViewShowBank.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                        }
                    }
                }

            }
        });

        relativeLayoutIDUPLOAD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayoutIDupload.isExpanded()) {
                    expandableLayoutIDupload.collapse();
                    imageViewExpandIdUpload.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                } else {
                    expandableLayoutIDupload.expand();
                    imageViewExpandIdUpload.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                }
            }
        });

        relativeLayoutShowDL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(expandableLayoutDL.isExpanded())
                {
                    expandableLayoutDL.collapse();
                    imageViewShowDL.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                }
                else
                {
                    expandableLayoutDL.expand();
                    expandableLayoutBIkeImage.collapse();
                    expandableLayoutRegistration.collapse();
                    expandableLayoutInsurance.collapse();

                    imageViewShowDL.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                    imageViewShowregist.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowInsurance.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowBike.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                }
            }
        });
        relativeLayoutShowRest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(expandableLayoutRegistration.isExpanded())
                {
                    expandableLayoutRegistration.collapse();
                    imageViewShowregist.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                }
                else
                {
                    expandableLayoutDL.collapse();
                    expandableLayoutBIkeImage.collapse();
                    expandableLayoutRegistration.expand();
                    expandableLayoutInsurance.collapse();

                    imageViewShowDL.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowregist.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                    imageViewShowInsurance.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowBike.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                }
            }
        });

        relativeLayoutShowInsurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(expandableLayoutInsurance.isExpanded())
                {
                    expandableLayoutInsurance.collapse();
                    imageViewShowInsurance.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                }
                else
                {
                    expandableLayoutDL.collapse();
                    expandableLayoutBIkeImage.collapse();
                    expandableLayoutRegistration.collapse();
                    expandableLayoutInsurance.expand();

                    imageViewShowDL.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowregist.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowInsurance.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                    imageViewShowBike.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                }
            }
        });
        relativeLayoutShowBIke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(expandableLayoutBIkeImage.isExpanded())
                {
                    expandableLayoutBIkeImage.collapse();
                    imageViewShowBike.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                }
                else
                {
                    expandableLayoutDL.collapse();
                    expandableLayoutBIkeImage.expand();
                    expandableLayoutRegistration.collapse();
                    expandableLayoutInsurance.collapse();

                    imageViewShowDL.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowregist.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowInsurance.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowBike.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                }
            }
        });

        relativeLayoutShowPan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(expandableLayoutPan.isExpanded())
                {
                    expandableLayoutPan.collapse();
                    imageViewShowPan.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                }
                else
                {
                    expandableLayoutPan.expand();
                    expandableLayoutCheque.collapse();
                    imageViewShowPan.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                    imageViewShowCheque.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                }
            }
        });
        relativeLayoutshowCheque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(expandableLayoutCheque.isExpanded())
                {
                    expandableLayoutCheque.collapse();
                    imageViewShowCheque.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                }
                else
                {
                    expandableLayoutPan.collapse();
                    expandableLayoutCheque.expand();
                    imageViewShowPan.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                    imageViewShowCheque.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                }
            }
        });


    }

    private void checkVehicleDetail() {
        if (!editTextBIKENO.getText().toString().equals("") && !editTextDLNO.getText().toString().equals("") && insuranceCopyUri != null
                && rcCopyUri != null && dlCopyUri != null && vehicleCopyUri != null) {
            relativeLayoutBankList.setVisibility(View.VISIBLE);
            expandableLayoutVehicle.collapse();
            expandableLayoutBank.expand();
            imageViewShowvehile.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
            imageViewShowBank.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
        }
    }

    private void initViews() {
        relativeLayoutBankList = (RelativeLayout) findViewById(R.id.tab_bank_details_first_joinus);
        relativeLayoutShowPersonalList = (RelativeLayout) findViewById(R.id.tab_personal_details_first_joinus);
        relativeLayoutShowVecihleList = (RelativeLayout) findViewById(R.id.tab_Cab_details_first_joinus);
        relativeLayoutShowBIke=(RelativeLayout)findViewById(R.id.tab_bikeimg_details_first_joinus);
        relativeLayoutShowDL=(RelativeLayout)findViewById(R.id.tab_DL_details_first_joinus);
        relativeLayoutShowInsurance=(RelativeLayout)findViewById(R.id.tab_insurance_details_first_joinus);
        relativeLayoutShowRest=(RelativeLayout)findViewById(R.id.tab_registration_details_first_joinus);
        relativeLayoutshowCheque=(RelativeLayout)findViewById(R.id.tab_Cheque_details_first_joinus);
        relativeLayoutShowPan=(RelativeLayout)findViewById(R.id.tab_pan_details_first_joinus);
        relativeLayoutIDUPLOAD=(RelativeLayout)findViewById(R.id.tab_add_details_first_joinus);

        linearLayout_upload_id = (LinearLayout) findViewById(R.id.linearLayout_id_upload);
        linearLayoutPadding = (LinearLayout) findViewById(R.id.linearLayoutPaddingForaddtinaol);
        imageViewExpandIdUpload = (ImageView) findViewById(R.id.image_add_details_first_joinus);
        imageViewShowBank = (ImageView) findViewById(R.id.image_bank_details_first_joinus);
        imageViewShowPersonal = (ImageView) findViewById(R.id.image_personal_details_first_joinus);
        imageViewShowvehile = (ImageView) findViewById(R.id.image_cab_details_first_joinus);
        imageViewShowPan=(ImageView)findViewById(R.id.image_pan_details_first_joinus);
        imageViewShowCheque=(ImageView)findViewById(R.id.image_Cheque_details_first_joinus);

        imageViewShowBike=(ImageView)findViewById(R.id.image_bikeimg_details_first_joinus);
        imageViewShowDL=(ImageView)findViewById(R.id.image_DL_details_first_joinus);
        imageViewShowInsurance=(ImageView)findViewById(R.id.image_insurance_details_first_joinus);
        imageViewShowregist=(ImageView)findViewById(R.id.image_registration_details_first_joinus);

        expandableLayoutPan=(net.cachapa.expandablelayout.ExpandableLayout)findViewById(R.id.expanded_menu_pan);
        expandableLayoutCheque=(net.cachapa.expandablelayout.ExpandableLayout)findViewById(R.id.expanded_menu_Cheque);
        expandableLayoutBank = (net.cachapa.expandablelayout.ExpandableLayout) findViewById(R.id.expandable_bank_details_first_joinus);
        expandableLayoutPersonal = (net.cachapa.expandablelayout.ExpandableLayout) findViewById(R.id.expandable_personal_details_first_joinus);
        expandableLayoutVehicle = (net.cachapa.expandablelayout.ExpandableLayout) findViewById(R.id.expandable_vehicle_details_first_joinus);
        expandableLayoutIDupload = (net.cachapa.expandablelayout.ExpandableLayout) findViewById(R.id.expanded_menu_id_upload);

        expandableLayoutBIkeImage=(net.cachapa.expandablelayout.ExpandableLayout)findViewById(R.id.expanded_menu_bikeimg);
        expandableLayoutDL=(net.cachapa.expandablelayout.ExpandableLayout)findViewById(R.id.expanded_menu_DL);
        expandableLayoutInsurance=(net.cachapa.expandablelayout.ExpandableLayout)findViewById(R.id.expanded_menu_insurance);
        expandableLayoutRegistration=(net.cachapa.expandablelayout.ExpandableLayout)findViewById(R.id.expanded_menu_registration);

        editTextBIKENO = (EditText) findViewById(R.id.input_bike_content_personal_details);
        editTextDLNO = (EditText) findViewById(R.id.input_DL_content_personal_details);

        profileImage = (CircleImageView) findViewById(R.id.profile_image_content_personal_details);
        radioGroupGender = (RadioGroup) findViewById(R.id.radiogroup_gender_content_personal_details);
        inputName = (EditText) findViewById(R.id.input_name_content_personal_details);
        inputEmail = (EditText) findViewById(R.id.input_email_content_personal_details);
        inputDOB = (EditText) findViewById(R.id.input_dob_content_personal_details);
        imageViewId = (ImageView) findViewById(R.id.image_idproof_content_personal_details);
        toAddCar = (CheckBox) findViewById(R.id.checkbox_add_car_content_personal_details);
        btnSubmit = (TextView) findViewById(R.id.btn_submit_content_personal_details);
        btnBack = (ImageView) findViewById(R.id.btn_back_content_personal_details);
        radioGroupAddress = (RadioGroup) findViewById(R.id.radio_group_address);


        insuranceCopy = (ImageView) findViewById(R.id.image_insurance_content_car_details);
        rcCopy = (ImageView) findViewById(R.id.image_rc_content_car_details);
        dlCopy = (ImageView) findViewById(R.id.image_dl_content_car_details);
        vehicleCopy = (ImageView) findViewById(R.id.image_vehicle_content_car_details);
        cancelledChequeCopy = (ImageView) findViewById(R.id.image_cancelled_cheque_content_car_details);
        panCopy = (ImageView) findViewById(R.id.image_pan_content_car_details);
        btnSubmit1 = (TextView) findViewById(R.id.btn_submit_content_car_details);
        inputIfsc = (EditText) findViewById(R.id.input_ifsc_content_car_details);
        inputBankName = (EditText) findViewById(R.id.input_bank_name_content_car_details);
        inputBankBranch = (EditText) findViewById(R.id.input_branch_name_content_car_details);
        inputAccountNumber = (EditText) findViewById(R.id.input_account_number_content_car_details);
        inputPanNumber = (EditText) findViewById(R.id.input_pan_number_content_car_details);
        progressBar = (RelativeLayout) findViewById(R.id.progress_bar_content_personal_details);
        progressBar.setVisibility(View.GONE);

        Utils.addSpaceFilter(inputDOB);
        Utils.addSpaceFilter(inputEmail);
        inputName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(25)});
    }

    private void pickImage(int requestCode,String title) {
        if (whichImage == 0){
            whichImage = requestCode;
            if (PermissionUtils.isReadStoragePermissionGranted(this)){
                com.hipool.imagePicker.ImagePicker.pickImage(this,title);
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)){
                    PermissionUtils.askReadExtStoragePermission(this);
                } else {
                    PermissionUtils.askReadExtStoragePermission(this);
                }
            }
        }
       /* ImagePicker imagePicker = ImagePicker.create(this)
                .theme(R.style.ImagePickerTheme)
                .returnAfterFirst(true) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                .folderMode(true) // set folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select"); // image selection title

        imagePicker.single();

        imagePicker.limit(1) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .origin(images) // original selected images, used in multi mode
                .start(requestCode); // start image picker activity with request code*/
       // Intent intent=getPi
        //com.hipool.imagePicker.ImagePicker.pickImage(this,"Image");*/
    }



    private void pickImageId() {
        /*ImagePicker imagePicker = ImagePicker.create(this)
                .theme(R.style.ImagePickerTheme)
                .returnAfterFirst(true) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                .folderMode(false) // set folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select"); // image selection title

        imagePicker.single();

        imagePicker.limit(1) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .origin(imagesId) // original selected images, used in multi mode
                .start(REQUEST_CODE_PICK_IMAGE_ID);*/ // start image picker activity with request code
    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = "tnh_" + Utils.getUnixTime() + ".png";

//        Log.e(TAG, "uris:" + uri + ":" + destinationFileName);

        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop.withAspectRatio(1, 1);
        uCrop.withMaxResultSize(500, 500);

        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.PNG);

        options.setCompressionQuality(100);

        uCrop.start(this);
    }


    private void onDOBClicked() {
        Calendar calendar = Calendar.getInstance();
        if (inputDOB.getText().toString().length() == 10) {
            try {
                calendar.setTime(dateFormat.parse(inputDOB.getText().toString()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        datePickerDialog = new DatePickerDialog(PersonalDetailsActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar date = Calendar.getInstance();
                date.set(i, i1, i2);
                if (CalendarUtils.isPastDate(dateFormat.format(date.getTime()))) {
                    inputDOB.setText(dateFormat.format(date.getTime()));
                } else {
                    Toast.makeText(PersonalDetailsActivity.this, "enter valid birthdate..", Toast.LENGTH_LONG).show();
                }
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case UCrop.REQUEST_CROP:
                if (resultCode == RESULT_OK && data != null) {
                    finalImageUri = UCrop.getOutput(data);
                    try {
                        profilePicBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),finalImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.v(TAG, "imageURI : " + finalImageUri);
                    profileImage.setImageURI(finalImageUri);
                } else if (resultCode == UCrop.RESULT_ERROR) {
                    final Throwable cropError = UCrop.getError(data);
                    Toast.makeText(getApplicationContext(), cropError.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
                break;
            case com.hipool.imagePicker.ImagePicker.PICK_IMAGE_REQUEST_CODE:
                switch (whichImage)
                {
                    case REQUEST_CODE_PICK_IMAGE:
                        profilePicBitmap= ImagePicker.getImageFromResult(this, requestCode, resultCode, data);

                        whichImage = 0;
                        if (profilePicBitmap == null){
                            profileImage.setImageResource(R.drawable.userimage);
                        } else {
                            startCropActivity(getImageUri(this,profilePicBitmap));
                           /* profilePicBitmap = getResizedBitmap( profilePicBitmap,500);
                            imageViewId.setImageBitmap( profilePicBitmap);*/
                        }
                        break;


                    case REQUEST_CODE_PICK_IMAGE_ID:
                        addressProofBitmap= ImagePicker.getImageFromResult(this, requestCode, resultCode, data);

                        whichImage = 0;
                        if (addressProofBitmap == null){
                            imageViewId.setImageResource(R.drawable.insurance);
                        } else {

                            addressProofBitmap = getResizedBitmap( addressProofBitmap,500);
                            imageViewId.setImageBitmap( addressProofBitmap);
                        }
                        break;


                    case REQUEST_CODE_PICK_IMAGE_INSURANCE:
                        insuranceCopyUri= ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
                        whichImage = 0;
                        if (insuranceCopyUri == null){
                            insuranceCopy.setImageResource(R.drawable.insurance);
                        } else {

                            insuranceCopyUri = getResizedBitmap( insuranceCopyUri,500);
                            insuranceCopy.setImageBitmap( insuranceCopyUri);
                        }
                        checkVehicleDetail();
                        break;


                    case REQUEST_CODE_PICK_IMAGE_RC:
                        rcCopyUri= ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
                        whichImage = 0;
                        if (rcCopyUri == null){
                            rcCopy.setImageResource(R.drawable.registration);
                        } else {

                            rcCopyUri = getResizedBitmap( rcCopyUri,500);
                            rcCopy.setImageBitmap( rcCopyUri);
                        }
                        checkVehicleDetail();
                        break;



                    case REQUEST_CODE_PICK_IMAGE_DL:
                        dlCopyUri= ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
                        whichImage = 0;
                        if (dlCopyUri == null){
                            dlCopy.setImageResource(R.drawable.dlimage);
                        } else {

                            dlCopyUri = getResizedBitmap( dlCopyUri,500);
                            dlCopy.setImageBitmap( dlCopyUri);
                        }
                        checkVehicleDetail();
                        break;


                    case REQUEST_CODE_PICK_IMAGE_VEHICLE:
                        vehicleCopyUri= ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
                        whichImage = 0;
                        if (vehicleCopyUri == null){
                            vehicleCopy.setImageResource(R.drawable.carimage);
                        } else {

                            vehicleCopyUri = getResizedBitmap( vehicleCopyUri,500);
                            vehicleCopy.setImageBitmap( vehicleCopyUri);
                        }
                        checkVehicleDetail();
                        break;


                    case REQUEST_CODE_PICK_IMAGE_CHEQUE:
                        cancelledChequeCopyUri= ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
                        whichImage = 0;
                        if (cancelledChequeCopyUri == null){
                            cancelledChequeCopy.setImageResource(R.drawable.cancel);
                        } else {

                            cancelledChequeCopyUri = getResizedBitmap( cancelledChequeCopyUri,500);
                            cancelledChequeCopy.setImageBitmap(cancelledChequeCopyUri);
                        }
                        break;

                    case REQUEST_CODE_PICK_IMAGE_PAN:
                        panCopyUri= ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
                        whichImage = 0;
                        if (panCopyUri == null){
                            panCopy.setImageResource(R.drawable.pancard);
                        } else {

                            panCopyUri = getResizedBitmap( panCopyUri,500);
                            panCopy.setImageBitmap(panCopyUri);
                        }
                        break;

                }
                break;


            default:
                break;
        }


    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void getBankDetails(String ifsc) {

        showProgressDialog();

        StringRequest request = new StringRequest(Request.Method.GET, "https://ifsc.razorpay.com/" + ifsc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("Not Found")) {
                    Toast.makeText(getApplicationContext(), "invalid IFSC code..", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        final BankDetail bankDetail = new Gson().fromJson(response, BankDetail.class);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                inputBankName.setText(bankDetail.getBANK());
                                inputBankBranch.setText(bankDetail.getBRANCH() + " " + bankDetail.getCITY());
                                hideProgressDialog();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });
            }
        });

        request.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(request);
    }

    private void sendDataToServer() {
        showProgressDialog();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, BASE_URL + ADD_PROFILE_URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                if (IS_DEBUG) {
                    Log.v(TAG, "profile:" + resultResponse);
                }

                if (resultResponse.contains("Successfully")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            if (toAddCar.isChecked()) {
                                sendDriverDataToServer();
                            } else {
                                Toast.makeText(getApplicationContext(), "success!, please login..", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            }
                            finish();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            Toast.makeText(getApplicationContext(), "failed!", Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (IS_DEBUG) {
                    Log.e(TAG, "profile:" + error.toString());
                }


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                Map<String, DataPart> params = new HashMap<>();
                    params.put("profile_pic", new DataPart("profile_pic.jpg", ImageUtils.getFileDataFromBitmap(getApplicationContext(), profilePicBitmap), "image/jpg"));
                    params.put("address", new DataPart("address.jpg", ImageUtils.getFileDataFromBitmap(getApplicationContext(),addressProofBitmap ), "image/jpg"));

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_contact_no", Prefs.getMobile(getApplicationContext()));
                params.put("cust_id", Prefs.getUserId(getApplicationContext()));
                params.put("customer_name", inputName.getText().toString());
                params.put("email_id", inputEmail.getText().toString());
                params.put("password", Prefs.getPassword(getApplicationContext()));
                params.put("dob", inputDOB.getText().toString());
                params.put("gender", gender);
                return params;
            }
        };

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                -1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        volleyMultipartRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(volleyMultipartRequest);
    }

    private void sendDriverDataToServer() {
//        showProgressDialog();

        progressBar.setVisibility(View.VISIBLE);

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, BASE_URL + ADD_CAR_DRIVER_DETAIL_URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                if (IS_DEBUG) {
                    Log.v(TAG, "cardetails:" + resultResponse);
                }

                if (resultResponse.toLowerCase().contains("success")){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            hideProgressDialog();
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "success!, please login..", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            finish();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            hideProgressDialog();
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "failed!", Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (IS_DEBUG) {
                    Log.e(TAG, "cardetails:" + error.toString());
                }


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        hideProgressDialog();

                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        }) {
            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                Map<String, DataPart> params = new HashMap<>();

                    params.put("insurance", new DataPart("insurance.jpg", ImageUtils.getFileDataFromBitmap(getApplicationContext(), insuranceCopyUri), "image/jpg"));
                    params.put("registration", new DataPart("registration.jpg", ImageUtils.getFileDataFromBitmap(getApplicationContext(), rcCopyUri), "image/jpg"));
                    params.put("dl_copy", new DataPart("dl_copy.jpg", ImageUtils.getFileDataFromBitmap(getApplicationContext(), dlCopyUri), "image/jpg"));
                    params.put("vehicle_copy", new DataPart("vehicle_copy.jpg", ImageUtils.getFileDataFromBitmap(getApplicationContext(), vehicleCopyUri), "image/jpg"));
                    params.put("pan_card", new DataPart("pan_card.jpg", ImageUtils.getFileDataFromBitmap(getApplicationContext(), panCopyUri), "image/jpg"));
                    params.put("cheque", new DataPart("cheque.jpg", ImageUtils.getFileDataFromBitmap(getApplicationContext(), cancelledChequeCopyUri), "image/jpg"));

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_contact_no", Prefs.getMobile(getApplicationContext()));
                params.put("cust_id", Prefs.getUserId(getApplicationContext()));
                params.put("pan_number", inputPanNumber.getText().toString());
                params.put("bank_name", inputBankName.getText().toString());
                params.put("bank_ifsc_code", inputIfsc.getText().toString());
                params.put("bank_branch", inputBankBranch.getText().toString());
                params.put("account_no", inputAccountNumber.getText().toString());
                return params;
            }
        };

        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                -1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        volleyMultipartRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(volleyMultipartRequest);
    }

}
