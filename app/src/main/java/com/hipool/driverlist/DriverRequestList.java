package com.hipool.driverlist;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.adapters.CustomerRequestsRecyclerAdapter;
import com.hipool.app.MyApp;
import com.hipool.models.CustomerDropLocation;
import com.hipool.models.CustomerRequest;
import com.hipool.models.DriverDetail;
import com.hipool.utils.Constants;
import com.hipool.utils.NetworkUtils;
import com.hipool.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hipool.utils.APIConstants.ADD_RIDE_DATA;
import static com.hipool.utils.APIConstants.BASE_URL;

public class DriverRequestList extends AppCompatActivity {
    private RecyclerView recyclerView;
    private CustomerRequestsRecyclerAdapter adapter;
    List<CustomerDropLocation> customerDropLocations = new ArrayList<>();
    List<DriverDetail> driverDetailsArrayList=new ArrayList<>();
    private com.hipool.models.DriverList driverList;
    String cust_id,current_lat,current_lon,pick_lat,pick_lon,drop_lat,drop_lon,pick_address,drop_address,total_distance,total_fare,total_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider_requests);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_activity_rider_requests);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Requests");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        cust_id=getIntent().getStringExtra("cust_id");
       current_lat =getIntent().getStringExtra("current_lat");
        current_lon=getIntent().getStringExtra("current_lon");
        pick_lat=getIntent().getStringExtra("pickup_lat");
        pick_lon=getIntent().getStringExtra("pickup_lon");
        drop_lat=getIntent().getStringExtra("drop_lat");
        drop_lon=getIntent().getStringExtra("drop_lon");
        pick_address=getIntent().getStringExtra("pick_address");
        drop_address=getIntent().getStringExtra("drop_address");
        total_distance=getIntent().getStringExtra("total_distance");
        total_fare=getIntent().getStringExtra("total_fare");
        total_time=getIntent().getStringExtra("total_time");
        initViews();
    }

    private void initViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_content_rider_requests);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        sendCustLocation();
    }
    private void sendCustLocation() {
        if(!NetworkUtils.isInternetConnected(getApplicationContext()))
            return;
        final MaterialDialog materialDialog = new MaterialDialog.Builder(getApplicationContext())
                .title("Getting Rider List")
                .content("Please wait ..")
                .progress(true, 0)
                .cancelable(false)
                .show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + ADD_RIDE_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("Response",response);
                if (response.contains("Driver found")) {

                    driverList=new Gson().fromJson(response, com.hipool.models.DriverList.class);


                    List<CustomerDropLocation> customerDropLocations=driverList.getData();
                    driverDetailsArrayList.clear();
                    for(int i=0;i<customerDropLocations.size();i++)
                    {
                        driverDetailsArrayList.add(customerDropLocations.get(i).getDriverDetail());

                    }
                    adapter = new CustomerRequestsRecyclerAdapter(driverDetailsArrayList, null, getApplicationContext());
                    recyclerView.setAdapter(adapter);
                    materialDialog.dismiss();

                } else {
                    materialDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Sorry, No Driver on this route. Please try after some time or change your pickup location",Toast.LENGTH_LONG).show();
                    finish();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                materialDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                final Map<String, String> params = new HashMap<>();
                params.put("cust_id", cust_id);
                params.put("current_lat", current_lat);
                params.put("current_lon", current_lon);
                params.put("pickup_lat", pick_lat);
                params.put("pickup_lon", pick_lon);
                params.put("drop_lat", drop_lat);
                params.put("drop_lon", drop_lon);
                params.put("pick_address",pick_address);
                params.put("drop_address",drop_address);
                params.put("total_distance",total_distance);
                params.put("total_fare",total_fare);
                params.put("total_time",total_time);
                Log.e("Param",params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

}
