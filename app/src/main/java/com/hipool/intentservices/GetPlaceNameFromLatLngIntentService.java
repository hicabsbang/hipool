package com.hipool.intentservices;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.os.ResultReceiver;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.model.LatLng;
import com.hipool.R;
import com.hipool.app.MyApp;
import com.hipool.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ashutosh on 08-06-2017.
 */

public class GetPlaceNameFromLatLngIntentService extends IntentService {

    private static final String TAG = GetPlaceNameFromLatLngIntentService.class.getSimpleName();

    /**
     * The receiver where results are forwarded from this service.
     */
    protected ResultReceiver mReceiver;

    public GetPlaceNameFromLatLngIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        String errorMessage = "";

        mReceiver = intent.getParcelableExtra(Constants.RECEIVER);

        // Check if receiver was properly registered.
        if (mReceiver == null) {
            Log.wtf(TAG, "No receiver received. There is nowhere to send the results.");
            return;
        }

        // Get the location passed to this service through an extra.
       // LatLng location = intent.getParcelableExtra(Constants.LOCATION_DATA_EXTRA);
        String lat=intent.getStringExtra("Lat");
        String lon=intent.getStringExtra("Lon");

        // Make sure that the location data was really sent over through an extra. If it wasn't,
        // send an error error message and return.
        if (lat == null || lon==null) {
            errorMessage = getString(R.string.no_location_data_provided);
            Log.wtf(TAG, errorMessage);
            deliverResultToReceiver(Constants.FAILURE_RESULT, errorMessage, intent.getStringExtra(Constants.LOCATION_DATA_WHICH_EXTRA));
            return;
        }

        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&key=" + getString(R.string.google_maps_key);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v(TAG, "geocoderes : " + response);

                try {
                    String address = new JSONObject(response).getJSONArray("results").getJSONObject(0).getString("formatted_address");

                    deliverResultToReceiver(Constants.SUCCESS_RESULT, address, intent.getStringExtra(Constants.LOCATION_DATA_WHICH_EXTRA));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
            }
        });

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void deliverResultToReceiver(int resultCode, String message, String which) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.RESULT_DATA_KEY, message);
        bundle.putString(Constants.RESULT_WHICH_KEY, which);
        mReceiver.send(resultCode, bundle);
    }
}
