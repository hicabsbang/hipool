package com.hipool.support;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hipool.R;
import com.hipool.adapters.SupportRecyclerAdapter;
import com.hipool.app.BaseActivity;
import com.hipool.models.SupportItem;

import java.util.ArrayList;
import java.util.List;

public class SupportActivity extends BaseActivity implements SupportRecyclerAdapter.SupportRecyclerAdapterCommunicator {

    private RelativeLayout tabAppRelated, tabPaymentRelated, tabCustomerRelated, tabOtherIssues;
    private ImageView imageTabAppRelated, imageTabPaymentRelated, imageTabCustomerRelated, imageTabOtherIssues;
    private RecyclerView recyclerAppRelated, recyclerPaymentRelated, recyclerCustomerRelated;
    private EditText inputOtherIssued;
    private TextView btnSubmit;
    private List<SupportItem> supportItems = new ArrayList<>();
    private SupportRecyclerAdapter adapter;
//    private int selected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Support");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        tabAppRelated = (RelativeLayout) findViewById(R.id.tab_app_related_support);
        tabPaymentRelated = (RelativeLayout) findViewById(R.id.tab_payment_related_support);
        tabCustomerRelated = (RelativeLayout) findViewById(R.id.tab_customer_related_support);
        tabOtherIssues = (RelativeLayout) findViewById(R.id.tab_other_issues_support);
        imageTabAppRelated = (ImageView) findViewById(R.id.image_tab_app_related_support);
        imageTabPaymentRelated = (ImageView) findViewById(R.id.image_tab_payment_related_support);
        imageTabCustomerRelated = (ImageView) findViewById(R.id.image_tab_customer_related_support);
        imageTabOtherIssues = (ImageView) findViewById(R.id.image_tab_other_issues_support);

        recyclerAppRelated = (RecyclerView) findViewById(R.id.recycler_app_related_support);
        supportItems.clear();
        for (int i = 0; i < 5 ; i++) {
            SupportItem supportItem = new SupportItem();
            supportItem.setItem("App Related " + i);
            supportItems.add(supportItem);
        }
        adapter = new SupportRecyclerAdapter(supportItems, this);
        recyclerAppRelated.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerAppRelated.setItemAnimator(new DefaultItemAnimator());
        recyclerAppRelated.setAdapter(adapter);

        recyclerPaymentRelated = (RecyclerView) findViewById(R.id.recycler_payment_related_support);
        recyclerPaymentRelated.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerPaymentRelated.setItemAnimator(new DefaultItemAnimator());
        recyclerPaymentRelated.setAdapter(adapter);
        recyclerPaymentRelated.setVisibility(View.GONE);

        recyclerCustomerRelated = (RecyclerView) findViewById(R.id.recycler_customer_related_support);
        recyclerCustomerRelated.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerCustomerRelated.setItemAnimator(new DefaultItemAnimator());
        recyclerCustomerRelated.setAdapter(adapter);
        recyclerCustomerRelated.setVisibility(View.GONE);

        inputOtherIssued = (EditText) findViewById(R.id.editText_comments_other_issues_support);
        inputOtherIssued.setVisibility(View.GONE);
        btnSubmit = (TextView) findViewById(R.id.btn_submit_support);


        tabAppRelated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerPaymentRelated.setVisibility(View.GONE);
                recyclerCustomerRelated.setVisibility(View.GONE);
                inputOtherIssued.setVisibility(View.GONE);
                recyclerAppRelated.setVisibility(View.VISIBLE);
                imageTabAppRelated.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                imageTabCustomerRelated.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                imageTabPaymentRelated.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                imageTabOtherIssues.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);



                supportItems.clear();
                for (int i = 0; i < 5 ; i++) {
                    SupportItem supportItem = new SupportItem();
                    supportItem.setItem("App Related " + i);
                    supportItems.add(supportItem);
                }
                adapter.notifyDataSetChanged();
            }
        });

        tabPaymentRelated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerPaymentRelated.setVisibility(View.VISIBLE);
                recyclerCustomerRelated.setVisibility(View.GONE);
                inputOtherIssued.setVisibility(View.GONE);
                recyclerAppRelated.setVisibility(View.GONE);
                imageTabAppRelated.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                imageTabCustomerRelated.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                imageTabPaymentRelated.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                imageTabOtherIssues.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);


                supportItems.clear();
                for (int i = 0; i < 5 ; i++) {
                    SupportItem supportItem = new SupportItem();
                    supportItem.setItem("Payment Related " + i);
                    supportItems.add(supportItem);
                }
                adapter.notifyDataSetChanged();
            }
        });

        tabCustomerRelated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerPaymentRelated.setVisibility(View.GONE);
                recyclerCustomerRelated.setVisibility(View.VISIBLE);
                inputOtherIssued.setVisibility(View.GONE);
                recyclerAppRelated.setVisibility(View.GONE);
                imageTabAppRelated.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                imageTabCustomerRelated.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
                imageTabPaymentRelated.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                imageTabOtherIssues.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);


                supportItems.clear();
                for (int i = 0; i < 5 ; i++) {
                    SupportItem supportItem = new SupportItem();
                    supportItem.setItem("Customer Related " + i);
                    supportItems.add(supportItem);
                }
                adapter.notifyDataSetChanged();
            }
        });

        tabOtherIssues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerPaymentRelated.setVisibility(View.GONE);
                recyclerCustomerRelated.setVisibility(View.GONE);
                inputOtherIssued.setVisibility(View.VISIBLE);
                recyclerAppRelated.setVisibility(View.GONE);
                imageTabAppRelated.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                imageTabCustomerRelated.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                imageTabPaymentRelated.setImageResource(R.drawable.ic_add_circle_outline_white_24dp);
                imageTabOtherIssues.setImageResource(R.drawable.ic_remove_circle_outline_white_24dp);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public void onItemSelected(int position) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }
}
