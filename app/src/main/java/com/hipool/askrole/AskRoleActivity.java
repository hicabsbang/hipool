package com.hipool.askrole;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.customerhome.HomeActivity;
import com.hipool.driverhome.DriverHomeActivity;
import com.hipool.models.CustomerDetails;
import com.hipool.service.BookingService;
import com.hipool.utils.Prefs;

import org.json.JSONObject;

import static com.hipool.utils.Constants.CUST_DETAILS;

public class AskRoleActivity extends AppCompatActivity {

    private RelativeLayout btnRide, btnDrive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_role);

        CustomerDetails customerDetails = new Gson().fromJson(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(CUST_DETAILS, ""), CustomerDetails.class);

        btnRide = (RelativeLayout) findViewById(R.id.btn_ride_content_ask_role);
        btnDrive = (RelativeLayout) findViewById(R.id.btn_drive_content_ask_role);

        btnRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                finish();
            }
        });


        btnDrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), DriverHomeActivity.class));
                finish();
            }
        });


    }

    @Override
    protected void onStart() {
        startService(new Intent(AskRoleActivity.this, BookingService.class));
        super.onStart();
    }
}
