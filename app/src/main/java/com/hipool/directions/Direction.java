package com.hipool.directions;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.app.BaseFragment;
import com.hipool.fragment.DropLocationFragment;
import com.hipool.fragment.HomeFragment;
import com.hipool.models.LegsItem;
import com.hipool.models.RoutesItem;
import com.hipool.models.StepsItem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ashut on 19-09-2017.
 */

public class Direction extends AsyncTask<Void, Void, String> {

    private static final String TAG = Direction.class.getSimpleName();
    private String directionUrl = "https://maps.googleapis.com/maps/api/directions/json?";


    private DirectionsRouteCommunicator routeCommunicator;
    private Context mContext;

    public interface DirectionsRouteCommunicator {
        void onRoutesReady(List<Route> routeList);
    }

    public Direction(BaseFragment fragment, LatLng origin, LatLng dest,String From) {

        this.directionUrl += "origin=" + origin.latitude + "," + origin.longitude + "&destination=" + dest.latitude + "," + dest.longitude;
        if(From.equals("Driver"))
        {
            this.directionUrl+="&alternatives=true&key=" + fragment.getString(R.string.google_maps_key);
            this.routeCommunicator = (HomeFragment) fragment;
        }
        else
        {

            this.directionUrl+="&key=" + fragment.getString(R.string.google_maps_key);
            this.routeCommunicator=(DropLocationFragment)fragment;
        }


        this.mContext = fragment.getContext();
//        Log.v(TAG, "url:" + directionUrl);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... voids) {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(directionUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
//            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            try {
                iStream.close();
                urlConnection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(mContext, "no routes!", Toast.LENGTH_LONG).show();
            }
        }
        return data;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);


        List<Route> routeList = new ArrayList<>();

        com.hipool.models.Direction direction = new Gson().fromJson(s, com.hipool.models.Direction.class);

        List<RoutesItem> routesItemList = direction.getRoutes();

        for (int i = 0 ; i < routesItemList.size() ; i++) {
            String via = routesItemList.get(i).getSummary();
            List<LegsItem> legsItemList = routesItemList.get(i).getLegs();
            String distanceText = legsItemList.get(0).getDistance().getText();
            int distance = legsItemList.get(0).getDistance().getValue();
            String timeText = legsItemList.get(0).getDuration().getText();
            int time = legsItemList.get(0).getDuration().getValue();
            PolylineOptions polylineOptions = new PolylineOptions();
            List<LatLng> latLngList = new ArrayList<>();
            for (int j = 0; j < legsItemList.size(); j++) {
                List<StepsItem> stepsItemList = legsItemList.get(j).getSteps();
                for (int k = 0 ; k < stepsItemList.size(); k++) {
                    String polyline = stepsItemList.get(k).getPolyline().getPoints();
                    List<LatLng> list = decodePoly(polyline);
                    /** Traversing all points */
                    for(int l=0;l<list.size();l++){
                        HashMap<String, String> hm = new HashMap<>();
                        LatLng latLng = new LatLng(list.get(l).latitude, list.get(l).longitude);
                        latLngList.add(latLng);
                    }
                }
            }
            // Adding all the points in the route to LineOptions
            polylineOptions.addAll(latLngList);
            polylineOptions.width(5);
            if (i == 0){
                polylineOptions.color(Color.BLUE);
            } else if (i == 1) {
                polylineOptions.color(Color.GREEN);
            } else if (i == 2) {
                polylineOptions.color(Color.RED);
            }
            Route route = new Route();
            route.setPolylineOptions(polylineOptions);
            route.setVia(via);
            route.setDistanceText(distanceText);
            route.setDistance(distance);
            route.setTimeText(timeText);
            route.setTime(time);
            routeList.add(route);
        }

        // Drawing polyline in the Google Map for the i-th route
        if(routeList.size() > 0) {
            routeCommunicator.onRoutesReady(routeList);
        }
        else {
            Log.d("onPostExecute","without Polylines drawn");
        }


    }

    /**
     * Method to decode polyline points
     * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     * */
    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    public class Route {
        PolylineOptions polylineOptions;
        String via, distanceText, timeText;
        int distance, time;

        public PolylineOptions getPolylineOptions() {
            return polylineOptions;
        }

        public void setPolylineOptions(PolylineOptions polylineOptions) {
            this.polylineOptions = polylineOptions;
        }

        public String getVia() {
            return via;
        }

        public void setVia(String via) {
            this.via = via;
        }

        public String getDistanceText() {
            return distanceText;
        }

        public void setDistanceText(String distanceText) {
            this.distanceText = distanceText;
        }

        public String getTimeText() {
            return timeText;
        }

        public void setTimeText(String timeText) {
            this.timeText = timeText;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        public int getTime() {
            return time;
        }

        public void setTime(int time) {
            this.time = time;
        }
    }

}
