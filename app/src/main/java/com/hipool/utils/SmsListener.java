package com.hipool.utils;

/**
 * Created by ashut on 11-10-2017.
 */

public interface SmsListener {
    public void messageReceived(String messageText);
}
