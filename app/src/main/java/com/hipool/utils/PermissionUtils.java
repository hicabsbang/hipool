package com.hipool.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by Ashutosh on 23-03-2017.
 */

public final class PermissionUtils {

    public static final int REQUEST_CODE_FINE_LOCATION = 111;
    public static final int REQUEST_CODE_READ_STORAGE = 112;
    public static final int REQUEST_CODE_WRITE_STORAGE = 113;
    public static final int REQUEST_CODE_CAMERA = 114;
    public static final int REQUEST_CODE_CALL= 115;
    public static final String[] PERMISSIONS_LIST = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.CALL_PHONE
    };
    public static final int REQUEST_CODE_ALL= 116;

    private PermissionUtils() {
    }

    public static boolean isLocationPermissionGranted(Context context){
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isReadStoragePermissionGranted(Context context){
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isWriteStoragePermissionGranted(Context context){
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isCameraPermissionGranted(Context context){
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isCallPermissionGranted(Context context){
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.CALL_PHONE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isAllNeededPermissionGranted(Context context){
        return isLocationPermissionGranted(context)
                && isReadStoragePermissionGranted(context)
                && isWriteStoragePermissionGranted(context)
                && isCameraPermissionGranted(context)
                && isCameraPermissionGranted(context)
                && isCallPermissionGranted(context);
    }

    public static void askReadExtStoragePermission(Activity activity){
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_READ_STORAGE);
    }

    public static void askCallPermission(Activity activity){
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CODE_CALL);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
