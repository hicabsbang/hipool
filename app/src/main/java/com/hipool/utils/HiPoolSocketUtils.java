package com.hipool.utils;

/**
 * Created by Abhinav on 16/09/2017.
 */

public class HiPoolSocketUtils {
    public static final String EVENT_CONNECT = "connect";
    public static final String EVENT_CONNECTED = "connected";
    public static final String EVENT_DRIVER_NOTIFICATION = "notifiedDriver";
    public static final String EVENT_CHANGE_RIDE_STATUS = "changeRideStatus";
    public static final String EVENT_CONFIRM_CUSTOMER_BOOKING = "confirmBooking";
    public static final String EVENT_UPDATE_LOCATION = "onLocationupdate";
    public static final String EVENT_DRIVER_RATING = "driverRating";
    public static final String EVENT_CUSTOMER_RATING = "customerRating";
    public static final String EVENT_DISCONNECT = "disconnect";

}