package com.hipool.utils;

import android.content.Context;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.hipool.models.DriverDetail;

import org.json.JSONArray;

/**
 * Created by ashut on 15-09-2017.
 */

public final class Prefs {
    private Prefs() {
    }

    public static void setUserId(Context context, String userId) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("user_id", userId).apply();
    }

    public static String getUserId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("user_id", "");
    }

    public static void setName(Context context, String name) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("name", name).apply();
    }

    public static String getName(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("name", "");
    }

    public static void setMobile(Context context, String mobile) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("mobile", mobile).apply();
    }

    public static String getMobile(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("mobile", "");
    }

    public static void setPassword(Context context, String password) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("password", password).apply();
    }

    public static String getPassword(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("password", "");
    }

    public static void setMyLat(Context context, double lat){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("lat", String.valueOf(lat)).apply();
    }

    public static double getMyLat(Context context){
        return Double.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("lat", ""));
    }

    public static void setMyLon(Context context, double lon){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("lon", String.valueOf(lon)).apply();
    }

    public static double getMyLon(Context context){
        return Double.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("lon", ""));
    }

    public static void setDeviceToken(Context context, String token) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("device_token", token).apply();
    }

    public static String getDeviceToken(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("device_token", "");
    }

    public static void setCustomerRequests(Context context, JSONArray jsonArray) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("customer_requests", jsonArray.toString()).apply();
    }

    public static String getCustomerRequests(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("customer_requests", "");
    }

    public static void setDriverRequests(Context context, DriverDetail driverDetail) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("driver_requests",new Gson().toJson(driverDetail)).apply();
    }

    public static String getDriverRequests(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("driver_requests", "");
    }

    public static void setDriverRequestId(Context context, String requestId) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("driver_request_id", requestId).apply();
    }

    public static String getDriverRequestId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("driver_request_id", "");
    }

    public static void setLiveBookingId(Context context, String bookingId) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("live_booking_id", bookingId).apply();
    }

    public static String getLiveBookingId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("live_booking_id", "");
    }

//    public static void setConfirmedCustomerData(Context context, String customer) {
//        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("confirmed_customer_data", customer).apply();
//    }
//
//    public static String getConfirmedCustomerData(Context context) {
//        return PreferenceManager.getDefaultSharedPreferences(context).getString("confirmed_customer_data", "");
//    }

    public static void setDriverReachedAtPickLocationTime(Context context, String status) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("driver_reached_time", status).apply();
    }

    public static String getDriverReachedAtPickLocationTime(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("driver_reached_time", "");
    }

    public static void setCustomerPickLat(Context context, String lat) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("customer_pick_lat", lat).apply();
    }

    public static String getCustomerPickLat(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("customer_pick_lat", "");
    }

    public static void setCustomerPickLon(Context context, String lon) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("customer_pick_lon", lon).apply();
    }

    public static String getCustomerPickLon(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("customer_pick_lon", "");
    }

    public static void setCustRole(Context context, String role) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("customer_role", role).apply();
    }

    public static String getCustRole(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("customer_role", "");
    }
}
