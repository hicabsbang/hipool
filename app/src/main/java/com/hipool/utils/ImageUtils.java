package com.hipool.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by Ashutosh on 06-03-2017.
 */

public final class ImageUtils {

    private ImageUtils(){}

    public static byte[] getFileDataFromBitmap(Context context, Bitmap bm) {
        Bitmap bitmap = bm;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static File getFilefromBitmap(Bitmap bitmap) {
        File extStorageDirectory = new File(Environment.getExternalStorageDirectory().toString() + "/hicabs/");
        OutputStream outStream = null;
        String filename = (System.currentTimeMillis() / 1000L) + Utils.randInt(1111, 9999) + "";
        File file = null;
        try {
            if (extStorageDirectory.mkdirs()){
                Log.e("file exist", "true");
            } else {
                Log.e("file exist", "false");
            }
            file = new File(extStorageDirectory, filename + ".png");
            Log.e("file exist", "" + file + ",Bitmap= " + filename);
            // make a new bitmap from your file
//            Bitmap bitmap = BitmapFactory.decodeFile(file.getName());
            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("file", "" + file);
        return file;

    }
}
