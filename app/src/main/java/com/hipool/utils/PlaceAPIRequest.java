package com.hipool.utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ashut on 29-08-2016.
 */
public class PlaceAPIRequest {

    private static final String TAG = PlaceAPIRequest.class.getSimpleName();
    private static final String API_KEY = "AIzaSyADBDfJGV0ynOh92YaopuPLIAZnsKV_H2o";

    public ArrayList<String> autoComplete(String string){
        ArrayList<String> resultList = null;

        String finalUrl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + string + "&location=12.983846,77.587419&radius=100000&strictbounds&key=" + API_KEY;
//        Log.v("urlsssss", finalUrl);

        UrlDataFetcher urlDataFetcher = new UrlDataFetcher();

        String response = urlDataFetcher.getUrlData(finalUrl.replace(" ", "%20"));
        //Log.v("Response", response.toString());

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("predictions");

            resultList = new ArrayList<>(jsonArray.length());
            for (int i = 0 ; i<jsonArray.length() ; i++)
                resultList.add(jsonArray.getJSONObject(i).getString("description"));
        } catch (JSONException e) {
            e.printStackTrace();
            Log.v(TAG, "Error...");
        }
        return resultList;
    }
}
