package com.hipool.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Ashutosh on 07-03-2017.
 */

public class CalendarUtils {

    private CalendarUtils(){}

    public static int getMaxDate(int day, int month, int year){
        return new GregorianCalendar(year, month, day).getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static boolean isPastDate(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        Date dateParam = null;
        try {
            dateParam = sdf.parse(date);
            return new Date().after(dateParam);
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isFutureDate(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        Date dateParam = null;
        try {
            dateParam = sdf.parse(date);
            return new Date().before(dateParam);
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static boolean getAgeValidation(int year, int month, int day) {
        Calendar userAge = new GregorianCalendar(year, month, day);
        Calendar minAdultAge = new GregorianCalendar();
        minAdultAge.add(Calendar.YEAR, -18);
        return !minAdultAge.before(userAge);
    }

    public static Date addDaysToDate(Date date, int numberOfDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, numberOfDays);
        return calendar.getTime();
    }

    public static int getAge(int DOByear, int DOBmonth, int DOBday) {

        int age;

        final Calendar calenderToday = Calendar.getInstance();
        int currentYear = calenderToday.get(Calendar.YEAR);
        int currentMonth = 1 + calenderToday.get(Calendar.MONTH);
        int todayDay = calenderToday.get(Calendar.DAY_OF_MONTH);

        age = currentYear - DOByear;

        if(DOBmonth > currentMonth){
            --age;
        }
        else if(DOBmonth == currentMonth){
            if(DOBday > todayDay){
                --age;
            }
        }
        return age;
    }
}
