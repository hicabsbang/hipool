package com.hipool.utils;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ashut on 29-08-2016.
 */
public class UrlDataFetcher {

    static String string = null;

    public UrlDataFetcher(){

    }

    public String getUrlData(String s){
        try {
            URL url = new URL(s);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            if(httpURLConnection.getResponseCode() == 200){
                InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while((line = bufferedReader.readLine()) != null){
                    stringBuilder.append(line);
                }
                string = stringBuilder.toString();

                httpURLConnection.disconnect();
            } else {
                Log.e("UrlDataFetcher", "Error fetching Url DriverList, Code : " + httpURLConnection.getResponseCode());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return string;
    }
}
