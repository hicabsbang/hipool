package com.hipool.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by ashut on 15-09-2017.
 */

public final class Constants {
    private Constants() {
    }

    public static final String PACKAGE_NAME =
            "com.hipool";
    public static final Boolean IS_DEBUG = true;
    public static final int FAILURE_RESULT = 1;
    public static final int SUCCESS_RESULT = 0;
    public static final String APP_PREFS =
            "my_prefs";
    public static final String CUST_ID =
            "cust_id";
    public static final String CUST_DETAILS =
            "cust_details";
    public static final String DRIVER_DETAILS =
            "driver_details";
    public static final String KEY_IS_LOGGED_IN = "is_logged_in";
    public static final String INPUT_METHOD_SERVICE = "input_method";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";
    public static final String LOCATION_DATA_WHICH_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_WHICH_EXTRA";
    public static final String LOCATION_DATA_EXTRA_DEST = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA_DEST";
    public static final String RESULT_DIST_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY_DIST";
    public static final String RESULT_TIME_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY_TIME";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";
    public static final String RESULT_DATA_KEY_LAT = PACKAGE_NAME +
            ".RESULT_DATA_KEY_LAT";
    public static final String RESULT_DATA_KEY_LON = PACKAGE_NAME +
            ".RESULT_DATA_KEY_LON";
    public static final String RESULT_WHICH_KEY = PACKAGE_NAME +
            ".RESULT_WHICH_KEY";

    public static final int STATUS_IDLE = 0;
    public static final int STATUS_DROP_LOCATION_ENTERED = 1;
    public static final int STATUS_COUPON_SELECTION = 2;
    public static final int STATUS_CAB_SEARCH = 3;
    public static final int STATUS_CAB_CONFIRMED = 4;
    public static final int STATUS_RIDE_STARTED = 5;
    public static final int STATUS_RIDE_STOPPED = 6;
    public static final int STATUS_PAYMENT_DONE = 7;
    public static final SimpleDateFormat DATE_TIME_DEFAULT_FORMAT = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss", Locale.ENGLISH);
    public static final int STATUS_DEFAULT = 0;
    public static final int STATUS_DRIVER_ROUTE_CONFIRMED = 1;
    public static final int STATUS_DRIVER_RIDE_STARTED = 2;
    public static final int STATUS_DRIVER_RIDE_STOPPED = 3;
    public static final int STATUS_CUSTOMER_CONFIRMED_RIDE_NOT_STARTED = 4;
    public static final int STATUS_CUSTOMER_CONFIRMED_RIDE_STARTED = 5;
    public static final int STATUS_CUSTOMER_PICKED = 6;
    public static final int STATUS_CUSTOMER_DROPPED = 7;
    public static final int STATUS_DRIVER_FEEDBACK_PENDING = 8;

    public static final String MID_PAYTM = "hicabs19871107698288";
    public static final String CLIENT_ID_PAYTM = "merchant-hicabs";
    public static final String CLIENT_SECRET_PAYTM = "c12f9ca0-d939-46b9-b2f7-605f9df56ed8";


    public static final int TYPE_IMAGE = 1;
    public static final int TYPE_VIDEO = 2;
}
