package com.hipool.utils;

/**
 * Created by ashut on 15-09-2017.
 */

public final class APIConstants {

    private APIConstants() {
    }

    public static final String BASE_URL = "http://hipool.hicabsdashboard.in/";
    public static final String CHECK_MOBILE_URL = "check-customer-status";
    public static final String ADD_PROFILE_URL = "update-customer-detail";
    public static final String SIGNIN_URL = "customer-login";
    public static final String CHECK_MOBILE_FORGOT_PASSWORD_URL = "customer-forgot-password";
    public static final String SET_PASSWORD_FORGOT_PASSWORD_URL = "update-customer-forgot-password";
    public static final String ADD_CAR_DRIVER_DETAIL_URL = "add-driver-detail";
    public static final String ADD_TRIP_DATA = "add-trip-data";
    public static final String ADD_RIDE_DATA = "add-ride-data";
    public static final String UPDATE_LOCATION = "update-current-location";
    public static final String CHANGE_RIDE_STATUS = "change-ride-status";
    public static final String DRIVER_GURRENT_STATUS = "driver-current-details";
    public static final String CUSTOMER_GURRENT_STATUS = "customer-current-details";
    public static final String GET_PAYMENT_METHOD = "get-customer-payment-methods";
    public static final String UPDATE_PAYMENT_METHOD = "update-customer-payment-methods";
    public static final String UPDATE_WALLET_AMOUNT = "update-customer-wallet-amount";
    public static final String GET_WALLET_AMOUNT = "get-customer-wallet-amount";
    public static final String GET_WALLET_HISTORY = "get-customer-wallet-history";
    public static final String GET_RIDE_TAKEN_HISTORY = "customer-ride-history";
    public static final String GET_RIDE_GIVEN_HISTORY = "driver-ride-history";
    public static final String GET_FEEDBACKS = "get-driver-feedback-options";
    public static final String GET_CUSTOMER_FEEDBACKS = "get-customer-feedback-options";
    public static final String GET_RIDE_FARE="get-fare-ride";
    public static final String SOS="customer-sos";


    public static final String BASE_URL_PAYTM = "https://accounts.paytm.com/";
    public static final String LOGIN_URL_PAYTM = "signin/otp";
    public static final String GET_TOKEN_URL_PAYTM = "signin/validate/otp";
    public static final String VALIDATE_TOKEN_URL_PAYTM = "/user/details";
    public static final String BASE_URL_PAYTM_WALLET = "https://trust.paytm.in/";
    public static final String CHECK_BAL_PAYTM_WALLET = "wallet-web/checkBalance";
    public static final String WITHDRAW_PAYTM_WALLET = "pay-tm";
    public static final String CHECK_TRX_STATUS = "get-transaction-status";
    public static final String VALIDATE_APP_VERSION = "get-customer-app-version";
    public static final String BLOCK_AMOUNT="block-amount";
    public static final String RELEASE_AMOUNT="realse-amount-customer";
}
