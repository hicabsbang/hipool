package com.hipool.collectpayment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.app.MyApp;
import com.hipool.feedback.FeedbackByDriverActivity;
import com.hipool.fragment.HomeFragment;
import com.hipool.models.CurrentDetail;
import com.hipool.models.CustomerDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.hipool.utils.APIConstants.BASE_URL;
import static com.hipool.utils.APIConstants.DRIVER_GURRENT_STATUS;
import static com.hipool.utils.Constants.APP_PREFS;
import static com.hipool.utils.Constants.CUST_DETAILS;

public class CollectPaymentActivity extends AppCompatActivity {

    String booking_id="";
    private static final String TAG = CollectPaymentActivity.class.getSimpleName();
    CustomerDetails customerDetails;
    public static boolean isActivityRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_payment);
        booking_id= getIntent().getStringExtra("booking_id");
        customerDetails = new Gson().fromJson(getApplicationContext().getSharedPreferences(APP_PREFS, MODE_PRIVATE).getString(CUST_DETAILS, ""), CustomerDetails.class);
        getStatus();

    }

    private void getStatus() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + DRIVER_GURRENT_STATUS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.contains("Success")){
                    try {
                        CurrentDetail currentDetail = new Gson().fromJson(new JSONObject(response).getString("data"), CurrentDetail.class);
                        Intent intent=new Intent(getApplicationContext(), FeedbackByDriverActivity.class);
                        intent.putExtra("currentDetail",currentDetail);
                        startActivity(intent);
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.contains("Customer Current Details Not Coming")) {
                   finish();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "getStatus" + error.toString());
                finish();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id", String.valueOf(customerDetails.getCustId()));
                params.put("customer_access_token", customerDetails.getCustomerAccessToken());
                Log.v(TAG, "params" + params.toString());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    protected void onDestroy() {
        isActivityRunning = false;
        super.onDestroy();
    }


}
