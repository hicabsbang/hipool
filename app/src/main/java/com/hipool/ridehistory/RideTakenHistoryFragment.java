package com.hipool.ridehistory;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.adapters.RideHistoryRecyclerAdapter;
import com.hipool.app.BaseFragment;
import com.hipool.app.MyApp;
import com.hipool.models.CustomerDetails;
import com.hipool.models.RideTaken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.hipool.utils.APIConstants.BASE_URL;
import static com.hipool.utils.APIConstants.GET_RIDE_TAKEN_HISTORY;
import static com.hipool.utils.Constants.APP_PREFS;
import static com.hipool.utils.Constants.CUST_DETAILS;
import static com.hipool.utils.Constants.IS_DEBUG;

/**
 * A simple {@link Fragment} subclass.
 */
public class RideTakenHistoryFragment extends BaseFragment implements RideHistoryRecyclerAdapter.RideHistoryRecyclerAdapterCommunicator {

    private final String TAG = RideTakenHistoryFragment.class.getSimpleName();
    private CustomerDetails customerDetails;
    private RecyclerView recyclerView;
    private RideHistoryRecyclerAdapter adapter;
    private List<RideTaken> rideTakenList = new ArrayList<>();


    public RideTakenHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ride_taken_history, container, false);

        customerDetails = new Gson().fromJson(getActivity().getSharedPreferences(APP_PREFS, MODE_PRIVATE).getString(CUST_DETAILS, ""), CustomerDetails.class);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_fragment_ride_taken_history);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new RideHistoryRecyclerAdapter(rideTakenList, null, this);
        recyclerView.setAdapter(adapter);

        getHistory(0, 10);

        return view;
    }


    private void getHistory(final int skipCount, final int takeCount) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + GET_RIDE_TAKEN_HISTORY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (IS_DEBUG){
                    Log.v(TAG, "res: " + response);
                }

                try {
                    JSONObject resObject = new JSONObject(response);
                    JSONArray historyArray = resObject.getJSONArray("data");
                    for (int i = 0; i < historyArray.length(); i++) {
                        RideTaken rideTaken = new Gson().fromJson(historyArray.getString(i), RideTaken.class);
                        rideTakenList.add(rideTaken);
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (IS_DEBUG){
                    Log.e(TAG, "err: " + error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params= new HashMap<>();
//                params.put("cust_id", String.valueOf(customerDetails.getCustId()));
                params.put("cust_id", String.valueOf(customerDetails.getCustId()));
                params.put("skip", String.valueOf(skipCount));
                params.put("take", String.valueOf(takeCount));
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onRideClicked(String rideData, String rideType) {
        Bundle bundle = new Bundle();
        bundle.putString("ridedata", rideData);
        bundle.putString("ridetype", rideType);
        startActivity(new Intent(getContext(), RideHistoryDetailActivity.class).putExtras(bundle));
    }
}
