package com.hipool.ridehistory;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.models.RideGiven;
import com.hipool.models.RideTaken;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class RideHistoryDetailActivity extends AppCompatActivity {

    private CircleImageView profilePic;
    private TextView name, rating, pickLocation, dropLocation, tripBillDriver, platformChargeDriver, earningDriver, tripFareCustomer, taxAppliedCustomer, discountCustomer, btnSupportDriver, totalBillCustomer, paymentAmountCustomer,roundingOff;
    private LinearLayout rootCustomerFareDetail, rootDriverFareDetail, rootCustomerSupport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_history_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_activity_ride_history_detail);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Ride Detail");

        initViews();


        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString("ridetype", "").equals("ridetaken")){
                RideTaken rideTaken = new Gson().fromJson(getIntent().getExtras().getString("ridedata", ""), RideTaken.class);
                Picasso.with(getApplicationContext()).load(rideTaken.getCustProfilePic()).placeholder(R.drawable.userimage).error(R.drawable.userimage).memoryPolicy(MemoryPolicy.NO_CACHE).into(profilePic);
                name.setText(rideTaken.getCustFirstName());
                double estimatedFare=Double.valueOf(String.format("%.2f",rideTaken.getEstimatedFare()));
                double estimatedTax=Double.valueOf(String.format("%.2f",rideTaken.getEstimatedFareTax()));
                double coupanAmount=Double.valueOf(String.format("%.2f",rideTaken.getCouponAmount()));
                pickLocation.setText(rideTaken.getCustPickAddress());
                dropLocation.setText(rideTaken.getCustDropAddress());
                rootDriverFareDetail.setVisibility(View.GONE);
                rootCustomerFareDetail.setVisibility(View.VISIBLE);
                tripFareCustomer.setText("₹"+estimatedFare);
                taxAppliedCustomer.setText("₹"+estimatedTax);
                discountCustomer.setText("₹"+coupanAmount);
                if(Double.valueOf(String.format("%.2f",(Math.round(rideTaken.getEstimatedFare() + rideTaken.getEstimatedFareTax() - rideTaken.getCouponAmount())-(estimatedFare+estimatedTax-coupanAmount))))>=0)
                    roundingOff.setText("+₹"+String.format("%.2f",(Math.round(rideTaken.getEstimatedFare() + rideTaken.getEstimatedFareTax() - rideTaken.getCouponAmount())-(estimatedFare+estimatedTax-coupanAmount))));
                else
                    roundingOff.setText("-₹"+String.format("%.2f",((estimatedFare+estimatedTax-coupanAmount)-Math.round(rideTaken.getEstimatedFare() + rideTaken.getEstimatedFareTax() - rideTaken.getCouponAmount()))));

                totalBillCustomer.setText("₹"+Math.round(rideTaken.getEstimatedFare() + rideTaken.getEstimatedFareTax() - rideTaken.getCouponAmount()));
                paymentAmountCustomer.setText("From your Account ₹" + Math.round(rideTaken.getEstimatedFare() + rideTaken.getEstimatedFareTax() - rideTaken.getCouponAmount()));
                btnSupportDriver.setVisibility(View.GONE);
                rootCustomerSupport.setVisibility(View.VISIBLE);
            } else if (getIntent().getExtras().getString("ridetype", "").equals("ridegiven")){
                RideGiven rideGiven = new Gson().fromJson(getIntent().getExtras().getString("ridedata", ""), RideGiven.class);
                Picasso.with(getApplicationContext()).load(rideGiven.getCustProfilePic()).placeholder(R.drawable.userimage).error(R.drawable.userimage).memoryPolicy(MemoryPolicy.NO_CACHE).into(profilePic);
                name.setText(rideGiven.getCustFirstName());
                pickLocation.setText(rideGiven.getCustPickAddress());
                dropLocation.setText(rideGiven.getCustDropAddress());
                rootDriverFareDetail.setVisibility(View.VISIBLE);
                rootCustomerFareDetail.setVisibility(View.GONE);

                tripBillDriver.setText("₹"+String.format("%.2f",rideGiven.getDriverPayment()));
                platformChargeDriver.setText("-₹"+String.format("%.2f",rideGiven.getDriverPaymentTax()));
                earningDriver.setText("₹"+String.format("%.2f",rideGiven.getDriverPayment() - rideGiven.getDriverPaymentTax()));
                btnSupportDriver.setVisibility(View.VISIBLE);
                rootCustomerSupport.setVisibility(View.GONE);
            } else {
                finish();
            }
        }
    }

    private void initViews() {
        profilePic = (CircleImageView) findViewById(R.id.profile_image_content_ride_history_detail);
        name = (TextView) findViewById(R.id.name_content_ride_history_detail);
        roundingOff=(TextView)findViewById(R.id.rounding_bill_customer_content_ride_history_details);
        rating = (TextView) findViewById(R.id.rating_content_ride_history_detail);
        pickLocation = (TextView) findViewById(R.id.pick_location_content_ride_history_detail);
        dropLocation = (TextView) findViewById(R.id.drop_location_content_ride_history_detail);
        tripBillDriver = (TextView) findViewById(R.id.trip_bill_driver_content_ride_history_detail);
        platformChargeDriver = (TextView) findViewById(R.id.platform_charge_driver_content_ride_history_detail);
        earningDriver = (TextView) findViewById(R.id.earning_driver_content_ride_history_detail);
        rootDriverFareDetail = (LinearLayout) findViewById(R.id.fare_details_driver_content_ride_history);
        rootCustomerFareDetail = (LinearLayout) findViewById(R.id.fare_details_customer_content_ride_history);
        tripFareCustomer = (TextView) findViewById(R.id.trip_fare_customer_content_ride_history_details);
        taxAppliedCustomer = (TextView) findViewById(R.id.tax_applied_customer_content_ride_history_details);
        discountCustomer = (TextView) findViewById(R.id.discount_customer_content_ride_history_details);
        btnSupportDriver = (TextView) findViewById(R.id.btn_support_driver_content_ride_history_detail);
        totalBillCustomer = (TextView) findViewById(R.id.total_bill_customer_content_ride_history_details);
        paymentAmountCustomer = (TextView) findViewById(R.id.payment_amount_customer_content_ride_history_details);
        rootCustomerSupport = (LinearLayout) findViewById(R.id.section_three_content_ride_history_details);







    }

}
