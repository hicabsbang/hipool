package com.hipool.ridehistory;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.app.BaseActivity;
import com.hipool.app.BaseFragment;
import com.hipool.askrole.AskRoleActivity;
import com.hipool.customerhome.HomeActivity;
import com.hipool.models.CustomerDetails;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

import static com.hipool.utils.Constants.APP_PREFS;
import static com.hipool.utils.Constants.CUST_DETAILS;

public class RideHistoryActivity extends BaseActivity {

    private LinearLayout rootTabs;
    private CustomerDetails customerDetails;
    private List<BaseFragment> fragmentList = new ArrayList<>();
    private ViewPager viewPager;
    private TextView rideTakenBtn, rideGivenBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_history);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_activity_ride_history);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Ride History");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        customerDetails = new Gson().fromJson(getSharedPreferences(APP_PREFS, MODE_PRIVATE).getString(CUST_DETAILS, ""), CustomerDetails.class);

        rideTakenBtn = (TextView) findViewById(R.id.textview_tab_ride_taken_content_ride_history);
        rideTakenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRideTaken();
                viewPager.setCurrentItem(0);
            }
        });
        rideGivenBtn = (TextView) findViewById(R.id.textview_tab_ride_given_content_ride_history);
        rideGivenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRideGiven();
                viewPager.setCurrentItem(1);
            }
        });

        rootTabs = (LinearLayout) findViewById(R.id.linear_root_tabs_content_ride_history);
        viewPager = (ViewPager) findViewById(R.id.viewpager_content_ride_history);


        fragmentList.add(new RideTakenHistoryFragment());
        fragmentList.add(new RideGivenHistoryFragment());
        viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragmentList.get(position);
            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0){
                    setRideTaken();
                } else if (position == 1){
                    setRideGiven();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if (customerDetails.getCustType() == 2) {
            rootTabs.setVisibility(View.VISIBLE);
        } else {
            rootTabs.setVisibility(View.GONE);
        }
    }

    private void setRideTaken(){
        rideTakenBtn.setBackgroundColor(getResources().getColor(R.color.transparent));
        rideGivenBtn.setBackgroundColor(getResources().getColor(R.color.gray_tab));
    }

    private void setRideGiven(){
        rideTakenBtn.setBackgroundColor(getResources().getColor(R.color.gray_tab));
        rideGivenBtn.setBackgroundColor(getResources().getColor(R.color.transparent));
    }

}
