package com.hipool.walletactivity;

import android.app.ProgressDialog;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.adapters.WalletHistoryAdapter;
import com.hipool.app.MyApp;
import com.hipool.models.CustomerDetails;
import com.hipool.models.PaymentMethod;
import com.hipool.models.PaytmCheckBalRes;
import com.hipool.models.PaytmGetTokenRes;
import com.hipool.models.PaytmLoginRes;
import com.hipool.models.WalletHistory;
import com.hipool.utils.NetworkUtils;
import com.hipool.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eltos.simpledialogfragment.SimpleDialog;
import eltos.simpledialogfragment.form.Input;
import eltos.simpledialogfragment.form.SimpleFormDialog;

import static com.hipool.utils.APIConstants.BASE_URL;
import static com.hipool.utils.APIConstants.BASE_URL_PAYTM;
import static com.hipool.utils.APIConstants.BASE_URL_PAYTM_WALLET;
import static com.hipool.utils.APIConstants.CHECK_BAL_PAYTM_WALLET;
import static com.hipool.utils.APIConstants.CHECK_TRX_STATUS;
import static com.hipool.utils.APIConstants.GET_PAYMENT_METHOD;
import static com.hipool.utils.APIConstants.GET_TOKEN_URL_PAYTM;
import static com.hipool.utils.APIConstants.GET_WALLET_AMOUNT;
import static com.hipool.utils.APIConstants.GET_WALLET_HISTORY;
import static com.hipool.utils.APIConstants.LOGIN_URL_PAYTM;
import static com.hipool.utils.APIConstants.UPDATE_PAYMENT_METHOD;
import static com.hipool.utils.APIConstants.UPDATE_WALLET_AMOUNT;
import static com.hipool.utils.APIConstants.VALIDATE_TOKEN_URL_PAYTM;
import static com.hipool.utils.APIConstants.WITHDRAW_PAYTM_WALLET;
import static com.hipool.utils.Constants.APP_PREFS;
import static com.hipool.utils.Constants.CLIENT_ID_PAYTM;
import static com.hipool.utils.Constants.CLIENT_SECRET_PAYTM;
import static com.hipool.utils.Constants.CUST_DETAILS;

public class WalletActivity extends AppCompatActivity implements SimpleDialog.OnDialogResultListener {

    private List<PaymentMethod> paymentMethodList = new ArrayList<>();
    private CustomerDetails customerDetails;
    private RadioGroup radioGroupPayment;
    private EditText editTextAmount;
    private TextView textViewProceedButton,textViewADDButton,textViewShowAmount,textViewShowAllTrans,textViewShowDebitTrans,textViewShowCreditTrans;
    private int paymentType=0;
    private String SSToken="",trxId="",orderId="";
    private PaytmLoginRes loginRes;
    private static final String TAG_OTP_DIALOG = "otp_dialog";
    private static final String KEY_OTP = "otp";
    private PaytmGetTokenRes tokenRes;
    private LinearLayout linearLayoutshowWallet,linearLayoutAddMoney;
    private List<WalletHistory> walletHistories=new ArrayList<>();
    private RecyclerView recyclerViewHistory;
    private WalletHistoryAdapter walletHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_activity_ride_history);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Account Detail");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        customerDetails = new Gson().fromJson(getSharedPreferences(APP_PREFS, MODE_PRIVATE).getString(CUST_DETAILS, ""), CustomerDetails.class);
        init();
        showStartUI();
        radioGroupPayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int radioButtonID = radioGroup.getCheckedRadioButtonId();
                switch (radioButtonID)
                {
                    case R.id.Paytm_radio_button : paymentType=1; break;
                    case R.id.phonepe_radio_button: paymentType=2; break;
                    case R.id.credit_radio_button: paymentType=3; break;
                    default: paymentType=0; break;
                }
            }
        });
        textViewProceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMoney();
            }
        });
        textViewShowAllTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                walletHistories.clear();
                walletHistoryAdapter.notifyDataSetChanged();
                textViewShowAllTrans.setClickable(false);
                textViewShowCreditTrans.setClickable(true);
                textViewShowDebitTrans.setClickable(true);

                textViewShowAllTrans.setBackgroundResource(R.drawable.topleftcurvehollow);
                textViewShowDebitTrans.setBackgroundResource(R.color.accent);
                textViewShowCreditTrans.setBackgroundResource(R.drawable.toprightcurev);

                textViewShowAllTrans.setTextColor(getResources().getColor(R.color.accent));
                textViewShowDebitTrans.setTextColor(getResources().getColor(R.color.ef_white));
                textViewShowCreditTrans.setTextColor(getResources().getColor(R.color.ef_white));
                getAllTrans(0);

            }
        });

        textViewShowDebitTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                walletHistories.clear();
                walletHistoryAdapter.notifyDataSetChanged();
                textViewShowAllTrans.setClickable(true);
                textViewShowCreditTrans.setClickable(true);
                textViewShowDebitTrans.setClickable(false);

                textViewShowAllTrans.setBackgroundResource(R.drawable.topleftcurve);
                textViewShowDebitTrans.setBackgroundResource(R.drawable.bg_green_rect);
                textViewShowCreditTrans.setBackgroundResource(R.drawable.toprightcurev);

                textViewShowAllTrans.setTextColor(getResources().getColor(R.color.ef_white));
                textViewShowDebitTrans.setTextColor(getResources().getColor(R.color.accent));
                textViewShowCreditTrans.setTextColor(getResources().getColor(R.color.ef_white));
                getAllTrans(1);
            }
        });

        textViewShowCreditTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                walletHistories.clear();
                walletHistoryAdapter.notifyDataSetChanged();
                textViewShowAllTrans.setClickable(true);
                textViewShowCreditTrans.setClickable(false);
                textViewShowDebitTrans.setClickable(true);

                textViewShowAllTrans.setBackgroundResource(R.drawable.topleftcurve);
                textViewShowDebitTrans.setBackgroundResource(R.color.accent);
                textViewShowCreditTrans.setBackgroundResource(R.drawable.toprightcurvehallow);

                textViewShowAllTrans.setTextColor(getResources().getColor(R.color.ef_white));
                textViewShowDebitTrans.setTextColor(getResources().getColor(R.color.ef_white));
                textViewShowCreditTrans.setTextColor(getResources().getColor(R.color.accent));
                getAllTrans(2);
            }
        });
        textViewADDButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddPaymentUI();
            }
        });


    }

    private void showStartUI() {
        getWalletupdate();
        linearLayoutAddMoney.setVisibility(View.GONE);
        linearLayoutshowWallet.setVisibility(View.VISIBLE);
    }
    private void showAddPaymentUI() {
        linearLayoutAddMoney.setVisibility(View.VISIBLE);
        linearLayoutshowWallet.setVisibility(View.GONE);
    }

    private void getWalletupdate() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Fetching account detail");
        progressDialog.setMessage("please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + GET_WALLET_AMOUNT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("data");
                    JSONObject jsonObject1=jsonArray.getJSONObject(0);
                    String credit=jsonObject1.getString("credit");
                    double creditD=Double.valueOf(credit);
                    if((creditD-(int)creditD)!=0)
                    textViewShowAmount.setText("₹"+String.format("%.2f",creditD));
                    else
                        textViewShowAmount.setText("₹"+String.format("%.0f",creditD));
                    getAllTrans(0);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("updateCustPay", error.toString());
                progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id",String.valueOf(customerDetails.getCustId()));
                Log.v("paymentParams", params.toString());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void getAllTrans(final int status) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + GET_WALLET_HISTORY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("data");
                    walletHistories.clear();
                    List<WalletHistory> walletHistoriesLocal = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        WalletHistory walletHistory = new Gson().fromJson(jsonArray.getString(i), WalletHistory.class);
                        walletHistoriesLocal.add(walletHistory);
                    }
                    walletHistories.addAll(walletHistoriesLocal);
                    walletHistoryAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("updateCustPay", error.toString());

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id",String.valueOf(customerDetails.getCustId()));
                params.put("status",String.valueOf(status));
                Log.v("paymentParams", params.toString());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void addMoney() {
        boolean toAddPaytm = true;
        for (int i = 0; i < paymentMethodList.size(); i++) {
            if (paymentMethodList.get(i).getPaymentMethodType() == 1) {
                SSToken=paymentMethodList.get(i).getAccessToken();
                validatePayTmAccessToken(paymentMethodList.get(i).getAccessToken());

                toAddPaytm = false;
                break;
            }
        }
        if (toAddPaytm) {
            Toast.makeText(getApplicationContext(), "Please Add paytm Payment Method", Toast.LENGTH_SHORT).show();
            loginPayTm();
        }
    }


    private void loginPayTm(){
       final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", Utils.getDeviceEmail(getApplicationContext()));
            jsonObject.put("phone", customerDetails.getCustContactNo());
            jsonObject.put("clientId", CLIENT_ID_PAYTM);
            jsonObject.put("scope", "wallet");
            jsonObject.put("responseType", "token");
            Log.v("paramss", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_PAYTM + LOGIN_URL_PAYTM, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {
                Log.v("ressss", response.toString());
                loginRes = new Gson().fromJson(response.toString(), PaytmLoginRes.class);
                if (loginRes.getStatus().equals("SUCCESS")){
                    Toast.makeText(getApplicationContext(), loginRes.getMessage(), Toast.LENGTH_LONG).show();
                    showOtpInputDialog();
                } else {
                    Toast.makeText(getApplicationContext(), "failed!", Toast.LENGTH_LONG).show();
                }

                progressDialog.dismiss();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errrr", error.toString());
//                Toast.makeText(getApplicationContext(), "something went wrong!", Toast.LENGTH_LONG).show();
               progressDialog.dismiss();
            }
        });

        jsonObjectRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(jsonObjectRequest);

    }

    private void showOtpInputDialog(){
        SimpleFormDialog.build()
                .title("paytm")
                .fields(
                        Input.plain(KEY_OTP).required().hint("OTP").max(6).min(6).inputType(InputType.TYPE_CLASS_NUMBER)
                )
                .pos("Ok")
                .neg("Cancel")
                .show(this, TAG_OTP_DIALOG);
    }



    private void validatePayTmAccessToken(final String token) {
        if (!NetworkUtils.isInternetConnected(this))
            return;

        if (token == null || token.equals("")) {
            return;
        }

        final MaterialDialog materialDialog = new MaterialDialog.Builder(this)
                .title("paytm")
                .content("Please wait..")
                .progress(true, 0)
                .cancelable(false)
                .show();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, BASE_URL_PAYTM + VALIDATE_TOKEN_URL_PAYTM, null, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {

                    Log.v("validateRes", response.toString());
                try {
                    if (!response.toString().contains("FAILURE")) {
                        checkBalPayTm(token);

                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loginPayTm();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                materialDialog.dismiss();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("validateRes", error.toString());
//                Toast.makeText(getApplicationContext(), "something went wrong!", Toast.LENGTH_SHORT).show();
                materialDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("session_token", token);
                return headers;
            }
        };

        jsonObjectRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    private void checkBalPayTm(final String token) {

        if (!NetworkUtils.isInternetConnected(this))
            return;

        if (token == null || token.equals("")) {
            return;
        }

        final MaterialDialog materialDialog = new MaterialDialog.Builder(this)
                .title("paytm")
                .content("Please wait.. ")
                .progress(true, 0)
                .cancelable(false)
                .show();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_PAYTM_WALLET + CHECK_BAL_PAYTM_WALLET, null, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {
                //if (isDebug)
                Log.e("checkPaytmBal", response.toString());
                final PaytmCheckBalRes checkBalRes = new Gson().fromJson(response.toString(), PaytmCheckBalRes.class);
                if (checkBalRes.getStatusCode().equals("SUCCESS")) {
                    double enteramount=Double.valueOf(editTextAmount.getText().toString());
                    double paytmBalance=checkBalRes.getResponse().getAmount();
                    if(paytmBalance>=enteramount)
                    {
                        payByPayTm((int) enteramount);
                    }
                    else {

                    }

                } else {
                    Toast.makeText(getApplicationContext(), "paytm check balance failed!", Toast.LENGTH_SHORT).show();
                }

                materialDialog.dismiss();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("checkPaytmBal", error.toString());
                materialDialog.dismiss();
                loginPayTm();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("ssotoken", token);
                return headers;
            }
        };

        jsonObjectRequest.setShouldCache(false);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApp.getInstance().addToRequestQueue(jsonObjectRequest);

    }

    private void payByPayTm(final int amount) {

        if (!NetworkUtils.isInternetConnected(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), "no internet!", Toast.LENGTH_SHORT).show();

            return;
        }

        final MaterialDialog materialDialog = new MaterialDialog.Builder(this)
                .title("paytm")
                .content("Please wait..")
                .progress(true, 0)
                .cancelable(false)
                .show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + WITHDRAW_PAYTM_WALLET, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                    Log.v("payByPaytm", response);
                materialDialog.dismiss();
                try {
                    String status = new JSONObject(response).getString("Status");
                    if (status.toLowerCase().contains("success")) {
                        Toast.makeText(getApplicationContext(), new JSONObject(response).getString("ResponseMessage"), Toast.LENGTH_SHORT).show();
                        trxId = new JSONObject(response).getString("TxnId");
                        checkPaytmTrx(amount);
                    } else if (status.toLowerCase().contains("failure")) {
                        Toast.makeText(getApplicationContext(), new JSONObject(response).getString("ResponseMessage"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (response.contains("Duplicate order id")) {
                        Toast.makeText(getApplicationContext(), "paytm Payment cant be done on this Trip", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                    Log.e("payByPaytm", error.toString());
                Toast.makeText(getApplicationContext(), "Payment Not Done", Toast.LENGTH_SHORT).show();
                materialDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

//                String orderId = rideReciept.getOrderID() + "D" + Utils.getUnixTime();
//                rideReciept.setOrderID(orderId);
                orderId="OTOBPTM"+Utils.getUnixTime();
                params.put("TxnAmount", String.valueOf(amount));
                params.put("OrderId", orderId);
                params.put("DeviceId", customerDetails.getCustContactNo());
                params.put("SSOToken", SSToken);
                params.put("CustId", String.valueOf(customerDetails.getCustId()));
                    Log.v("payByPaytmP", params.toString());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                -1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void checkPaytmTrx(final int amount) {

        if (!NetworkUtils.isInternetConnected(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), "no internet!", Toast.LENGTH_SHORT).show();

            return;
        }

        final MaterialDialog materialDialog = new MaterialDialog.Builder(this)
                .title("paytm")
                .content("Please wait..")
                .progress(true, 0)
                .cancelable(false)
                .show();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ORDERID", orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + CHECK_TRX_STATUS, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {

                    Log.v("paytmtrxstatus", response.toString());
                materialDialog.dismiss();

                try {
                    if (response.getString("STATUS").equals("TXN_SUCCESS")) {
                        updateCustomerWallet(amount,1);

                    } else {
                        Toast.makeText(getApplicationContext(), "paytm Payment Failed!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Waiting for Confirmation!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                    Log.e("paytmtrxstatus", error.toString());
                materialDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Waiting for Confirmation!", Toast.LENGTH_SHORT).show();
            }
        });

        jsonObjectRequest.setShouldCache(false);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApp.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void updateCustomerWallet(final int amount, final int coming_from) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + UPDATE_WALLET_AMOUNT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("updateCustPay", response);
                getCustomerPaymentMethod();
                showStartUI();
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("updateCustPay", error.toString());
                progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id",String.valueOf(customerDetails.getCustId()));
                params.put("amount",String.valueOf(amount));
                params.put("orderId",orderId);
                params.put("coming_from",String.valueOf(coming_from));
                Log.v("paymentParams", params.toString());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void init() {
        radioGroupPayment=(RadioGroup)findViewById(R.id.radio_group_payment);
        editTextAmount=(EditText)findViewById(R.id.input_mobile_content_login);
        textViewProceedButton=(TextView)findViewById(R.id.proceed_button);
        linearLayoutshowWallet=(LinearLayout)findViewById(R.id.linearLayout_show_payment_history);
        linearLayoutAddMoney=(LinearLayout)findViewById(R.id.add_payment_layout);
        textViewADDButton=(TextView)findViewById(R.id.textViewAddBalance);
        textViewShowAllTrans=(TextView)findViewById(R.id.textViewAllButton);
        textViewShowAmount=(TextView)findViewById(R.id.balanceTextview);
        textViewShowDebitTrans=(TextView)findViewById(R.id.textViewDebitButton);
        textViewShowCreditTrans=(TextView)findViewById(R.id.textViewCreditButton);
        recyclerViewHistory=(RecyclerView)findViewById(R.id.recyclerViewHistory);
        recyclerViewHistory.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewHistory.setItemAnimator(new DefaultItemAnimator());
        walletHistoryAdapter=new WalletHistoryAdapter(walletHistories,this);
        recyclerViewHistory.setAdapter(walletHistoryAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getCustomerPaymentMethod();
    }

    private void getCustomerPaymentMethod() {

        if (!NetworkUtils.isInternetConnected(this))
            return;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + GET_PAYMENT_METHOD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                    Log.v("getCustPay", response);

                try {
                    JSONObject resObject = new JSONObject(response);
                    JSONArray methodArray = resObject.getJSONArray("data");

                    paymentMethodList.clear();
                    for (int i = 0; i < methodArray.length(); i++) {
                        PaymentMethod paymentMethod = new Gson().fromJson(methodArray.getString(i), PaymentMethod.class);
                        paymentMethodList.add(paymentMethod);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("getCustPay", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id", String.valueOf(customerDetails.getCustId()));
               // Log.v("paymentParams", params.toString());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onResult(@NonNull String dialogTag, int which, @NonNull Bundle extras) {
        if (dialogTag.equals(TAG_OTP_DIALOG)){
            if (which == BUTTON_POSITIVE){
                getToken(extras.getString(KEY_OTP));
            } else if (which == BUTTON_NEGATIVE){
                Toast.makeText(getApplicationContext(), "cancelled!", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "paymrnt method not added!", Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return false;
    }

    private void getToken(String otp){

        if (loginRes == null){
            return;
        }

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("otp", otp);
            jsonObject.put("state", loginRes.getState());
            Log.v("paramss", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL_PAYTM + GET_TOKEN_URL_PAYTM, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {
                Log.v("ressss", response.toString());
                try {
                    if (!response.toString().contains("Invalid OTP")){
                        tokenRes = new Gson().fromJson(response.toString(), PaytmGetTokenRes.class);
                        Toast.makeText(getApplicationContext(), "success!", Toast.LENGTH_LONG).show();
                        updateCustomerPaymentMethod(1);
                    } else {
                        Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errrr", error.toString());
//                Toast.makeText(getApplicationContext(), "something went wrong!", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String credentials = CLIENT_ID_PAYTM + ":" + CLIENT_SECRET_PAYTM;
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);
                return headers;
            }
        };

        jsonObjectRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void updateCustomerPaymentMethod(final int i) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + UPDATE_PAYMENT_METHOD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("updateCustPay", response);
                getCustomerPaymentMethod();
                addMoney();
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("updateCustPay", error.toString());
                progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id",String.valueOf(customerDetails.getCustId()));
                if (i == 1){
                    params.put("payment_method_type", String.valueOf(1));
                    params.put("access_token", tokenRes.getAccessToken());
                } else if (i == 2){
                    params.put("payment_method_type", String.valueOf(2));
                    params.put("access_token", "");
                }
                Log.v("paymentParams", params.toString());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);

    }
}
