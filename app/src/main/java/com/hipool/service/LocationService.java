package com.hipool.service;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.hipool.app.MyApp;
import com.hipool.models.CustomerRequest;
import com.hipool.utils.Prefs;
import com.hipool.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.hipool.utils.APIConstants.BASE_URL;
import static com.hipool.utils.APIConstants.UPDATE_LOCATION;
import static com.hipool.utils.Constants.CUST_ID;

/**
 * Created by ashut on 28-07-2017.
 */

public class LocationService extends Service implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        LocationListener {

    public static final String TAG = LocationService.class.getSimpleName();

    private static LocationService mInstance;
    private Location mLastKnownLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters

    private String mLastLocationName;

    public static synchronized LocationService getInstance(){
        return mInstance;
    }


    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.getDefault());

    public LocationService() {
        super();
        Log.e(TAG, "onCreate");
        mInstance = this;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "onBind");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        if (mGoogleApiClient == null) {
            buildGoogleApiClient();
        } else {
            mGoogleApiClient.connect();
        }
        super.onStartCommand(intent, flags, startId);
        return Service.START_STICKY;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.v(TAG, "Play services connected");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v(TAG, "Play services connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // Refer to the reference doc for ConnectionResult to see what error codes might
        // be returned in onConnectionFailed.
        Log.v(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastKnownLocation = location;
//        setLastLocationName();
        Prefs.setMyLat(getApplicationContext(), location.getLatitude());
        Prefs.setMyLon(getApplicationContext(), location.getLongitude());
        if (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(CUST_ID, 0) != 0) {
            {
                try {
                    JSONObject jsonObject=new JSONObject();
                    jsonObject.put("cust_id",String.valueOf(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(CUST_ID, 0)));
                    jsonObject.put("current_lat",location.getLatitude());
                    jsonObject.put("current_lon",location.getLongitude());
                    BookingService.getInstance().LocationUpdate(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            //updateLocation(location.getLatitude(), location.getLongitude());
            try {
                if (!Prefs.getLiveBookingId(getApplicationContext()).equals("")) {
                    if (Prefs.getDriverReachedAtPickLocationTime(getApplicationContext()).equals("")) {
                        if (!Prefs.getCustomerPickLat(getApplicationContext()).equals("") && !Prefs.getCustomerPickLon(getApplicationContext()).equals("")) {
                            if (Utils.getEstDistance(new LatLng(location.getLatitude(), location.getLongitude()), new LatLng(Double.valueOf(Prefs.getCustomerPickLat(getApplicationContext())), Double.valueOf(Prefs.getCustomerPickLon(getApplicationContext())))) < .2d); {
                                Prefs.setDriverReachedAtPickLocationTime(getApplicationContext(), dateFormat.format(Calendar.getInstance().getTime()));
                            }
                        }
                    }
                }
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        stopLocationUpdates();
        super.onDestroy();
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.v(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        createLocationRequest();
    }

    /**
     * Creating location request object
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
        mGoogleApiClient.connect();
    }

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public Location getLastKnownLocation() {
//        return mLastKnownLocation;
//    }
//
//    public String getLastLocationName() {
//        return mLastLocationName;
//    }

    private void setLastLocationName() {
        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + mLastKnownLocation.getLatitude() + "," + mLastKnownLocation.getLongitude() + "&result_type=locality&key=AIzaSyCjll3njSmEEGalf2iGBeReakB_BvgWdhE";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v(TAG, response);
                try {
                    String address = new JSONObject(response).getJSONArray("results").getJSONObject(0).getString("formatted_address");
                    mLastLocationName = address;
//                    Prefs.setMyLocationName(getApplicationContext(), address);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
            }
        });

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void updateLocation(final double lat, final double lon) {

//        if (!Prefs.getLiveBookingId(getApplicationContext()).equals("")) {
//            CustomerRequest customerRequest = new Gson().fromJson(Prefs.getCustomerRequests(getApplicationContext()), CustomerRequest.class);
//
//        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + UPDATE_LOCATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v(TAG, "updatelocation:" + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "updatelocation:" + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id", String.valueOf(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(CUST_ID, 0)));
                params.put("current_lat", String.valueOf(lat));
                params.put("current_lon", String.valueOf(lon));
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }
}
