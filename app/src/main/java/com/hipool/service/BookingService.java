package com.hipool.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.SplashActivity;
import com.hipool.app.MyApp;
import com.hipool.models.CustomerDetails;
import com.hipool.riderrequests.RiderRequestsActivity;
import com.hipool.utils.HiPoolSocketUtils;
import com.hipool.utils.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.hipool.utils.APIConstants.BASE_URL;
import static com.hipool.utils.APIConstants.SOS;
import static com.hipool.utils.APIConstants.UPDATE_LOCATION;
import static com.hipool.utils.Constants.CUST_DETAILS;
import static com.hipool.utils.Constants.CUST_ID;
import static com.hipool.utils.HiPoolSocketUtils.EVENT_CONFIRM_CUSTOMER_BOOKING;
import static com.hipool.utils.HiPoolSocketUtils.EVENT_UPDATE_LOCATION;


/**
 * Created by Abhinav on 16/09/2017.
 */

public class BookingService extends Service {

    private Socket socket;
    private static BookingService mInstance;
    private String TAG = "BookingService";
    String customer_id = "";
    public static final String SOCKET_DATA_RECIEVED_EVENT = "socket_data_recieved_event";
    public static final String SOCKET_EVENT = "socket_event";
    public static final String SOCKET_DATA = "socket_data";
    CustomerDetails customerDetails;

    public static synchronized BookingService getInstance() {
        return mInstance;
    }

    public BookingService() {
        super.onCreate();
        Log.e(TAG, "onCreate");
        socket = MyApp.getInstance().getSocket();
        mInstance = this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "OnstartCommand");
        customerDetails = new Gson().fromJson(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(CUST_DETAILS, ""), CustomerDetails.class);
        if (socket == null) {
            socket = MyApp.getInstance().getSocket();
            createSocketConnection();
        } else {
            createSocketConnection();
        }
        super.onStartCommand(intent, flags, startId);
        return Service.START_STICKY;
    }

    private void createSocketConnection() {
        socket.on(HiPoolSocketUtils.EVENT_CONNECT, eventConnect)
                .on(HiPoolSocketUtils.EVENT_CONNECTED, eventConnected)
                .on(HiPoolSocketUtils.EVENT_DRIVER_NOTIFICATION, eventDriverNotified)
                .on(HiPoolSocketUtils.EVENT_CHANGE_RIDE_STATUS, changeRideStatus)
                .on(HiPoolSocketUtils.EVENT_UPDATE_LOCATION,onLocationupdate)
                .on(HiPoolSocketUtils.EVENT_DRIVER_RATING,eventDriverRating)
                .on(HiPoolSocketUtils.EVENT_CUSTOMER_RATING,eventCustomerRating)
                .on(EVENT_CONFIRM_CUSTOMER_BOOKING, confirmCustomerBooking);
        socket.connect();
    }

    private void destroySocketConnection() {
        socket.disconnect();
        socket
                .off(HiPoolSocketUtils.EVENT_CONNECT, eventConnect)
                .off(HiPoolSocketUtils.EVENT_CONNECTED, eventConnected)
                .off(HiPoolSocketUtils.EVENT_DRIVER_NOTIFICATION, eventDriverNotified)
                .off(HiPoolSocketUtils.EVENT_CHANGE_RIDE_STATUS, changeRideStatus)
                .off(HiPoolSocketUtils.EVENT_UPDATE_LOCATION,onLocationupdate)
                .off(HiPoolSocketUtils.EVENT_DRIVER_RATING,eventDriverRating)
                .off(HiPoolSocketUtils.EVENT_CUSTOMER_RATING,eventCustomerRating)
                .off(EVENT_CONFIRM_CUSTOMER_BOOKING, confirmCustomerBooking);
    }

    private void updateSocketId() {
        JSONObject object = new JSONObject();
        try {
            object.put("id", customerDetails.getCustId());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        Log.e(TAG, "connectiondata:" + object);
        socket.emit("connectiondata", object);
    }

    public void updateCustomerLoginStatus(int status) {
        JSONObject object = new JSONObject();
        try {
            object.put("cust_id", customerDetails.getCustId());
            object.put("customer_login_status", status);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        Log.e(TAG, ":changeCustLoginStatus:" + object);
        socket.emit("changeCustomerLoginStatus", object);
    }

    public void bookCab(String bookingData) {
        try {
            JSONObject bookingObject = new JSONObject(bookingData);
            Log.v(TAG, ":bookCab:" + bookingObject);
            socket.emit("notifiedDriver", bookingObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void changeRideStatus(String rideStatus) {
        try {
            JSONObject object = new JSONObject();
            object.put("booking_id", Prefs.getLiveBookingId(getApplicationContext()));
            object.put("ride_status", rideStatus);
            Log.v(TAG, ":bookCab:" + object);
            socket.emit("changeRideStatus", object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void LocationUpdate(JSONObject jsonObject) {
        Log.v(TAG, ":onLocationupdate:" + jsonObject);
        socket.emit("onLocationupdate", jsonObject);

    }

    private void sendTokenSocket() {
        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null) {
            try {
                JSONObject object = new JSONObject();
                object.put("cust_id", customerDetails.getCustId());
                object.put("customer_device_id", token);
                socket.emit("updateCustomerDeviceID", object);
                Log.e("deviceTokenFirebase", token);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void confirmCustomerBooking(JSONObject jsonObject) {
        socket.emit("confirmBooking", jsonObject);
        Log.v(TAG, EVENT_CONFIRM_CUSTOMER_BOOKING + ":" + jsonObject);
    }

    private Emitter.Listener eventConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                sendData(HiPoolSocketUtils.EVENT_CONNECT, args.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private Emitter.Listener eventConnected = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                try {
                    sendData(HiPoolSocketUtils.EVENT_CONNECTED, (String) args[0]);
                    updateSocketId();
                    updateCustomerLoginStatus(1);
                    sendTokenSocket();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private Emitter.Listener eventDriverNotified = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            sendData(HiPoolSocketUtils.EVENT_DRIVER_NOTIFICATION, ((JSONObject) args[0]).toString());
            JSONObject resObject = ((JSONObject) args[0]);
            try {
                JSONArray jsonArray = resObject.getJSONArray("content");
                JSONObject requestObject = jsonArray.getJSONObject(0);
                String prefString = Prefs.getCustomerRequests(getApplicationContext());
                JSONArray reqArray = new JSONArray();
                if (!prefString.equals("")) {
                    reqArray = new JSONArray(prefString);
                }
                reqArray.put(requestObject);
                Prefs.setCustomerRequests(getApplicationContext(), reqArray);
                createCustomerRequestNotification(requestObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private Emitter.Listener changeRideStatus = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            sendData(HiPoolSocketUtils.EVENT_CHANGE_RIDE_STATUS, ((JSONObject) args[0]).toString());
        }
    };

    private Emitter.Listener eventDriverRating = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            sendData(HiPoolSocketUtils.EVENT_DRIVER_RATING, ((JSONObject) args[0]).toString());
        }
    };

    private Emitter.Listener eventCustomerRating = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            sendData(HiPoolSocketUtils.EVENT_CUSTOMER_RATING, ((JSONObject) args[0]).toString());
        }
    };

    private Emitter.Listener onLocationupdate = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            sendData(EVENT_UPDATE_LOCATION, ((JSONObject) args[0]).toString());
        }
    };
    private Emitter.Listener confirmCustomerBooking = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            sendData(EVENT_CONFIRM_CUSTOMER_BOOKING, ((JSONObject) args[0]).toString());
        }
    };

    private void sendData(String eventConnect, String data) {
        Intent intent = new Intent(SOCKET_DATA_RECIEVED_EVENT);
        intent.putExtra(SOCKET_EVENT, eventConnect);
        intent.putExtra(SOCKET_DATA, data);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        destroySocketConnection();
        super.onDestroy();
    }

    private void createCustomerRequestNotification(JSONObject requestObject) {

        String name = "";

        try {
            name = requestObject.getString("cust_first_name");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent resultIntent = new Intent(this, RiderRequestsActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_ONE_SHOT
                );

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Ride Request")
                        .setContentText(name + " requested a ride")
                        .setContentIntent(resultPendingIntent);

        int mNotificationId = 001;
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    public void updateCustomerRatings(int bookingId, String ratingValue, int feedbackSelectedId, String s) {
        try {
            JSONObject object = new JSONObject();
            object.put("booking_id", bookingId);
            object.put("star_rating", ratingValue);
            if (feedbackSelectedId != 0) {
                object.put("feedback_id", feedbackSelectedId);
            } else {
                object.put("feedback_id", 0);
            }
            if (!s.equals("")) {
                object.put("comment", s);
            } else {
                object.put("comment", "");
            }
            Log.v(TAG, ":customerRating:" + object);
            socket.emit("customerRating", object);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }


    public void sos(final String side) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + SOS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v(TAG, "sos:" + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "sos:" + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id", String.valueOf(customerDetails.getCustId()));
                params.put("booking_id", Prefs.getLiveBookingId(getApplicationContext()));
                params.put("side", side);
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    public void updateDriverRatings(int booking_id, String ratingValue, int feedbackSelectedId, String s) {
        try {
            JSONObject object = new JSONObject();
            object.put("booking_id", booking_id);
            object.put("star_rating", ratingValue);
            if (feedbackSelectedId != 0) {
                object.put("feedback_id", feedbackSelectedId);
            } else {
                object.put("feedback_id", 0);
            }
            if (!s.equals("")) {
                object.put("comment", s);
            } else {
                object.put("comment", "");
            }
            Log.v(TAG, ":customerRating:" + object);
            socket.emit("driverRating", object);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }
}
