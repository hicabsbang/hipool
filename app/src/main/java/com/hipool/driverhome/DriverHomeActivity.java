package com.hipool.driverhome;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.fragment.DropLocationFragment;
import com.hipool.fragment.HomeFragment;
import com.hipool.models.CustomerDetails;
import com.hipool.ridehistory.RideHistoryActivity;
import com.hipool.service.BookingService;
import com.hipool.settings.SettingsActivity;
import com.hipool.walletactivity.WalletActivity;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.hipool.utils.Constants.APP_PREFS;
import static com.hipool.utils.Constants.CUST_DETAILS;

/**
 * Created by Abhinav on 18/09/2017.
 */

public class DriverHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private HomeFragment homeFragment;
    private CustomerDetails customerDetails;
    TextView textViewName,textViewPhone;
    CircleImageView imageViewProfile;
    NavigationView navigationView;
    TextView toot_bar_title;
    private DropLocationFragment dropLocationFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_activity_home);
        toolbar.setTitle("");
        toot_bar_title=(TextView)toolbar.findViewById(R.id.toolbar_title);
        toot_bar_title.setText("Select Your Route");

        setSupportActionBar(toolbar);

        customerDetails = new Gson().fromJson(getSharedPreferences(APP_PREFS, MODE_PRIVATE).getString(CUST_DETAILS, ""), CustomerDetails.class);


        init();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_activity_home);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        fab.hide();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();



        homeFragment = new HomeFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container_content_home, homeFragment).commit();


    }

    private void init() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_home);
        textViewName=(TextView)headerLayout.findViewById(R.id.name_nav_header_home);
        textViewPhone=(TextView)headerLayout.findViewById(R.id.mobile_nav_header_home);
        imageViewProfile=(CircleImageView)headerLayout.findViewById(R.id.profile_image_nav_header_home);


        textViewName.setText(customerDetails.getCustFirstName());
        textViewPhone.setText(customerDetails.getCustContactNo());

        Picasso.with(getApplicationContext()).load(customerDetails.getCustProfilePic().replace(" ", "%20")).fit().error(R.drawable.driverdefault).placeholder(R.drawable.driverdefault).memoryPolicy(MemoryPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_STORE).into(imageViewProfile);

    }

    @Override
    protected void onStart() {
        startService(new Intent(DriverHomeActivity.this, BookingService.class));
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.book_ride) {
            // Handle the camera action
            getSupportFragmentManager().beginTransaction().replace(R.id.container_content_home, homeFragment).commit();
        } else if (id == R.id.ride_history) {
            startActivity(new Intent(getApplicationContext(), RideHistoryActivity.class));
        } else if (id == R.id.rate_card) {

        } else if (id == R.id.payments) {
            startActivity(new Intent(getApplicationContext(), WalletActivity.class));
        } else if (id == R.id.settings) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
