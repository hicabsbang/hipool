package com.hipool.feedback;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.app.MyApp;
import com.hipool.models.CurrentDetail;
import com.hipool.models.CustomerCurrentDetail;
import com.hipool.models.CustomerDetails;
import com.hipool.models.Feedback;
import com.hipool.service.BookingService;
import com.hipool.utils.Prefs;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.hipool.utils.APIConstants.BASE_URL;
import static com.hipool.utils.APIConstants.CUSTOMER_GURRENT_STATUS;
import static com.hipool.utils.APIConstants.GET_CUSTOMER_FEEDBACKS;
import static com.hipool.utils.APIConstants.GET_FEEDBACKS;
import static com.hipool.utils.Constants.APP_PREFS;
import static com.hipool.utils.Constants.CUST_DETAILS;
import static com.hipool.utils.Constants.IS_DEBUG;

public class FeedbackByCustomerActivity extends AppCompatActivity {


    private final String TAG = FeedbackByCustomerActivity.class.getSimpleName();
    private CircleImageView circleImageView;
    private TextView nameCustomer, pickLocation, dropLocation, ratingComment, btnSubmit,textViewFare;
    private MaterialSpinner spinnerFeedbacks;
    private EditText feedbackComments;
    private RatingBar ratingBar;
    private String ratingValue;
    private List<Feedback> feedbackList = new ArrayList<>();
    private int feedbackSelectedId = 0;
    private CurrentDetail currentDetail;
    private CustomerDetails customerDetails;
    private int booking_id;
    public static boolean isActivityRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_by_customer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_activity_ride_history_detail);
        toolbar.setTitle("Rider FeedBack");
        init();
        customerDetails = new Gson().fromJson(getSharedPreferences(APP_PREFS, MODE_PRIVATE).getString(CUST_DETAILS, ""), CustomerDetails.class);
        getFeedbacks();
        getStatus();
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                ratingValue = String.valueOf(v);
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BookingService.getInstance().updateDriverRatings(booking_id, ratingValue, feedbackSelectedId, feedbackComments.getText().toString());
                finish();
            }
        });
    }

    private void init() {
        textViewFare=(TextView)findViewById(R.id.fare_customer_content_feedback_by_driver);
        circleImageView = (CircleImageView) findViewById(R.id.profilepic_content_feedback_by_driver);
        nameCustomer = (TextView) findViewById(R.id.name_customer_content_feedback_by_driver);
        pickLocation = (TextView) findViewById(R.id.pick_location_content_feedback_by_driver);
        dropLocation = (TextView) findViewById(R.id.drop_location_content_feedback_by_driver);
        ratingBar = (RatingBar) findViewById(R.id.ratingbar_content_feedback_by_driver);
        ratingComment = (TextView) findViewById(R.id.textview_rating_comment_content_feedback_by_driver);
        spinnerFeedbacks = (MaterialSpinner) findViewById(R.id.spinner_feedback_layout_feedback);
        feedbackComments = (EditText) findViewById(R.id.editText_comments_layout_feedback);
        btnSubmit = (TextView) findViewById(R.id.textview_btn_submit_layout_feedback);
    }
    private void getFeedbacks() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, BASE_URL + GET_CUSTOMER_FEEDBACKS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (IS_DEBUG) {
                    Log.v(TAG, "res:" + response);
                }

                try {
                    JSONArray feedbackArray = new JSONArray(response);
                    feedbackList.clear();
                    for (int i = 0; i < feedbackArray.length() ; i++) {
                        Feedback feedback = new Gson().fromJson(feedbackArray.getString(i), Feedback.class);
                        feedbackList.add(feedback);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String[] items = new String[feedbackList.size() + 1];
                            items[0] = "Please select";
                            for (int i = 0; i < feedbackList.size(); i++) {
                                items[i + 1] = feedbackList.get(i).getFeedback();
                            }
                            spinnerFeedbacks.setItems(items);
                            spinnerFeedbacks.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
                                @Override
                                public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                                    if (position == 0) {
                                        feedbackSelectedId = 0;
                                    } else {
                                        feedbackSelectedId = feedbackList.get(position - 1).getFeedbackId();
                                    }
                                }
                            });
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (IS_DEBUG) {
                    Log.v(TAG, "err:" + error.toString());
                }
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void getStatus() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + CUSTOMER_GURRENT_STATUS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("getStatus", "getStatus" + response);
                if (response.contains("Success")){
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                        CustomerCurrentDetail customerCurrentDetail=new Gson().fromJson(jsonObject.getJSONObject("data").toString(), CustomerCurrentDetail.class);
                        nameCustomer.setText(customerCurrentDetail.getCustFirstName());
                        pickLocation.setText(customerCurrentDetail.getCustPickAddress());
                        dropLocation.setText(customerCurrentDetail.getCustDropAddress());
                        textViewFare.setText("₹"+String.format("%.2f", customerCurrentDetail.getEstimatedFare()+customerCurrentDetail.getEstimatedFareTax()));
                        Picasso.with(getApplicationContext()).load(customerCurrentDetail.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(circleImageView);
                        booking_id=customerCurrentDetail.getBookingId();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (response.contains("Customer Current Details Not Coming")) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("getStatus", "getStatus" + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id", String.valueOf(customerDetails.getCustId()));
                params.put("customer_access_token", customerDetails.getCustomerAccessToken());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    protected void onDestroy() {
        isActivityRunning=false;
        super.onDestroy();
    }
}
