package com.hipool;

import android.*;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.hipool.askrole.AskRoleActivity;
import com.hipool.customerhome.HomeActivity;
import com.hipool.driverhome.DriverHomeActivity;
import com.hipool.login.LoginActivity;
import com.hipool.models.CustomerDetails;
import com.hipool.service.LocationService;
import com.hipool.utils.Prefs;
import com.hipool.utils.Utils;

import static com.hipool.utils.Constants.APP_PREFS;
import static com.hipool.utils.Constants.CUST_DETAILS;
import static com.hipool.utils.Constants.KEY_IS_LOGGED_IN;

public class SplashActivity extends AppCompatActivity {


    private static final String TAG = SplashActivity.class.getSimpleName();
    private static final int REQUEST_CODE_ALL_PERMISSIONS = 101;
    private static final String[] PERMISSIONS_LIST = {
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS
    };
    private boolean isNextActivityStarted = false;
    private RelativeLayout rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        rootView = (RelativeLayout) findViewById(R.id.root_splash);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!Utils.isInternetConnected(getApplicationContext())) {
            showDialog("No Internet!");
        } else if (!Utils.isGPSEnabled(getApplicationContext())) {
            showDialog("Turn ON the GPS!");
        } else {
            ActivityCompat.requestPermissions(this, PERMISSIONS_LIST, REQUEST_CODE_ALL_PERMISSIONS);
        }
    }

    private void showDialog(final String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_INDEFINITE).setAction("Ok", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (message.contains("Internet")) {
                    finish();
                } else if (message.contains("GPS")) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                } else {
                    ActivityCompat.requestPermissions(SplashActivity.this, PERMISSIONS_LIST, REQUEST_CODE_ALL_PERMISSIONS);
                }
            }
        }).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ALL_PERMISSIONS:
                if (grantResults.length > 0) {
                    if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        showDialog("Let hipool access device location");
                    } else if (grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                        showDialog("Let hipool access device storage");
                    } else if (grantResults[2] != PackageManager.PERMISSION_GRANTED) {
                        showDialog("Let hipool access device storage");
                    } else if (grantResults[3] != PackageManager.PERMISSION_GRANTED) {
                        showDialog("Let hipool access device camera");
                    } else if (grantResults[4] != PackageManager.PERMISSION_GRANTED) {
                        showDialog("Let hipool make calls");
                    } else if (grantResults[5] != PackageManager.PERMISSION_GRANTED) {
                        showDialog("Let hipool read sms");
                    } else if (grantResults[6] != PackageManager.PERMISSION_GRANTED) {
                        showDialog("Let hipool recieve sms");
                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!isNextActivityStarted) {
                                    isNextActivityStarted = true;
                                    if (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean(KEY_IS_LOGGED_IN, false)) {
                                        CustomerDetails customerDetails = new Gson().fromJson(getSharedPreferences(APP_PREFS, MODE_PRIVATE).getString(CUST_DETAILS, ""), CustomerDetails.class);
                                        startService(new Intent(SplashActivity.this, LocationService.class));
                                        if (customerDetails.getCustType() == 2) {
                                            if (Prefs.getCustRole(getApplicationContext()).equals("") || Prefs.getCustRole(getApplicationContext()).equals("default")) {
                                                startActivity(new Intent(getApplicationContext(), AskRoleActivity.class));
                                            } else {
                                                startActivity(new Intent(getApplicationContext(), DriverHomeActivity.class));
                                            }
                                        } else {
                                            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                        }
                                        finish();
                                    } else {
                                        startService(new Intent(SplashActivity.this, LocationService.class));
                                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                        finish();
                                    }
                                }
                            }
                        }, 3000);
                    }
                }
        }
    }

}
