package com.hipool.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.app.BaseActivity;
import com.hipool.app.MyApp;
import com.hipool.askrole.AskRoleActivity;
import com.hipool.customerhome.HomeActivity;
import com.hipool.driverhome.DriverHomeActivity;
import com.hipool.models.CustomerDetails;
import com.hipool.personaldetails.PersonalDetailsActivity;
import com.hipool.service.BookingService;
import com.hipool.utils.Constants;
import com.hipool.utils.Prefs;
import com.hipool.utils.SmsListener;
import com.hipool.utils.SmsReceiver;
import com.hipool.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import eltos.simpledialogfragment.SimpleDialog;
import eltos.simpledialogfragment.form.Input;
import eltos.simpledialogfragment.form.SimpleFormDialog;

import static com.hipool.utils.APIConstants.BASE_URL;
import static com.hipool.utils.APIConstants.CHECK_MOBILE_FORGOT_PASSWORD_URL;
import static com.hipool.utils.APIConstants.CHECK_MOBILE_URL;
import static com.hipool.utils.APIConstants.SET_PASSWORD_FORGOT_PASSWORD_URL;
import static com.hipool.utils.APIConstants.SIGNIN_URL;
import static com.hipool.utils.Constants.IS_DEBUG;

public class LoginActivity extends BaseActivity implements SimpleDialog.OnDialogResultListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private LinearLayout rootMobileValidation, rootSetPassword, rootMobileForgotPassword, rootPasswordSignin;
    private EditText inputMobileValidation, inputPasswordSetPassword, inputConfirmPasswordSetPassword, inputMobileForgotPassword, inputPasswordSignin;
    private TextView btnSigninMobileValidation, btnForgotPassword, btnSigninSetPassword, btnResetForgotPassword, btnPasswordSignin;
    private ImageView btnBack;
    private String otp = "";
    private String mobile = "";
    private String userId = "";
    private SpinKitView spinKitView;
    private RelativeLayout rootOTP;
    private EditText inputOTP;
    private TextView btnSubmitOTP, btnResendOTP;
    private ImageView btnCancelOTP;
    private int otpType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();
        setValidateMobileView();

        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                Log.d("Text", messageText);
                Toast.makeText(LoginActivity.this,"Message: "+messageText,Toast.LENGTH_LONG).show();
                inputOTP.setText(messageText.substring(8, 14));
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btnSigninMobileValidation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mobile = inputMobileValidation.getText().toString();
                if (mobile.length() < 10) {
                    Toast.makeText(getApplicationContext(), "enter valid 10 digit mobile number..", Toast.LENGTH_LONG).show();
                } else {
                    checkMobile(mobile);
                }
            }
        });

        btnSigninSetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!userId.equals("")) {

                    if (inputPasswordSetPassword.getText().toString().length() < 6) {
                        Toast.makeText(getApplicationContext(), "password should be 6 to 15 character..", Toast.LENGTH_LONG).show();
                    } else if (inputConfirmPasswordSetPassword.getText().toString().length() < 6) {
                        Toast.makeText(getApplicationContext(), "password should be 6 to 15 character..", Toast.LENGTH_LONG).show();
                    } else if (!inputPasswordSetPassword.getText().toString().equals(inputConfirmPasswordSetPassword.getText().toString())) {
                        Toast.makeText(getApplicationContext(), "password do not match..", Toast.LENGTH_LONG).show();
                    } else {
                        Prefs.setMobile(getApplicationContext(), mobile);
                        Prefs.setPassword(getApplicationContext(), inputConfirmPasswordSetPassword.getText().toString());
                        Prefs.setUserId(getApplicationContext(), userId);
                        startActivity(new Intent(getApplicationContext(), PersonalDetailsActivity.class));
                        finish();
                    }
                } else {
                    if (inputPasswordSetPassword.getText().toString().length() < 6) {
                        Toast.makeText(getApplicationContext(), "password should be 6 to 15 character..", Toast.LENGTH_LONG).show();
                    } else if (inputConfirmPasswordSetPassword.getText().toString().length() < 6) {
                        Toast.makeText(getApplicationContext(), "password should be 6 to 15 character..", Toast.LENGTH_LONG).show();
                    } else if (!inputPasswordSetPassword.getText().toString().equals(inputConfirmPasswordSetPassword.getText().toString())) {
                        Toast.makeText(getApplicationContext(), "password do not match..", Toast.LENGTH_LONG).show();
                    } else {
                        setPasswordForgotPassword(inputConfirmPasswordSetPassword.getText().toString());
                    }
                }
            }
        });

        btnPasswordSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String password = inputPasswordSignin.getText().toString();
                if (password.length() > 5) {
                    signIn(password);
                } else {
                    Toast.makeText(getApplicationContext(), "enter password", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setForgotPasswordView();
            }
        });

        btnResetForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mobile = inputMobileForgotPassword.getText().toString();
                if (mobile.length() < 10) {
                    Toast.makeText(getApplicationContext(), "enter valid 10 digit mobile number..", Toast.LENGTH_LONG).show();
                } else {
                    checkMobileForgotPassword(mobile);
                }
            }
        });


        btnSubmitOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (inputOTP.getText().toString().length() == 6) {
                    rootOTP.setVisibility(View.GONE);
                    otpType = 0;
                    setSetPasswordView();
                } else {
                    Toast.makeText(getApplicationContext(), "enter valid 6 digit otp..", Toast.LENGTH_LONG).show();
                }
            }
        });

        rootOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btnCancelOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rootOTP.setVisibility(View.GONE);
                otpType = 0;
            }
        });

        btnResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (otpType == 1) {
                    mobile = inputMobileForgotPassword.getText().toString();
                    if (mobile.length() < 10) {
                        Toast.makeText(getApplicationContext(), "enter valid 10 digit mobile number..", Toast.LENGTH_LONG).show();
                    } else {
                        checkMobileForgotPassword(mobile);
                    }
                } else if (otpType == 2) {
                    mobile = inputMobileValidation.getText().toString();
                    if (mobile.length() < 10) {
                        Toast.makeText(getApplicationContext(), "enter valid 10 digit mobile number..", Toast.LENGTH_LONG).show();
                    } else {
                        checkMobile(mobile);
                    }
                }
            }
        });

    }


    private void initViews() {
        rootMobileValidation = (LinearLayout) findViewById(R.id.root_input_mobile_content_login);
        inputMobileValidation = (EditText) findViewById(R.id.input_mobile_content_login);
        btnSigninMobileValidation = (TextView) findViewById(R.id.btn_signin_content_login);
        btnForgotPassword = (TextView) findViewById(R.id.btn_forgot_password_content_login);
        rootSetPassword = (LinearLayout) findViewById(R.id.root_input_passwords_content_login);
        inputPasswordSetPassword = (EditText) findViewById(R.id.input_password_content_login);
        inputConfirmPasswordSetPassword = (EditText) findViewById(R.id.input_password_confirm_content_login);
        btnSigninSetPassword = (TextView) findViewById(R.id.btn_signin_set_password_content_login);
        rootMobileForgotPassword = (LinearLayout) findViewById(R.id.root_input_mobile_forgot_password_content_login);
        inputMobileForgotPassword = (EditText) findViewById(R.id.input_mobile_forgot_password_content_login);
        btnResetForgotPassword = (TextView) findViewById(R.id.btn_reset_forgot_password_content_login);
        btnBack = (ImageView) findViewById(R.id.btn_back_content_login);
        rootPasswordSignin = (LinearLayout) findViewById(R.id.root_password_signin_content_login);
        inputPasswordSignin = (EditText) findViewById(R.id.input_signin_password_content_login);
        btnPasswordSignin = (TextView) findViewById(R.id.btn_signin_password_content_login);
        spinKitView = (SpinKitView) findViewById(R.id.spinner_content_login);
        spinKitView.setVisibility(View.GONE);
        rootOTP = (RelativeLayout) findViewById(R.id.root_otp_login);
        inputOTP = (EditText) findViewById(R.id.input_otp_login);
        btnSubmitOTP = (TextView) findViewById(R.id.btn_submit_otp_login);
        btnCancelOTP = (ImageView) findViewById(R.id.btn_otp_cancel);
        btnResendOTP = (TextView) findViewById(R.id.btn_resend_otp_login) ;




        Utils.addSpaceFilter(inputMobileValidation);
        Utils.addSpaceFilter(inputPasswordSignin);
        Utils.addSpaceFilter(inputMobileForgotPassword);
        Utils.addSpaceFilter(inputPasswordSetPassword);
        Utils.addSpaceFilter(inputConfirmPasswordSetPassword);
        Utils.addSpaceFilter(inputOTP);
        inputMobileValidation.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        inputMobileForgotPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        inputPasswordSetPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
        inputConfirmPasswordSetPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
        inputOTP.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
    }

    private void setValidateMobileView() {
        btnForgotPassword.setVisibility(View.VISIBLE);
        rootMobileValidation.setVisibility(View.VISIBLE);
        rootPasswordSignin.setVisibility(View.GONE);
        rootSetPassword.setVisibility(View.GONE);
        rootMobileForgotPassword.setVisibility(View.GONE);
        btnBack.setVisibility(View.GONE);
        inputMobileValidation.setText("");
    }

    private void setPasswordSigninView() {

        rootMobileValidation.setVisibility(View.GONE);
        rootPasswordSignin.setVisibility(View.VISIBLE);
        rootSetPassword.setVisibility(View.GONE);
        rootMobileForgotPassword.setVisibility(View.GONE);
        btnBack.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(null);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setValidateMobileView();
            }
        });
        inputPasswordSignin.setText("");
    }

    private void setSetPasswordView() {
        btnForgotPassword.setVisibility(View.GONE);
        rootMobileValidation.setVisibility(View.GONE);
        rootPasswordSignin.setVisibility(View.GONE);
        rootSetPassword.setVisibility(View.VISIBLE);
        rootMobileForgotPassword.setVisibility(View.GONE);
        btnBack.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(null);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setValidateMobileView();
            }
        });
        inputPasswordSetPassword.setText("");
        inputConfirmPasswordSetPassword.setText("");
    }

    private void setForgotPasswordView() {
        rootMobileValidation.setVisibility(View.GONE);
        rootPasswordSignin.setVisibility(View.GONE);
        rootSetPassword.setVisibility(View.GONE);
        btnForgotPassword.setVisibility(View.GONE);
        rootMobileForgotPassword.setVisibility(View.VISIBLE);
        btnBack.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(null);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setValidateMobileView();
            }
        });
        inputMobileForgotPassword.setText("");
    }

    private void signIn(final String password) {


        spinKitView.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + SIGNIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                if (IS_DEBUG){
                    Log.v(TAG, "res:" + response);
                }

                if (response.contains("Login successful")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SharedPreferences.Editor editor = getSharedPreferences(Constants.APP_PREFS, MODE_PRIVATE).edit();
                            try {
                                editor.putInt(Constants.CUST_ID, new JSONObject(response).getJSONObject("data").getInt("cust_id")).apply();
                                editor.putString(Constants.CUST_DETAILS, new JSONObject(response).getJSONObject("data").toString()).apply();
                                CustomerDetails customerDetails = new Gson().fromJson(new JSONObject(response).getJSONObject("data").toString(), CustomerDetails.class);
                                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(Constants.CUST_DETAILS, new Gson().toJson(customerDetails)).apply();
                                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putBoolean(Constants.KEY_IS_LOGGED_IN, true).apply();
                                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putInt(Constants.CUST_ID, customerDetails.getCustId()).apply();

                                spinKitView.setVisibility(View.GONE);

                                if (customerDetails.getCustType() == 2 && customerDetails.getCustRideStatus() == 2) {
                                    startActivity(new Intent(getApplicationContext(), DriverHomeActivity.class));
                                } else if (customerDetails.getCustType() == 2 && customerDetails.getCustRideStatus() == 1){
                                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                } else if (customerDetails.getCustType() == 2 && customerDetails.getCustRideStatus() == 0) {
                                    startActivity(new Intent(getApplicationContext(), AskRoleActivity.class));
                                } else {
                                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                }

                                finish();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            spinKitView.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "failed!", Toast.LENGTH_LONG).show();
                        }
                    });
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (IS_DEBUG){
                    Log.e(TAG, "err:" + error.toString());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        spinKitView.setVisibility(View.GONE);
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_contact_no", mobile);
                params.put("password", password);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void checkMobile(final String mobileNo) {

        spinKitView.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + CHECK_MOBILE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (IS_DEBUG){
                    Log.v(TAG, "res:" + response);
                }
                try {
                    JSONObject resObject = new JSONObject(response);
                    if (resObject.getString("message").contains("Password not Exist")){
                        otpType = 2;
                        otp = resObject.getJSONObject("data").getString("otp");
                        userId = resObject.getJSONObject("data").getString("cust_id");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                spinKitView.setVisibility(View.GONE);
                                rootOTP.setVisibility(View.VISIBLE);
                                inputOTP.setText("");
//                                SimpleFormDialog.build()
//                                        .title("OTP")
//                                        .fields(
//                                                Input.plain("otp").required().hint("OTP").max(6).min(6).inputType(InputType.TYPE_CLASS_NUMBER)
//                                        )
//                                        .pos("Ok")
//                                        .neg("Resend")
//                                        .show(LoginActivity.this, "dialog_otp");


                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                spinKitView.setVisibility(View.GONE);
                                setPasswordSigninView();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (IS_DEBUG){
                    Log.e(TAG, "err:" + error.toString());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_contact_no", mobileNo);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void checkMobileForgotPassword(final String mobileNo) {
        showProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + CHECK_MOBILE_FORGOT_PASSWORD_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (IS_DEBUG){
                    Log.v(TAG, "res:" + response);
                }
                try {
                    if (response.contains("Number Exist")){
                        otpType = 1;
                        JSONObject resObject = new JSONObject(response);
                        otp = resObject.getString("data");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();
                                rootOTP.setVisibility(View.VISIBLE);
                                inputOTP.setText("");
//                                SimpleFormDialog.build()
//                                        .title("OTP")
//                                        .fields(
//                                                Input.plain("otp").required().hint("OTP").max(6).min(6).inputType(InputType.TYPE_CLASS_NUMBER)
//                                        )
//                                        .pos("Ok")
//                                        .neg("Resend")
//                                        .show(LoginActivity.this, "dialog_otp_forgot_password");
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();
                                Toast.makeText(getApplicationContext(), "Number don't exist..", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (IS_DEBUG){
                    Log.e(TAG, "err:" + error.toString());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_contact_no", mobileNo);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void setPasswordForgotPassword(final String password) {
        showProgressDialog();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + SET_PASSWORD_FORGOT_PASSWORD_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (IS_DEBUG){
                    Log.v(TAG, "res:" + response);
                }

                if (response.contains("Customer Password Is Old One")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            Toast.makeText(getApplicationContext(), "new password can not be same as old password", Toast.LENGTH_LONG).show();
                        }
                    });
                } else if (response.contains("Customer Password Updated Successfully")){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            Toast.makeText(getApplicationContext(), "success, please login..", Toast.LENGTH_LONG).show();
                            setValidateMobileView();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            Toast.makeText(getApplicationContext(), "failed!", Toast.LENGTH_LONG).show();
                        }
                    });
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (IS_DEBUG){
                    Log.e(TAG, "err:" + error.toString());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_contact_no", mobile);
                params.put("password", password);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onResult(@NonNull String dialogTag, int which, @NonNull Bundle extras) {
        if (dialogTag.equals("dialog_otp")){
            if (which == BUTTON_POSITIVE){
                if (otp.equals(extras.getString("otp"))){
                    setSetPasswordView();
                } else {
                    Toast.makeText(getApplicationContext(), "please enter valid OTP", Toast.LENGTH_LONG).show();
                    SimpleFormDialog.build()
                            .title("OTP")
                            .fields(
                                    Input.plain("otp").required().hint("OTP").max(6).min(6).inputType(InputType.TYPE_CLASS_NUMBER)
                            )
                            .pos("Ok")
                            .neg("Resend")
                            .show(LoginActivity.this, "dialog_otp");
                }
            } else if (which == BUTTON_NEGATIVE){
                checkMobile(mobile);
            } else {
                Toast.makeText(getApplicationContext(), "cancelled!", Toast.LENGTH_LONG).show();
            }
            return true;
        } else if (dialogTag.equals("dialog_otp_forgot_password")){
            if (which == BUTTON_POSITIVE){
                if (otp.equals(extras.getString("otp"))){
                    setSetPasswordView();
                } else {
                    Toast.makeText(getApplicationContext(), "please enter valid OTP", Toast.LENGTH_LONG).show();
                    SimpleFormDialog.build()
                            .title("OTP")
                            .fields(
                                    Input.plain("otp").required().hint("OTP").max(6).min(6).inputType(InputType.TYPE_CLASS_NUMBER)
                            )
                            .pos("Ok")
                            .neg("Resend")
                            .show(LoginActivity.this, "dialog_otp_forgot_password");
                }
            } else if (which == BUTTON_NEGATIVE){
                checkMobileForgotPassword(mobile);
            } else {
                Toast.makeText(getApplicationContext(), "cancelled!", Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return false;
    }



}
