package com.hipool.fragment;


import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.app.BaseFragment;
import com.hipool.app.MyApp;
import com.hipool.collectpayment.CollectPaymentActivity;
import com.hipool.directions.Direction;
import com.hipool.feedback.FeedbackByDriverActivity;
import com.hipool.login.LoginActivity;
import com.hipool.models.CurrentDetail;
import com.hipool.models.CustomerDetails;
import com.hipool.models.CustomerRequest;
import com.hipool.riderrequests.RiderRequestsActivity;
import com.hipool.service.BookingService;
import com.hipool.utils.DisplayUtility;
import com.hipool.utils.HiPoolSocketUtils;
import com.hipool.utils.Prefs;
import com.hipool.utils.Utils;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import eltos.simpledialogfragment.SimpleDialog;
import eltos.simpledialogfragment.form.Input;
import eltos.simpledialogfragment.form.SimpleFormDialog;

import static android.content.Context.MODE_PRIVATE;
import static com.hipool.service.BookingService.SOCKET_DATA;
import static com.hipool.service.BookingService.SOCKET_DATA_RECIEVED_EVENT;
import static com.hipool.service.BookingService.SOCKET_EVENT;
import static com.hipool.utils.APIConstants.ADD_TRIP_DATA;
import static com.hipool.utils.APIConstants.BASE_URL;
import static com.hipool.utils.APIConstants.CHANGE_RIDE_STATUS;
import static com.hipool.utils.APIConstants.DRIVER_GURRENT_STATUS;
import static com.hipool.utils.Constants.APP_PREFS;
import static com.hipool.utils.Constants.CUST_DETAILS;
import static com.hipool.utils.Constants.IS_DEBUG;
import static com.hipool.utils.Constants.STATUS_CUSTOMER_CONFIRMED_RIDE_NOT_STARTED;
import static com.hipool.utils.Constants.STATUS_CUSTOMER_CONFIRMED_RIDE_STARTED;
import static com.hipool.utils.Constants.STATUS_CUSTOMER_DROPPED;
import static com.hipool.utils.Constants.STATUS_CUSTOMER_PICKED;
import static com.hipool.utils.Constants.STATUS_DEFAULT;
import static com.hipool.utils.Constants.STATUS_DRIVER_FEEDBACK_PENDING;
import static com.hipool.utils.Constants.STATUS_DRIVER_RIDE_STARTED;
import static com.hipool.utils.Constants.STATUS_DRIVER_RIDE_STOPPED;
import static com.hipool.utils.Constants.STATUS_DRIVER_ROUTE_CONFIRMED;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        OnMapReadyCallback, Direction.DirectionsRouteCommunicator,
        SimpleDialog.OnDialogResultListener {

    private static final String TAG = HomeFragment.class.getSimpleName();
    private GoogleMap mMap;
    private Marker markerMyLocation;
    private Location mLastKnownLocation;
    private GoogleApiClient mGoogleApiClient;
    private SupportMapFragment mapFragment;
    private Marker markerOrigin, markerDest;
    private static final int REQUEST_CODE_AUTOCOMPLETE_ORIGIN = 112;
    private static final int REQUEST_CODE_AUTOCOMPLETE_DEST = 113;
    private TextView inputDestLocation;
    private List<Polyline> polylineList = new ArrayList<>();
    private TextView viaOne, viaTwo, viaThree;
    private List<Direction.Route> routeListGlobal = new ArrayList<>();
    private RadioGroup radioGroupRouteSelection;
    private TextView distanceText, timeText,tool_bar_title;
    private MaterialSpinner spinnerDepartureTime;
    private int selectedDepartureTime = 0;
    private CustomerDetails customerDetails;
    private JSONArray routeArray = new JSONArray();
    private RelativeLayout btnConfirm, btnNavigate;
    private LinearLayout rootViaConfirmed, rootRouteSelectionBottom;
    private TextView viaConfirmedTop;
    private int routeSelected = 0;
    private LinearLayout rootConfirmedRouteBottom, rootRideStartedPickCustomerBottom;
    private TextView distanceTextConfirmedRoute, timeTextConfirmedRoute;
    private RelativeLayout btnCancelRouteConfirmed, btnStartRouteConfirmed, btnPickCustomer, btnStopRide, btnDrop;
    private FloatingActionButton floatingActionButton;

//    private CircleImageView profilePicConfirmedCustomer;
//    private TextView pickLocationConfirmedCustomer, dropLocationConfirmedCustomer, mobileConfirmedCustomer, nameConfirmedCustomer;
    private Marker markerPickCustomer, markerDropCustomer;
    private int statusCurrent = 0;
    private String otp = "";
    private boolean toApplyStatus = true;
    private RelativeLayout rootCustomerConfirmed;
    private CircleImageView profilePicUserCustomerConfirmed, profilePicCoRiderCustomerConfirmed;
    private TextView nameUserCustomerConfirmed, nameCoRiderCustomerConfirmed;
    private RadioButton radioBtnRouteTwo, radioButtonRouteThree;
    private RelativeLayout colorRouteTwo, colorRouteThree;
    private ImageView imageViewSupportCall;
    private String mobileCustomer = "";
    private RelativeLayout btnCancelCustomer;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
    private RelativeLayout btnSOS;
    private ImageView floatingDestMarker;
    private LatLng latLngCenter;
    private boolean isDestFloating = false;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        customerDetails = new Gson().fromJson(getActivity().getSharedPreferences(APP_PREFS, MODE_PRIVATE).getString(CUST_DETAILS, ""), CustomerDetails.class);

        initViews(view);

        buildGoogleApiClient();

        inputDestLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toApplyStatus = false;
                openAutocompleteActivity("dest");
            }
        });

        radioGroupRouteSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int j) {
                int id = radioGroup.getCheckedRadioButtonId();
                switch (id) {
                    case R.id.radiobutton_route_one_selection_fragment_home:
                        for (Polyline polyline : polylineList) {
                            if (polyline != null) {
                                polyline.remove();
                            }
                        }
                        polylineList.clear();
                        polylineList.add(mMap.addPolyline(routeListGlobal.get(0).getPolylineOptions()));
                        //distanceText.setText("Distance : " + routeListGlobal.get(0).getDistanceText());
                        distanceText.setText(""+routeListGlobal.get(0).getDistanceText()+"("+routeListGlobal.get(0).getTimeText()+")");
                       // timeText.setText("Time : " + routeListGlobal.get(0).getTimeText());

                        routeArray = new JSONArray();
                        for (int i = 0; i < routeListGlobal.get(0).getPolylineOptions().getPoints().size(); i++) {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("lat", routeListGlobal.get(0).getPolylineOptions().getPoints().get(i).latitude);
                                jsonObject.put("lon", routeListGlobal.get(0).getPolylineOptions().getPoints().get(i).longitude);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            routeArray.put(jsonObject);
                        }
                        routeSelected = 0;
                        break;

                    case R.id.radiobutton_route_two_selection_fragment_home:
                        if (routeListGlobal.size() > 1) {

                            for (Polyline polyline : polylineList) {
                                if (polyline != null) {
                                    polyline.remove();
                                }
                            }
                            polylineList.clear();
                            polylineList.add(mMap.addPolyline(routeListGlobal.get(1).getPolylineOptions()));
                            distanceText.setText(""+routeListGlobal.get(1).getDistanceText()+"("+routeListGlobal.get(1).getTimeText()+")");
                           // timeText.setText("Time : " + );

                            routeArray = new JSONArray();
                            for (int i = 0; i < routeListGlobal.get(1).getPolylineOptions().getPoints().size(); i++) {
                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("lat", routeListGlobal.get(1).getPolylineOptions().getPoints().get(i).latitude);
                                    jsonObject.put("lon", routeListGlobal.get(1).getPolylineOptions().getPoints().get(i).longitude);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                routeArray.put(jsonObject);
                            }
                            routeSelected = 1;
                        }
                        break;

                    case R.id.radiobutton_route_three_selection_fragment_home:
                        if (routeListGlobal.size() > 2) {

                            for (Polyline polyline : polylineList) {
                                if (polyline != null) {
                                    polyline.remove();
                                }
                            }
                            polylineList.clear();
                            polylineList.add(mMap.addPolyline(routeListGlobal.get(2).getPolylineOptions()));
                          //  distanceText.setText("Distance : " + routeListGlobal.get(2).getDistanceText());
                            distanceText.setText(""+routeListGlobal.get(2).getDistanceText()+"("+routeListGlobal.get(2).getTimeText()+")");
                           // timeText.setText("Time : " + routeListGlobal.get(2).getTimeText());

                            routeArray = new JSONArray();
                            for (int i = 0; i < routeListGlobal.get(2).getPolylineOptions().getPoints().size(); i++) {
                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("lat", routeListGlobal.get(2).getPolylineOptions().getPoints().get(i).latitude);
                                    jsonObject.put("lon", routeListGlobal.get(2).getPolylineOptions().getPoints().get(i).longitude);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                routeArray.put(jsonObject);
                            }
                            routeSelected = 2;
                        }
                        break;

                }
            }
        });

        spinnerDepartureTime.setItems("Now", "5 min", "10 min", "15 min", "20 min", "25 min", "30 min");
        spinnerDepartureTime.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                switch (position) {
                    case 0:
                        selectedDepartureTime = 0;
                        break;

                    case 1:
                        selectedDepartureTime = 5;
                        break;

                    case 2:
                        selectedDepartureTime = 10;
                        break;

                    case 3:
                        selectedDepartureTime = 15;
                        break;

                    case 4:
                        selectedDepartureTime = 20;
                        break;

                    case 5:
                        selectedDepartureTime = 25;
                        break;

                    case 6:
                        selectedDepartureTime = 30;
                        break;

                    default:
                        break;
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmRoute(markerOrigin.getPosition(), markerDest.getPosition());
            }
        });

        return view;
    }

    private void initViews(View view) {
        tool_bar_title=(TextView)getActivity().findViewById(R.id.toolbar_title);
        inputDestLocation = (TextView) view.findViewById(R.id.drop_textview);
        inputDestLocation.setText("enter destination");
        viaOne = (TextView) view.findViewById(R.id.via_route_one_fragment_home);
        viaTwo = (TextView) view.findViewById(R.id.via_route_two_fragment_home);
        viaThree = (TextView) view.findViewById(R.id.via_route_three_fragment_home);
        radioGroupRouteSelection = (RadioGroup) view.findViewById(R.id.radiogroup_route_selection_fragment_home);
        distanceText = (TextView) view.findViewById(R.id.distance_text_fragment_home);
        timeText = (TextView) view.findViewById(R.id.time_text_fragment_home);
        spinnerDepartureTime = (MaterialSpinner) view.findViewById(R.id.spinner_departure_time_fragment_home);
        btnConfirm = (RelativeLayout) view.findViewById(R.id.confirm_button_fragment_home);
        rootViaConfirmed = (LinearLayout) view.findViewById(R.id.root_via_confirmed_route_fragment_home);
        rootRouteSelectionBottom = (LinearLayout) view.findViewById(R.id.root_route_selection_bottom_fragment_home);
        viaConfirmedTop = (TextView) view.findViewById(R.id.via_confirmed_route_fragment_home);
        rootConfirmedRouteBottom = (LinearLayout) view.findViewById(R.id.root_confirmed_route_bottom_fragment_home);
        distanceTextConfirmedRoute = (TextView) view.findViewById(R.id.distance_text_route_confirmed_fragment_home);
        timeTextConfirmedRoute = (TextView) view.findViewById(R.id.time_text_route_confirmed_fragment_home);
        btnCancelRouteConfirmed = (RelativeLayout) view.findViewById(R.id.cancel_button_route_confirmed_fragment_home);
        btnCancelRouteConfirmed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeRideStatus(Prefs.getDriverRequestId(getContext()), "0");
            }
        });
        btnStartRouteConfirmed = (RelativeLayout) view.findViewById(R.id.start_button_route_confirmed_fragment_home);
        btnStartRouteConfirmed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeRideStatus(Prefs.getDriverRequestId(getContext()), "1");
            }
        });
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab_activity_home);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), RiderRequestsActivity.class));
            }
        });
        floatingActionButton.hide();
//        rootConfirmedCustomer = (RelativeLayout) view.findViewById(R.id.root_confirmed_customer_fragment_home);
//        profilePicConfirmedCustomer = (CircleImageView) view.findViewById(R.id.profile_pic_confirmed_customer_fragment_home);
//        nameConfirmedCustomer = (TextView) view.findViewById(R.id.name_confirmed_customer_fragment_home);
//        pickLocationConfirmedCustomer = (TextView) view.findViewById(R.id.pick_location_confirmed_customer_fragment_home);
//        dropLocationConfirmedCustomer = (TextView) view.findViewById(R.id.drop_location_confirmed_customer_fragment_home);
//        mobileConfirmedCustomer = (TextView) view.findViewById(R.id.mobile_confirmed_customer_fragment_home);
        rootRideStartedPickCustomerBottom = (LinearLayout) view.findViewById(R.id.root_ride_started_pick_customer_bottom_fragment_home);
        btnPickCustomer = (RelativeLayout) view.findViewById(R.id.pick_button_ride_started_pick_customer_fragment_home);
        btnPickCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpleFormDialog.build()
                        .title("OTP")
                        .fields(
                                Input.plain("otp").required().hint("OTP").max(4).min(4).inputType(InputType.TYPE_CLASS_NUMBER)
                        )
                        .pos("Ok")
                        .neg("Cancel")
                        .show(HomeFragment.this, "dialog_otp");
            }
        });
        btnStopRide = (RelativeLayout) view.findViewById(R.id.stop_button_ride_started_fragment_home);
        btnStopRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeRideStatus(Prefs.getDriverRequestId(getContext()), "2");
            }
        });
        btnDrop = (RelativeLayout) view.findViewById(R.id.drop_button_ride_started_fragment_home);
        btnDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BookingService.getInstance().changeRideStatus("3");
            }
        });
        btnNavigate = (RelativeLayout) view.findViewById(R.id.navigate_btn_fragment_home);
        btnNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (statusCurrent == STATUS_CUSTOMER_CONFIRMED_RIDE_NOT_STARTED || statusCurrent == STATUS_CUSTOMER_CONFIRMED_RIDE_STARTED) {
                    if (markerPickCustomer != null) {
                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + markerPickCustomer.getPosition().latitude + "," + markerPickCustomer.getPosition().longitude);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivity(mapIntent);
                        } else {
                            Toast.makeText(getContext(), "Navigation not possible on this device!", Toast.LENGTH_LONG).show();
                        }
                    }
                } else if (statusCurrent == STATUS_CUSTOMER_PICKED) {
                    if (markerDropCustomer != null) {
                        String uri = "google.navigation:q=" + markerDropCustomer.getPosition().latitude + "," + markerDropCustomer.getPosition().longitude;
//                        Log.v(TAG, "uri: " + uri);
                        Uri gmmIntentUri = Uri.parse(uri);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivity(mapIntent);
                        } else {
                            Toast.makeText(getContext(), "Navigation not possible on this device!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });
        rootCustomerConfirmed = (RelativeLayout) view.findViewById(R.id.root_customer_confirmed_fragment_home);
        profilePicUserCustomerConfirmed = (CircleImageView) view.findViewById(R.id.profile_pic_user_customer_confirmed_fragment_home);
        nameUserCustomerConfirmed = (TextView) view.findViewById(R.id.name_user_customer_confirmed_fragment_home);
        profilePicCoRiderCustomerConfirmed = (CircleImageView) view.findViewById(R.id.profile_pic_corider_customer_confirmed_home_fragment);
        nameCoRiderCustomerConfirmed = (TextView) view.findViewById(R.id.name_corider_customer_confirmed_home_fragment);


        radioBtnRouteTwo = (RadioButton) view.findViewById(R.id.radiobutton_route_two_selection_fragment_home);
        radioButtonRouteThree = (RadioButton) view.findViewById(R.id.radiobutton_route_three_selection_fragment_home);

        colorRouteTwo = (RelativeLayout) view.findViewById(R.id.route_color_two_fragment_home);
        colorRouteThree = (RelativeLayout) view.findViewById(R.id.route_color_three_fragment_home);

        imageViewSupportCall = (ImageView) view.findViewById(R.id.image_support_call_fragment_home);
        imageViewSupportCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (statusCurrent == STATUS_CUSTOMER_CONFIRMED_RIDE_NOT_STARTED || statusCurrent == STATUS_CUSTOMER_CONFIRMED_RIDE_STARTED || statusCurrent == STATUS_CUSTOMER_PICKED) {
                    new MaterialDialog.Builder(getContext())
                            .content("Are you sure you want to call the customer!")
                            .positiveText("Confirm")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    placeCall(mobileCustomer);
                                    dialog.dismiss();
                                }
                            })
                            .negativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        });

        btnCancelCustomer = (RelativeLayout) view.findViewById(R.id.btn_cancel_fragment_home);
        btnCancelCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (statusCurrent == STATUS_CUSTOMER_CONFIRMED_RIDE_NOT_STARTED || statusCurrent == STATUS_CUSTOMER_CONFIRMED_RIDE_STARTED) {
                        if (!Prefs.getDriverReachedAtPickLocationTime(getContext()).equals("")) {
                            if (Utils.diffTime(dateFormat.parse(Prefs.getDriverReachedAtPickLocationTime(getContext())), Calendar.getInstance().getTime()) > 2) {
                                new MaterialDialog.Builder(getContext())
                                        .content("Are you sure you want to cancel the customer!")
                                        .positiveText("Confirm")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                BookingService.getInstance().changeRideStatus("4");
                                                dialog.dismiss();
                                            }
                                        })
                                        .negativeText("Cancel")
                                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .show();
                            } else {
                                Toast.makeText(getContext(), "Customer can only be cancelled after 2 minutes of reaching customer pick location!", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });


        btnSOS = (RelativeLayout) view.findViewById(R.id.sos_btn_fragment_home);
        btnSOS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(getContext())
                        .content("This will trigger a call to Police control room, Please Confirm..")
                        .positiveText("Confirm")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                BookingService.getInstance().sos("0");
                                Toast.makeText(getContext(), "sos started..", Toast.LENGTH_LONG).show();
                            }
                        })
                        .negativeText("Cancel")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        floatingDestMarker = (ImageView) view.findViewById(R.id.floating_dest_fragment_home);
        floatingDestMarker.setVisibility(View.GONE);

    }

    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(), this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (mLastKnownLocation == null) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mLastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "Play services connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // Refer to the reference doc for ConnectionResult to see what error codes might
        // be returned in onConnectionFailed.
        Log.e(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setMyLocationMarker(Prefs.getMyLat(getContext()), Prefs.getMyLon(getContext()));
        setOriginLocation(Prefs.getMyLat(getContext()), Prefs.getMyLon(getContext()));

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                try {
                    if (statusCurrent == STATUS_DEFAULT) {
                        latLngCenter = mMap.getCameraPosition().target;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                try {
                    if (isDestFloating) {
                        isDestFloating = false;
                        floatingDestMarker.setVisibility(View.GONE);
                        setDestLocation(latLngCenter.latitude + ", " + latLngCenter.longitude, latLngCenter.latitude, latLngCenter.longitude);
                        new Direction(HomeFragment.this, markerOrigin.getPosition(), markerDest.getPosition(), "Driver").execute();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                try {
                    if (statusCurrent == STATUS_DEFAULT) {
                        if (marker.getTag().equals(2)) {
                            isDestFloating = true;
                            floatingDestMarker.setVisibility(View.VISIBLE);
                            return true;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });
    }

    private void setMyLocationMarker(double lat, double lon) {
        if (markerMyLocation != null) {
            markerMyLocation.remove();
        }
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_my_location);
        Bitmap smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 12), (int) DisplayUtility.dp2px(getContext(), 15), false);
        markerMyLocation = mMap.addMarker(new MarkerOptions().position(new LatLng(lat,
                lon)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

        if (markerOrigin == null && markerDest == null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    markerMyLocation.getPosition(), 15));
        }
    }

    private void setOriginLocation(double lat, double lon) {
//        originInput.setText(place);
        if (markerOrigin != null) {
            markerOrigin.remove();
        }

        Bitmap bm = Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_origin_location_96dp);
        Bitmap smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 40), (int) DisplayUtility.dp2px(getContext(), 40), false);
        markerOrigin = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

        if (markerDest == null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    markerOrigin.getPosition(), 15));
        } else {
            updateCamera();
        }
    }

    private void setDestLocation(String place, double lat, double lon) {
        if (markerDest != null) {
            markerDest.remove();
        }

        inputDestLocation.setText(place);
        Bitmap bm = Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_dest_location_96dp);
        Bitmap smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 40), (int) DisplayUtility.dp2px(getContext(), 40), false);
        markerDest = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
        markerDest.setTag(2);

        if (markerOrigin == null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    markerDest.getPosition(), 15));
        } else {
            updateCamera();
        }

    }

    private void updateCamera() {
        //Calculate the markers to get their position
        List<Marker> markers = new ArrayList<>();
        LatLngBounds.Builder b = new LatLngBounds.Builder();
        if (markerOrigin != null) {
            markers.add(markerOrigin);
        }

        if (markerDest != null) {
            markers.add(markerDest);
        }
        for (Marker m : markers) {
            b.include(m.getPosition());
        }
        LatLngBounds bounds = b.build();
        //Change the padding as per needed
        if (markers.size() > 1) {
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 150);
            mMap.animateCamera(cu);
        } else {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    markerOrigin.getPosition(), 15));
        }

//        getTravelDistanceTime();
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
                socketDataReciever, new IntentFilter(SOCKET_DATA_RECIEVED_EVENT));

        String prefString = Prefs.getCustomerRequests(getContext());
        try {
            JSONArray requestArray = new JSONArray(prefString);
            if (requestArray.length() > 0) {
                floatingActionButton.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        getStatus();
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(
                socketDataReciever);
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

        if (BookingService.getInstance() == null) {
            getActivity().startService(new Intent(getContext(), BookingService.class));
        }
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    private void openAutocompleteActivity(String which) {
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .setCountry("IN")
                    .build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(typeFilter)
                    .build(getActivity());
            if (which.contains("origin")) {
                startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE_ORIGIN);
            } else if (which.contains("dest")) {
                startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE_DEST);
            }
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Log.e(TAG, message);
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutesReady(List<Direction.Route> routeList) {
        routeListGlobal = routeList;
        for (Polyline polyline : polylineList) {
            if (polyline != null) {
                polyline.remove();
            }
        }
        polylineList.clear();

        routeSelected = 0;

        polylineList.add(mMap.addPolyline(routeList.get(0).getPolylineOptions()));

        rootRouteSelectionBottom.setVisibility(View.VISIBLE);

        showSecondRouteUI();
        showThirdRouteUI();

        try {
            if (routeList.size() == 1) {
                hideSecondRouteUI();
                hideThirdRouteUI();
            } else if (routeList.size() == 2) {
                hideThirdRouteUI();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            viaOne.setText(routeList.get(0).getVia());

            viaTwo.setText(routeList.get(1).getVia());

            viaThree.setText(routeList.get(2).getVia());
        } catch (Exception e) {
            e.printStackTrace();
        }


        //distanceText.setText("Distance : " + routeListGlobal.get(0).getDistanceText());
        timeText.setText("");
        distanceText.setText(""+routeListGlobal.get(0).getDistanceText()+"("+routeListGlobal.get(0).getTimeText()+")");
        Toast.makeText(getContext(),"522 Registered user on this route",Toast.LENGTH_SHORT).show();

        radioGroupRouteSelection.check(R.id.radiobutton_route_one_selection_fragment_home);

        routeArray = new JSONArray();
        for (int i = 0; i < routeListGlobal.get(0).getPolylineOptions().getPoints().size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("lat", routeListGlobal.get(0).getPolylineOptions().getPoints().get(i).latitude);
                jsonObject.put("lon", routeListGlobal.get(0).getPolylineOptions().getPoints().get(i).longitude);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            routeArray.put(jsonObject);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE_ORIGIN) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
//                setOriginLocation(place.getAddress().toString().replace(System.getProperty("line.separator"), ""), place.getLatLng().latitude, place.getLatLng().longitude);
                Log.i(TAG, "Place Selected: " + place.getAddress());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                Log.e(TAG, "Error: Status = " + status.toString());
            } else if (resultCode == AppCompatActivity.RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
            }
        } else if (requestCode == REQUEST_CODE_AUTOCOMPLETE_DEST) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                Log.i(TAG, "Place Selected: " + place.getAddress());
                setDestLocation(place.getAddress().toString().replace(System.getProperty("line.separator"), ""), place.getLatLng().latitude, place.getLatLng().longitude);
//                ((AddTripActivity) getActivity()).showFab();
                new Direction(HomeFragment.this, markerOrigin.getPosition(), markerDest.getPosition(), "Driver").execute();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                Log.e(TAG, "Error: Status = " + status.toString());
            } else if (resultCode == AppCompatActivity.RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
            }
        }
    }


    private void confirmRoute(final LatLng origin, final LatLng dest) {

        showProgressDialog();

        Prefs.setDriverRequestId(getContext(), "");
        Prefs.setCustomerRequests(getContext(), new JSONArray());
        Prefs.setLiveBookingId(getContext(), "");
        floatingActionButton.hide();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + ADD_TRIP_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.v(TAG, "confirmRoute:" + response);
                if (response.toLowerCase().contains("success")) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            rootRouteSelectionBottom.setVisibility(View.GONE);
                            rootViaConfirmed.setVisibility(View.VISIBLE);
                            viaConfirmedTop.setText(routeListGlobal.get(routeSelected).getVia());
                            rootConfirmedRouteBottom.setVisibility(View.VISIBLE);
                            distanceTextConfirmedRoute.setText("Distance : " + routeListGlobal.get(routeSelected).getDistanceText());
                            timeTextConfirmedRoute.setText("Time : " + routeListGlobal.get(routeSelected).getTimeText());
                            tool_bar_title.setText("Your Ride");
                            try {
                                Prefs.setDriverRequestId(getContext(), new JSONObject(response).getJSONObject("data").getString("driver_request_id"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Prefs.setCustRole(getContext(), "driver");
                        }
                    });
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            Toast.makeText(getContext(), "failed!", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "confirmRoute:" + error.toString());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Toast.makeText(getContext(), "failed!", Toast.LENGTH_LONG).show();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                final Map<String, String> params = new HashMap<>();
                params.put("cust_id", String.valueOf(customerDetails.getCustId()));
                params.put("current_lat", String.valueOf(Prefs.getMyLat(getContext())));
                params.put("current_lon", String.valueOf(Prefs.getMyLon(getContext())));
                params.put("pickup_lat", String.valueOf(origin.latitude));
                params.put("pickup_lon", String.valueOf(origin.longitude));
                params.put("drop_lat", String.valueOf(dest.latitude));
                params.put("drop_lon", String.valueOf(dest.longitude));
                params.put("route", routeArray.toString());
                params.put("time", String.valueOf(selectedDepartureTime));
                params.put("via", routeListGlobal.get(routeSelected).getVia());
                params.put("distance_total", String.valueOf(routeListGlobal.get(routeSelected).getDistance()));
                params.put("time_total", String.valueOf(routeListGlobal.get(routeSelected).getTime()));
                params.put("dest_place", inputDestLocation.getText().toString());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);

    }


    private BroadcastReceiver socketDataReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                String eventName = intent.getExtras().getString(SOCKET_EVENT);
                if (eventName != null) {
                    String data = intent.getExtras().getString(SOCKET_DATA);

                        Log.v("recieved_event_data", data);
                        Log.v("recieved_event_name", eventName);

                        if (eventName.equals(HiPoolSocketUtils.EVENT_DRIVER_NOTIFICATION)) {
                            floatingActionButton.show();
                        } else if (eventName.equals(HiPoolSocketUtils.EVENT_CHANGE_RIDE_STATUS)) {
                            if (data.contains("Ride Start")) {
                                tool_bar_title.setText("On Trip");
                                statusCurrent = STATUS_CUSTOMER_PICKED;
                                rootRideStartedPickCustomerBottom.setVisibility(View.GONE);
                                btnDrop.setVisibility(View.VISIBLE);
                                Prefs.setCustomerRequests(getContext(), new JSONArray());
                            } else if (data.contains("Ride Stop")){
                                statusCurrent = STATUS_CUSTOMER_DROPPED;
                                btnDrop.setVisibility(View.GONE);
                                rootCustomerConfirmed.setVisibility(View.GONE);
                                btnStopRide.setVisibility(View.VISIBLE);

                                if (markerPickCustomer != null) {
                                    markerPickCustomer.remove();
                                }

                                markerPickCustomer = null;

                                if (markerDropCustomer != null) {
                                    markerDropCustomer.remove();
                                }

                                markerDropCustomer = null;

                                Prefs.setCustomerRequests(getContext(), new JSONArray());

                                if (!CollectPaymentActivity.isActivityRunning) {
                                    CollectPaymentActivity.isActivityRunning = true;
                                    Intent intent1=new Intent(getContext(), CollectPaymentActivity.class);
                                    intent1.putExtra("booking_id",Prefs.getLiveBookingId(getContext()));
                                    startActivity(intent1);
                                }
                            }
                        }

                }
            }
        }
    };


    private void changeRideStatus(final String driverRequestId, final String rideStatus) {
        showProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + CHANGE_RIDE_STATUS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v(TAG, "changeRideStatus:" + response);
                if (!response.contains("Fail")) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();

                            if (rideStatus.equals("2")) {
                                Prefs.setCustomerRequests(getContext(), new JSONArray());
                                setDefaultUI();
                                Prefs.setCustRole(getContext(), "default");
                            } else if (rideStatus.equals("1")){
                                rootConfirmedRouteBottom.setVisibility(View.GONE);
                                if (!Prefs.getLiveBookingId(getContext()).equals("")) {
                                    rootRideStartedPickCustomerBottom.setVisibility(View.VISIBLE);
                                } else {
                                    btnStopRide.setVisibility(View.VISIBLE);
                                }
                                statusCurrent = STATUS_DRIVER_RIDE_STARTED;
                                if (markerPickCustomer != null) {
                                    statusCurrent = STATUS_CUSTOMER_CONFIRMED_RIDE_STARTED;
                                }
                            } else if (rideStatus.equals("0")) {
                                Prefs.setCustomerRequests(getContext(), new JSONArray());
                                setDefaultUI();
                                Prefs.setCustRole(getContext(), "default");
                            }
                        }
                    });
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            Toast.makeText(getContext(), "failed!", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "changeRideStatus:" + error.toString());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("driver_request_id", driverRequestId);
                params.put("ride_status", rideStatus);
                Log.v(TAG, "changeRideStatus:" + params.toString());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void resetMap() {
        for (Polyline polyline : polylineList) {
            if (polyline != null) {
                polyline.remove();
            }
        }
        polylineList.clear();

        if (markerDest != null) {
            markerDest.remove();
        }

        markerDest = null;

        if (markerPickCustomer != null) {
            markerPickCustomer.remove();
        }

        markerPickCustomer = null;

        if (markerDropCustomer != null) {
            markerDropCustomer.remove();
        }

        markerDropCustomer = null;

        updateCamera();
    }

    private void getStatus() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + DRIVER_GURRENT_STATUS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v(TAG, "getStatus" + response);
                if (response.contains("Success")){
                    try {
                        CurrentDetail currentDetail = new Gson().fromJson(new JSONObject(response).getString("data"), CurrentDetail.class);
                        Log.v(TAG, "getStatus:" +
                        currentDetail.getBookingStatus() + ":" +
                        currentDetail.getRideStatus() + ":" +
                        currentDetail.getStatus());
                        int booking_status=new JSONObject(response).getInt("booking_status");


                        if (toApplyStatus) {
                            if((currentDetail.getRideStatus()==1 ||currentDetail.getRideStatus()==0)&& currentDetail.getStatus()==0)
                            {
                                if(booking_status==5)
                                {
                                    statusCurrent = STATUS_DRIVER_FEEDBACK_PENDING;
                                    setFeedbackUI(currentDetail);

                                    Prefs.setCustomerPickLat(getContext(), "");
                                    Prefs.setCustomerPickLon(getContext(), "");
                                    Prefs.setDriverReachedAtPickLocationTime(getContext(), "");

                                }
                                else if(currentDetail.getRideStatus()==0 )
                                {
                                    statusCurrent = STATUS_CUSTOMER_CONFIRMED_RIDE_NOT_STARTED;
                                    setCustomerConfirmedRideNotStartedUI(currentDetail);
                                    Prefs.setCustomerRequests(getContext(), new JSONArray());

                                    Prefs.setCustRole(getContext(), "driver");
                                }
                                else if(currentDetail.getBookingStatus()==1)
                                {
                                    statusCurrent = STATUS_CUSTOMER_CONFIRMED_RIDE_STARTED;
                                    setCustomerConfirmedRideStartedUI(currentDetail);

                                    Prefs.setCustomerRequests(getContext(), new JSONArray());

                                    Prefs.setCustRole(getContext(), "driver");
                                }
                                else if(currentDetail.getBookingStatus()==2)
                                {
                                    statusCurrent = STATUS_CUSTOMER_PICKED;
                                    setCustomerPickedUI(currentDetail);


                                    Prefs.setCustomerRequests(getContext(), new JSONArray());

                                    Prefs.setCustomerPickLat(getContext(), "");
                                    Prefs.setCustomerPickLon(getContext(), "");
                                    Prefs.setDriverReachedAtPickLocationTime(getContext(), "");

                                    Prefs.setCustRole(getContext(), "driver");
                                }
                                else if (currentDetail.getBookingStatus()==3)
                                {
                                    statusCurrent = STATUS_CUSTOMER_DROPPED;
                                    setCustomerDroppedUI(currentDetail);


                                    Prefs.setCustomerRequests(getContext(), new JSONArray());

                                    Prefs.setCustomerPickLat(getContext(), "");
                                    Prefs.setCustomerPickLon(getContext(), "");
                                    Prefs.setDriverReachedAtPickLocationTime(getContext(), "");


                                    Prefs.setCustRole(getContext(), "driver");
                                }

                            }
                            else if (currentDetail.getStatus()==1 && currentDetail.getRideStatus()==0)
                            {
                                tool_bar_title.setText("Your Ride");
                                statusCurrent = STATUS_DRIVER_ROUTE_CONFIRMED;
                                setRouteSelectedUI(currentDetail);

                                Prefs.setCustomerPickLat(getContext(), "");
                                Prefs.setCustomerPickLon(getContext(), "");
                                Prefs.setDriverReachedAtPickLocationTime(getContext(), "");


                                Prefs.setCustRole(getContext(), "driver");
                            }
                            else if (currentDetail.getStatus()==1 && currentDetail.getRideStatus()==1)
                            {
                                tool_bar_title.setText("Your Ride");
                                statusCurrent = STATUS_DRIVER_RIDE_STARTED;
                                setRideStartedUI(currentDetail);
                                Prefs.setCustomerPickLat(getContext(), "");
                                Prefs.setCustomerPickLon(getContext(), "");
                                Prefs.setDriverReachedAtPickLocationTime(getContext(), "");


                                Prefs.setCustRole(getContext(), "driver");
                            }
                            else if(currentDetail.getStatus()==0 && currentDetail.getRideStatus()==2) {

                                Prefs.setCustRole(getContext(), "default");
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (markerPickCustomer != null) {
                                            markerPickCustomer.remove();
                                        }

                                        markerPickCustomer = null;

                                        if (markerDropCustomer != null) {
                                            markerDropCustomer.remove();
                                        }

                                        markerDropCustomer = null;

                                        floatingActionButton.hide();

                                        Prefs.setCustomerRequests(getContext(), new JSONArray());
                                    }
                                });
                            }

                        }



                        toApplyStatus = true;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.contains("Customer Current Details Not Coming")) {
                    if(toApplyStatus)
                    {
                        statusCurrent = STATUS_DEFAULT;
                        setDefaultUI();

                        Prefs.setCustRole(getContext(), "default");
                    }
                    toApplyStatus=true;

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "getStatus" + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id", String.valueOf(customerDetails.getCustId()));
                params.put("customer_access_token", customerDetails.getCustomerAccessToken());
                Log.v(TAG, "params" + params.toString());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }


    private void setDefaultUI() {
        if (mMap != null) {
            tool_bar_title.setText("Select Your Route");
            rootViaConfirmed.setVisibility(View.GONE);
            rootConfirmedRouteBottom.setVisibility(View.GONE);
            btnStopRide.setVisibility(View.GONE);
            rootRouteSelectionBottom.setVisibility(View.GONE);
            inputDestLocation.setText("enter destination");
            btnNavigate.setVisibility(View.GONE);
            btnSOS.setVisibility(View.GONE);
            Prefs.setCustomerPickLat(getContext(), "");
            Prefs.setCustomerPickLon(getContext(), "");
            Prefs.setDriverReachedAtPickLocationTime(getContext(), "");
            resetMap();
        }
    }

    private void setRouteSelectedUI(CurrentDetail currentDetail) {
        if (mMap != null) {
            imageViewSupportCall.setImageResource(R.drawable.ic_support);
            rootRouteSelectionBottom.setVisibility(View.GONE);
            rootViaConfirmed.setVisibility(View.VISIBLE);
            viaConfirmedTop.setText(currentDetail.getVia());
            rootConfirmedRouteBottom.setVisibility(View.VISIBLE);
            btnNavigate.setVisibility(View.GONE);
            btnSOS.setVisibility(View.GONE);
            distanceTextConfirmedRoute.setText("Distance : " + Utils.round(currentDetail.getDistanceDriverRide() / 1000d, 1) + " Kms");
            timeTextConfirmedRoute.setText("Time : " + currentDetail.getTimeDriverRide());

            Prefs.setDriverRequestId(getContext(), String.valueOf(currentDetail.getId()));

            if (markerOrigin != null) {
                markerOrigin.remove();
            }

            Bitmap bm = Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_origin_location_96dp);
            Bitmap smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 40), (int) DisplayUtility.dp2px(getContext(), 40), false);
            markerOrigin = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(currentDetail.getPickLat()), Double.valueOf(currentDetail.getPickLon()))).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
            if (markerDest != null) {
                markerDest.remove();
            }

            inputDestLocation.setText(currentDetail.getPlaceDest());
            bm = Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_dest_location_96dp);
            smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 40), (int) DisplayUtility.dp2px(getContext(), 40), false);
            markerDest = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(currentDetail.getDropLat()), Double.valueOf(currentDetail.getDropLon()))).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
            markerDest.setTag(2);

            for (Polyline polyline : polylineList) {
                if (polyline != null) {
                    polyline.remove();
                }
            }

            polylineList.clear();

            PolylineOptions polylineOptions = new PolylineOptions();
            try {
                JSONArray routeArray = new JSONArray(currentDetail.getFullRoute());
                for (int  i = 0 ; i < routeArray.length() ; i++ ) {
                    JSONObject latLngObject = routeArray.getJSONObject(i);
                    polylineOptions.add(new LatLng(latLngObject.getDouble("lat"), latLngObject.getDouble("lon")));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            polylineOptions.color(Color.BLUE);
            polylineList.add(mMap.addPolyline(polylineOptions));


            PolylineOptions polylineOptions1 = new PolylineOptions();
            List<LatLng> latLngList = new ArrayList<>();
            latLngList.add(new LatLng(currentDetail.getActualPickLat(), currentDetail.getActualPickLon()));
            latLngList.add(new LatLng(currentDetail.getCustEnterPickLan(), currentDetail.getCustEnterPickLon()));
            polylineOptions1.addAll(latLngList);
            polylineOptions1.color(Color.GREEN);
            Polyline polyline = mMap.addPolyline(polylineOptions1);
            polyline.setPattern(Arrays.<PatternItem>asList(new Dot(), new Gap(10)));
            polylineList.add(polyline);

            PolylineOptions polylineOptions2 = new PolylineOptions();
            latLngList.clear();
            latLngList.add(new LatLng(currentDetail.getActualDropLat(), currentDetail.getActualDropLon()));
            latLngList.add(new LatLng(currentDetail.getCustEnterDropLan(), currentDetail.getCustEnterDropLon()));
            polylineOptions2.addAll(latLngList);
            polylineOptions2.color(Color.RED);
            Polyline polyline2 = mMap.addPolyline(polylineOptions2);
            polyline2.setPattern(Arrays.<PatternItem>asList(new Dot(), new Gap(10)));
            polylineList.add(polyline2);


//
//            Polyline polyline = mMap.addPolyline(polylineOptions);
//
//            List<PatternItem> pattern = Arrays.<PatternItem>asList(new Dot(), new Gap(10));
//            polyline.setPattern(pattern);
//
//            polylineList.add(polyline);

        }
    }

    private void setRideStartedUI(CurrentDetail currentDetail) {
        if (mMap != null) {
            setRouteSelectedUI(currentDetail);
            rootConfirmedRouteBottom.setVisibility(View.GONE);
            btnStopRide.setVisibility(View.VISIBLE);
        }
    }

    private void setRideStoppedUI() {
        if (mMap != null) {
            setDefaultUI();
        }
    }

    private void setCustomerConfirmedRideNotStartedUI(CurrentDetail currentDetail) {
        if (mMap != null) {
            setRouteSelectedUI(currentDetail);
//            rootConfirmedCustomer.setVisibility(View.GONE);
//            floatingActionButton.hide();
//            Prefs.setLiveBookingId(getContext(), String.valueOf(currentDetail.getBookingId()));
//            Picasso.with(getContext()).load(currentDetail.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(profilePicConfirmedCustomer);
//            nameConfirmedCustomer.setText(currentDetail.getCustFirstName() + "" + currentDetail.getCustLastName());
//            mobileConfirmedCustomer.setText(currentDetail.getCustContactNo());
//            pickLocationConfirmedCustomer.setText(currentDetail.getCustPickAddress());
//            dropLocationConfirmedCustomer.setText(currentDetail.getCustDropAddress());

            mobileCustomer = currentDetail.getCustContactNo();
            imageViewSupportCall.setImageResource(R.drawable.ic_call_green_24dp);
            btnNavigate.setVisibility(View.VISIBLE);
            btnSOS.setVisibility(View.VISIBLE);
            rootCustomerConfirmed.setVisibility(View.VISIBLE);
            floatingActionButton.hide();
            Prefs.setLiveBookingId(getContext(), String.valueOf(currentDetail.getBookingId()));
            Picasso.with(getContext()).load(customerDetails.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(profilePicUserCustomerConfirmed);
            nameUserCustomerConfirmed.setText(customerDetails.getCustFirstName().split(" ")[0]);
            Picasso.with(getContext()).load(currentDetail.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(profilePicCoRiderCustomerConfirmed);
            nameCoRiderCustomerConfirmed.setText(currentDetail.getCustFirstName().split(" ")[0]);

            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_pick_marker_customer);
            Bitmap smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 40), (int) DisplayUtility.dp2px(getContext(), 40), false);
            markerPickCustomer = mMap.addMarker(new MarkerOptions().position(new LatLng(currentDetail.getActualPickLat(),
                    currentDetail.getActualPickLon())).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

            bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_drop_marker_customer);
            smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 40), (int) DisplayUtility.dp2px(getContext(), 40), false);
            markerDropCustomer = mMap.addMarker(new MarkerOptions().position(new LatLng(currentDetail.getActualDropLat(),
                    currentDetail.getActualDropLon())).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
            otp = String.valueOf(currentDetail.getOtp());

            Prefs.setCustomerPickLat(getContext(), String.valueOf(markerPickCustomer.getPosition().latitude));
            Prefs.setCustomerPickLon(getContext(), String.valueOf(markerPickCustomer.getPosition().longitude));
            Prefs.setDriverReachedAtPickLocationTime(getContext(), "");

        }
    }

    private void setCustomerConfirmedRideStartedUI(CurrentDetail currentDetail) {
        if (mMap != null) {
            setCustomerConfirmedRideNotStartedUI(currentDetail);
            rootConfirmedRouteBottom.setVisibility(View.GONE);
            rootRideStartedPickCustomerBottom.setVisibility(View.VISIBLE);
            otp = String.valueOf(currentDetail.getOtp());
        }
    }

    private void setCustomerPickedUI(CurrentDetail currentDetail) {
        if (mMap != null) {
            tool_bar_title.setText("On Trip");
            setCustomerConfirmedRideStartedUI(currentDetail);
            rootConfirmedRouteBottom.setVisibility(View.GONE);
            rootRideStartedPickCustomerBottom.setVisibility(View.GONE);
            btnDrop.setVisibility(View.VISIBLE);
        }
    }

    private void setCustomerDroppedUI(CurrentDetail currentDetail) {
        if (mMap != null) {
//            setRideStartedUI(currentDetail);
        }
    }

    private void setFeedbackUI(CurrentDetail currentDetail) {
        Intent intent=new Intent(getContext(), FeedbackByDriverActivity.class);
        intent.putExtra("currentDetail",currentDetail);
        startActivity(intent);
    }


    @Override
    public boolean onResult(@NonNull String dialogTag, int which, @NonNull Bundle extras) {
        if (dialogTag.equals("dialog_otp")){
            if (which == BUTTON_POSITIVE){
                if (otp.equals(extras.getString("otp"))){
                    BookingService.getInstance().changeRideStatus("2");
                } else {
                    Toast.makeText(getContext(), "please enter valid OTP", Toast.LENGTH_LONG).show();
                    SimpleFormDialog.build()
                            .title("OTP")
                            .fields(
                                    Input.plain("otp").required().hint("OTP").max(4).min(4).inputType(InputType.TYPE_CLASS_NUMBER)
                            )
                            .pos("Ok")
                            .neg("Cancel")
                            .show(HomeFragment.this, "dialog_otp");
                }
            } else if (which == BUTTON_NEGATIVE){
                Toast.makeText(getContext(), "cancelled!", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), "cancelled!", Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        Prefs.setCustomerRequests(getContext(), new JSONArray());
        super.onDestroyView();
    }

    private void hideSecondRouteUI() {
        radioBtnRouteTwo.setVisibility(View.GONE);
        viaTwo.setVisibility(View.GONE);
        colorRouteTwo.setVisibility(View.GONE);
    }

    private void hideThirdRouteUI() {
        radioButtonRouteThree.setVisibility(View.GONE);
        viaThree.setVisibility(View.GONE);
        colorRouteThree.setVisibility(View.GONE);
    }

    private void showSecondRouteUI() {
        radioBtnRouteTwo.setVisibility(View.VISIBLE);
        viaTwo.setVisibility(View.VISIBLE);
        colorRouteTwo.setVisibility(View.VISIBLE);
    }

    private void showThirdRouteUI() {
        radioButtonRouteThree.setVisibility(View.VISIBLE);
        viaThree.setVisibility(View.VISIBLE);
        colorRouteThree.setVisibility(View.VISIBLE);
    }

    private void placeCall(String number) {

        Intent callIntent = new Intent(Intent.ACTION_CALL);

        callIntent.setData(Uri.parse("tel:" + number));


        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startActivity(callIntent);

    }
}
