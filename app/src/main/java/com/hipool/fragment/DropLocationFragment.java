package com.hipool.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.os.ResultReceiver;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.hipool.R;
import com.hipool.SplashActivity;
import com.hipool.adapters.CustomerRequestsRecyclerAdapter;
import com.hipool.adapters.PaymentModeRecyclerAdapter;
import com.hipool.app.BaseFragment;
import com.hipool.app.MyApp;
import com.hipool.collectpayment.CollectPaymentActivity;
import com.hipool.customerhome.HomeActivity;
import com.hipool.directions.Direction;
import com.hipool.driverlist.DriverRequestList;
import com.hipool.feedback.FeedbackByCustomerActivity;
import com.hipool.intentservices.GetPlaceNameFromLatLngIntentService;
import com.hipool.models.CurrentDetail;
import com.hipool.models.CustomerCurrentDetail;
import com.hipool.models.CustomerDetails;
import com.hipool.models.CustomerDropLocation;
import com.hipool.models.CustomerRequest;
import com.hipool.models.DriverDetail;
import com.hipool.models.DriverList;
import com.hipool.models.PaymentMode;
import com.hipool.service.BookingService;
import com.hipool.utils.Constants;
import com.hipool.utils.DisplayUtility;
import com.hipool.utils.HiPoolSocketUtils;
import com.hipool.utils.NetworkUtils;
import com.hipool.utils.Prefs;
import com.hipool.walletactivity.WalletActivity;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.hipool.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import eltos.simpledialogfragment.SimpleDialog;
import eltos.simpledialogfragment.form.Input;
import eltos.simpledialogfragment.form.SimpleFormDialog;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.hipool.service.BookingService.SOCKET_DATA;
import static com.hipool.service.BookingService.SOCKET_DATA_RECIEVED_EVENT;
import static com.hipool.service.BookingService.SOCKET_EVENT;
import static com.hipool.utils.APIConstants.ADD_RIDE_DATA;
import static com.hipool.utils.APIConstants.BASE_URL;
import static com.hipool.utils.APIConstants.CUSTOMER_GURRENT_STATUS;
import static com.hipool.utils.APIConstants.DRIVER_GURRENT_STATUS;
import static com.hipool.utils.APIConstants.GET_RIDE_FARE;
import static com.hipool.utils.APIConstants.GET_WALLET_AMOUNT;
import static com.hipool.utils.Constants.APP_PREFS;
import static com.hipool.utils.Constants.CUST_DETAILS;
import static com.hipool.utils.Constants.IS_DEBUG;
import static com.hipool.utils.Constants.STATUS_CAB_CONFIRMED;
import static com.hipool.utils.Constants.STATUS_DROP_LOCATION_ENTERED;
import static com.hipool.utils.Constants.STATUS_IDLE;
import static com.hipool.utils.Constants.STATUS_RIDE_STARTED;
import static com.hipool.utils.Constants.STATUS_RIDE_STOPPED;


/**
 * Created by Abhinav on 18/09/2017.
 */

public class DropLocationFragment extends BaseFragment implements  LocationListener,
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, OnMapReadyCallback,Direction.DirectionsRouteCommunicator,
PaymentModeRecyclerAdapter.PaymentModeRecyclerCommunicator, HomeActivity.OnBackPressedListener,SimpleDialog.OnDialogResultListener {


    TextView autoCompleteTextViewDrop,autoCompleteTextViewPickup,buttonConfirm,textViewTime,textViewFare;
    View view;
    private AddressResultReceiver mResultReceiver;
    ImageView imageViewLocation,imageViewReverse,imageViewFloatMarker;
    private CardView cardViewGetLocation,cardViewImageForPayment;
    private int SESSION_STATUS=STATUS_IDLE;
    private SupportMapFragment mapFragment;
    private GoogleApiClient mGoogleApiClient;
    int SOURCE_LOCATION_SELECT = 1;
    private Location mLastKnownLocation;
    private String est_fares="0";
    int DROP_LOCATION_SELECT = 2;
    TextView tool_bar_title;
    private GoogleMap mMap;
    RecyclerView recyclerViewShowDriver,recyclerViewPaymentModes;
    private CircleImageView circleImageViewPaymentModeSelected;
    private  int selectedPaymentModeId=0;
    private List<Polyline> polylineList = new ArrayList<>();
    private JSONArray routeArray = new JSONArray();
    private List<Direction.Route> routeListGlobal = new ArrayList<>();
    private RelativeLayout linearLayoutShowMapSelection,relativeLayoutPaymentButton,relativeLayoutCabsearch;
    LatLng latLngPickup,latLngDrop;
    private DriverList driverList;
    List<DriverDetail> driverDetailsArrayList;
    private CustomerRequestsRecyclerAdapter adapter;
    private LinearLayout linearLayoutConfirm;
    private List<PaymentMode> paymentModeList = new ArrayList<>();
    private PaymentModeRecyclerAdapter paymentModeRecyclerAdapter;


    private Marker markerOrigin, markerDest,markerMyLocation,markerPick,markerDrop;
    double pickupLat,pickuplon,dropLat,droplon;
    private int locationSearchType = 1;
    private boolean isGPSEnabled = true;
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10;
    private LocationRequest mLocationRequest;
    CustomerDetails customerDetails;
    private double total_distance=0.0,fare=0.0,discount=0.0,total_fare=0.0,total_tax=0.0;
    private int total_time;
    private String timetext="",distance_text="",driverPhoneno="";
    Handler handler=null;
    TextView nameConfirmedCustomer,mobileConfirmedCustomer,pickLocationConfirmedCustomer,dropLocationConfirmedCustomer;
    TextView textViewDriverName,textViewCABNO, textViewOTP,textViewRideFARE,textViewCustomerName,textViewDrivername1;
    CircleImageView circleImageViewDriverPic,circleImageViewBikePic,circleImageViewCustomerPic,getCircleImageViewDriverPic1;
    private CircleImageView profilePicConfirmedCustomer;
    private  RelativeLayout rootConfirmedCustomer,relativeLayoutCallButton,relativeLayoutCancelButton,relativeLayoutShareRide,relativeLayoutSupport,relativeLayoutShowDriver,relativeLayoutStartRide;
    private boolean toApplyStatus=true;
    private boolean toMapReady=true;
    private RelativeLayout btnSOS;

    public DropLocationFragment()
    {

    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.activity_passenger_home, container, false);

        customerDetails = new Gson().fromJson(getActivity().getSharedPreferences(APP_PREFS, MODE_PRIVATE).getString(CUST_DETAILS, ""), CustomerDetails.class);
        ((HomeActivity) getActivity()).setOnBackPressedListener(this);
        init();

        setPaymentModesRecyclerView();
        relativeLayoutPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                relativeLayoutPaymentButton.setVisibility(View.GONE);
                recyclerViewPaymentModes.setVisibility(View.VISIBLE);
            }
        });
        buildGoogleApiClient();
        cardViewGetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                latLngPickup=new LatLng(Prefs.getMyLat(getContext()),Prefs.getMyLon(getContext()));
               startIntentService(new LatLng(Prefs.getMyLat(getContext()),Prefs.getMyLon(getContext())),"any");
            }
        });
       /* imageViewReverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pickText=autoCompleteTextViewPickup.getText().toString();
                String dropText=autoCompleteTextViewDrop.getText().toString();
                LatLng pickLatLng=latLngPickup;
                LatLng dropLatLng=latLngDrop;
                latLngPickup=dropLatLng;
                latLngDrop=pickLatLng;
                autoCompleteTextViewPickup.setText(dropText);
                autoCompleteTextViewDrop.setText(pickText);
            }
        });*/

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWalletupdate();
            }
        });

        setSourceText();
        setDropText();

        return view;
    }

    @Override
    public void onResume()
    {
        super.onResume();
       /* if(mGoogleApiClient.isConnected() && isGPSEnabled)
        {
            startLocationUpdates();
        }*/
       // super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
                socketDataReciever, new IntentFilter(SOCKET_DATA_RECIEVED_EVENT));

        String prefString = Prefs.getCustomerRequests(getContext());
        try {
            JSONArray requestArray = new JSONArray(prefString);
            if (requestArray.length() > 0) {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        getStatus();
    }

    private void placeCall(String number) {

        Intent callIntent = new Intent(Intent.ACTION_CALL);

        callIntent.setData(Uri.parse("tel:" + number));

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CALL_PHONE}, 1);
        }
        else
            startActivity(callIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==1)
        {
            if(grantResults.length > 0)
            {
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    placeCall(driverPhoneno);
                }
            }


        }
    }

    private BroadcastReceiver socketDataReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                String eventName = intent.getExtras().getString(SOCKET_EVENT);
                if (eventName != null) {
                    String data = intent.getExtras().getString(SOCKET_DATA);
                    if (IS_DEBUG) {
                        Log.v("recieved_event_data", data);
                        Log.v("recieved_event_name", eventName);

                        if (eventName.equals(HiPoolSocketUtils.EVENT_DRIVER_NOTIFICATION)) {

                        }
                        if (eventName.equals(HiPoolSocketUtils.EVENT_CHANGE_RIDE_STATUS)) {
                            // if()
                            if(data.contains("Ride Start"))
                            {
                                showRideStartedUI();
                            }

                            else if(data.contains("Ride Stop"))
                            {
                                SESSION_STATUS=STATUS_RIDE_STOPPED;
                                setDefaultUI();
                                if(!FeedbackByCustomerActivity.isActivityRunning)
                                {
                                    FeedbackByCustomerActivity.isActivityRunning = true;
                                    Intent intent1=new Intent(getContext(), FeedbackByCustomerActivity.class);

                                    startActivity(intent1);
                                }
                            }
                            else if(data.contains("Rider Cancel the ride"))
                            {
                                SESSION_STATUS=STATUS_IDLE;
                                setDefaultUI();

                            }
                        }
                        if (eventName.equals(HiPoolSocketUtils.EVENT_CONFIRM_CUSTOMER_BOOKING)) {
                            if (data.contains("Customer Notified")) {
                                if (data.contains("Customer Notified")) {
                                    String stringDriver=Prefs.getDriverRequests(getActivity());
                                    setDefaultUI();
                                    try {
                                        Prefs.setLiveBookingId(getContext(), new JSONObject(data).getJSONObject("data").getJSONObject("content").getString("insertId"));
                                        JSONObject jsonObject=new JSONObject(stringDriver);
                                        DriverDetail driverDetail=new Gson().fromJson(jsonObject.toString(), DriverDetail.class);

                                        Picasso.with(getContext()).load(driverDetail.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(circleImageViewDriverPic);
                                        Picasso.with(getContext()).load(driverDetail.getVehicleCopy()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(circleImageViewBikePic);
                                        Picasso.with(getContext()).load(customerDetails.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(circleImageViewCustomerPic);
                                        Picasso.with(getContext()).load(driverDetail.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(getCircleImageViewDriverPic1);

                                        textViewDriverName.setText(driverDetail.getCustFirstName());
                                        autoCompleteTextViewDrop.setText(driverDetail.getCustDropLocation());
                                        autoCompleteTextViewPickup.setText(driverDetail.getCustPickLocation());
                                        textViewRideFARE.setText("₹"+Math.round(driverDetail.getCustTotalFare()+driverDetail.getCustTotalTAX()));
                                        textViewOTP.setText("OTP: "+driverDetail.getCustOtp());
                                        driverPhoneno=driverDetail.getCustContactNo();
                                        textViewCABNO.setText(driverDetail.getBikeNo());
                                        textViewDrivername1.setText(driverDetail.getCustFirstName());
                                        textViewCustomerName.setText(customerDetails.getCustFirstName());
                                        setPickDropMarker(driverDetail.getDriverPickLat(),driverDetail.getDriverPickLon(),driverDetail.getDriverDropLat(),driverDetail.getDriverDropLon(),driverDetail.getFullRoute(),driverDetail.getStartIndex(),driverDetail.getEndIndex(),driverDetail.getCustPickLocationLat(),driverDetail.getCustPickLocationLon(),driverDetail.getCustDropLocationLat(),driverDetail.getCustDropLocationLon());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    showRideConrimUI();
                                    if (handler != null) {
                                        handler.removeCallbacks(cabSearchCancelRunnable);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    };

    private void setPickDropMarker(double driverPickLat, double driverPickLon, double driverDropLat, double driverDropLon, String fullRoute, int startIndex, int endIndex, double custPickLocationLat, double custPickLocationLon, double custDropLocationLat, double custDropLocationLon) {

        if(markerPick!=null)
        {
            markerPick.remove();
        }
        Bitmap bm = Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_pick_marker_customer);
        Bitmap smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 40), (int) DisplayUtility.dp2px(getContext(), 40), false);
        markerPick = mMap.addMarker(new MarkerOptions().position(new LatLng(driverPickLat, driverPickLon)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

        if (markerDrop != null) {
            markerDrop.remove();
        }


        bm = Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_drop_marker_customer);
        smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 40), (int) DisplayUtility.dp2px(getContext(), 40), false);
        markerDrop = mMap.addMarker(new MarkerOptions().position(new LatLng(driverDropLat, driverDropLon)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

        if (markerOrigin != null) {
            markerOrigin.remove();
        }
        bm = Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.greenman);
        smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 40), (int) DisplayUtility.dp2px(getContext(), 40), false);
        markerOrigin = mMap.addMarker(new MarkerOptions().position(new LatLng(custPickLocationLat, custPickLocationLon)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

        if (markerDest != null) {
            markerDest.remove();
        }


        bm = Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.redman);
        smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 40), (int) DisplayUtility.dp2px(getContext(), 40), false);
        markerDest = mMap.addMarker(new MarkerOptions().position(new LatLng(custDropLocationLat, custDropLocationLon)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));



        for (Polyline polyline : polylineList) {
            if (polyline != null) {
                polyline.remove();
            }
        }

        polylineList.clear();

        PolylineOptions polylineOptions = new PolylineOptions();
        try {
            JSONArray routeArray = new JSONArray(fullRoute);
            for (int  i = startIndex ; i <= endIndex ; i++ ) {
                JSONObject latLngObject = routeArray.getJSONObject(i);
                polylineOptions.add(new LatLng(latLngObject.getDouble("lat"), latLngObject.getDouble("lon")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        polylineOptions.color(Color.BLUE);
        polylineList.add(mMap.addPolyline(polylineOptions));

        PolylineOptions polylineOptions1 = new PolylineOptions();
        List<LatLng> latLngList = new ArrayList<>();
        latLngList.add(new LatLng(custPickLocationLat, custPickLocationLon));
        latLngList.add(new LatLng(driverPickLat, driverPickLon));
        polylineOptions1.addAll(latLngList);
        polylineOptions1.color(Color.BLACK);
        Polyline polyline = mMap.addPolyline(polylineOptions1);
        polyline.setPattern(Arrays.<PatternItem>asList(new Dot(), new Gap(10)));
        polylineList.add(polyline);

        PolylineOptions polylineOptions2 = new PolylineOptions();
        latLngList.clear();
        latLngList.add(new LatLng(custDropLocationLat, custDropLocationLon));
        latLngList.add(new LatLng(driverDropLat, driverDropLon));
        polylineOptions2.addAll(latLngList);
        polylineOptions2.color(Color.BLACK);
        Polyline polyline2 = mMap.addPolyline(polylineOptions2);
        polyline2.setPattern(Arrays.<PatternItem>asList(new Dot(), new Gap(10)));
        polylineList.add(polyline2);
    }

    private void showRideConrimUI() {
        tool_bar_title.setText("Booking Confirm");
            SESSION_STATUS=STATUS_CAB_CONFIRMED;
            recyclerViewShowDriver.setVisibility(View.GONE);
            relativeLayoutCabsearch.setVisibility(View.GONE);
            linearLayoutShowMapSelection.setVisibility(View.VISIBLE);
            autoCompleteTextViewDrop.setClickable(false);
            autoCompleteTextViewPickup.setClickable(false);
            linearLayoutConfirm.setVisibility(View.GONE);
        relativeLayoutShowDriver.setVisibility(View.VISIBLE);
        relativeLayoutStartRide.setVisibility(View.GONE);
    }

    private Runnable cabSearchCancelRunnable = new Runnable() {
        @Override
        public void run() {
            if (!Prefs.getLiveBookingId(getContext()).equals(""))
            {
                relativeLayoutCabsearch.setVisibility(View.GONE);
                recyclerViewShowDriver.setVisibility(View.VISIBLE);
                sendCustLocation();
            }

        }
    };

    private void sendCustLocation() {
        if(!NetworkUtils.isInternetConnected(getContext()))
            return;
        final MaterialDialog materialDialog = new MaterialDialog.Builder(getActivity())
                .title("Getting Rider List")
                .content("Please wait ..")
                .progress(true, 0)
                .cancelable(false)
                .show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + ADD_RIDE_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("Response",response);
                if (response.contains("Driver found")) {
                    driverList=null;
                    driverList=new Gson().fromJson(response, DriverList.class);
                    List<CustomerDropLocation> customerDropLocations=driverList.getData();
                    driverDetailsArrayList.clear();
                    for(int i=0;i<customerDropLocations.size();i++)
                    {
                        driverDetailsArrayList.add(customerDropLocations.get(i).getDriverDetail());

                    }
                    adapter = new CustomerRequestsRecyclerAdapter(driverDetailsArrayList, null, getContext());
                    recyclerViewShowDriver.setAdapter(adapter);
                    materialDialog.dismiss();
                    showDriverListUI();
                } else {
                    materialDialog.dismiss();
                    Toast.makeText(getContext(),"Sorry, No Driver on this route. Please try after some time or change your pickup location",Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                materialDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                final Map<String, String> params = new HashMap<>();
                params.put("cust_id", String.valueOf(customerDetails.getCustId()));
                params.put("current_lat", String.valueOf(Prefs.getMyLat(getContext())));
                params.put("current_lon", String.valueOf(Prefs.getMyLon(getContext())));
                params.put("pickup_lat", String.valueOf(latLngPickup.latitude));
                params.put("pickup_lon", String.valueOf(latLngPickup.longitude));
                params.put("drop_lat", String.valueOf(latLngDrop.latitude));
                params.put("drop_lon", String.valueOf(latLngDrop.longitude));
                params.put("pick_address",autoCompleteTextViewPickup.getText().toString());
                params.put("drop_address",autoCompleteTextViewDrop.getText().toString());
                params.put("total_distance",String.valueOf(total_distance));
                params.put("est_fare",String.valueOf(fare));
                params.put("total_tax",String.valueOf(total_tax));
                params.put("total_time",String.valueOf(total_time));
                params.put("orderId","OTOTRIP"+Utils.getUnixTime());
                Log.e("Param",params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void getWalletupdate() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Checking account balance");
        progressDialog.setMessage("please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + GET_WALLET_AMOUNT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("data");
                    JSONObject jsonObject1=jsonArray.getJSONObject(0);
                    if(total_fare>Double.valueOf(jsonObject1.getString("credit")))
                    {
                        SimpleFormDialog.build()
                                .title("Account Alert")
                                .icon(R.drawable.ic_cancel_red_24dp)
                                .msg("You have low balance for this ride Please recharge your account.")
                                .pos("Ok")
                                .neg("Cancel")
                                .show(DropLocationFragment.this, "Account Alert");
                    }
                    else
                    {
                        sendCustLocation();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("updateCustPay", error.toString());
                progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id",String.valueOf(customerDetails.getCustId()));
                Log.v("paymentParams", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }

    private void showDriverListUI() {
        tool_bar_title.setText("Rider List");
        linearLayoutShowMapSelection.setVisibility(View.GONE);
        recyclerViewShowDriver.setVisibility(View.VISIBLE);
    }

    protected synchronized void buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(), this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        //createLocationRequest();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

        if (BookingService.getInstance() == null) {
            getActivity().startService(new Intent(getContext(), BookingService.class));
        }
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    private void setDropText() {
        autoCompleteTextViewDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    toApplyStatus = false;
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(new AutocompleteFilter.Builder().setCountry("IN").build())
                                    .build(getActivity());
                    startActivityForResult(intent, DROP_LOCATION_SELECT);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
            }
        });


        /*autoCompleteTextViewDrop.setAdapter(new PlacesAutoCompleteAdapter(getActivity(),R.layout.item_autocomplete_places));
        autoCompleteTextViewDrop.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //startIntentService(autoCompleteTextViewPickup.getText().toString(), "any");
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(autoCompleteTextViewDrop.getWindowToken(), 0);
                    }
                }).run();
            }
        });*/
    }



    private void setSourceText() {
        autoCompleteTextViewPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    toApplyStatus = false;
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(new AutocompleteFilter.Builder().setCountry("IN").build())
                                    .build(getActivity());
                    startActivityForResult(intent, SOURCE_LOCATION_SELECT);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
            }
        });



        /*autoCompleteTextViewPickup.setAdapter(new PlacesAutoCompleteAdapter(getActivity(),R.layout.item_autocomplete_places));
        autoCompleteTextViewPickup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //startIntentService(autoCompleteTextViewPickup.getText().toString(), "any");
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(autoCompleteTextViewPickup.getWindowToken(), 0);
                    }
                }).run();
            }
        });*/

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SOURCE_LOCATION_SELECT) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                Log.e("", "Place: " + place.getName());
                autoCompleteTextViewPickup.setText(place.getAddress());
                latLngPickup=place.getLatLng();
                setOriginLocation(latLngPickup.latitude,latLngPickup.longitude);
                if(!autoCompleteTextViewPickup.getText().equals("") && !autoCompleteTextViewDrop.getText().equals(""))
                    new Direction(DropLocationFragment.this, markerOrigin.getPosition(), markerDest.getPosition(),"").execute();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                // TODO: Handle the error.
                Log.e("", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {

            }
        }

        else if (requestCode == DROP_LOCATION_SELECT) {
            if (resultCode == RESULT_OK) {
                SESSION_STATUS=STATUS_DROP_LOCATION_ENTERED;
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                Log.e("", "Place: " + place.getName());
                autoCompleteTextViewDrop.setText(place.getAddress());
                latLngDrop=place.getLatLng();
                setMarkerDest(latLngDrop.latitude,latLngDrop.longitude);
                if(!autoCompleteTextViewPickup.getText().equals("") && !autoCompleteTextViewDrop.getText().equals(""))
                    new Direction(DropLocationFragment.this, markerOrigin.getPosition(), markerDest.getPosition(),"").execute();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                // TODO: Handle the error.
                Log.e("", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {

            }
        }

    }

    private void init() {
       // latLng.= Prefs.getMyLat(getContext());
        tool_bar_title=(TextView)getActivity().findViewById(R.id.toolbar_title);
        rootConfirmedCustomer = (RelativeLayout) view.findViewById(R.id.root_confirmed_customer_fragment_home);
        profilePicConfirmedCustomer = (CircleImageView) view.findViewById(R.id.profile_pic_confirmed_customer_fragment_home);
        nameConfirmedCustomer = (TextView) view.findViewById(R.id.name_confirmed_customer_fragment_home);
        pickLocationConfirmedCustomer = (TextView) view.findViewById(R.id.pick_location_confirmed_customer_fragment_home);
        dropLocationConfirmedCustomer = (TextView) view.findViewById(R.id.drop_location_confirmed_customer_fragment_home);
        mobileConfirmedCustomer = (TextView) view.findViewById(R.id.mobile_confirmed_customer_fragment_home);
        driverDetailsArrayList =new ArrayList<>();
        linearLayoutShowMapSelection=(RelativeLayout)view.findViewById(R.id.cust_home_main_layout);
        recyclerViewShowDriver=(RecyclerView)view.findViewById(R.id.recycler_content_rider_requests);
        recyclerViewShowDriver.setVisibility(View.GONE);
        recyclerViewShowDriver.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewShowDriver.setItemAnimator(new DefaultItemAnimator());
        recyclerViewPaymentModes=(RecyclerView)view.findViewById(R.id.recycler_payment_mode_selection_content_book_ride);
        buttonConfirm=(TextView)view.findViewById(R.id.confirm_button_fragment_home);
        latLngPickup=new LatLng(Prefs.getMyLat(getContext()),Prefs.getMyLon(getContext()));
        mResultReceiver=new AddressResultReceiver(new Handler());
        autoCompleteTextViewPickup=(TextView) view.findViewById(R.id.pick_textview);
        autoCompleteTextViewDrop=(TextView) view.findViewById(R.id.drop_textview);
        cardViewGetLocation=(CardView) view.findViewById(R.id.get_current_location);
        relativeLayoutPaymentButton=(RelativeLayout)view.findViewById(R.id.relative_root_item_payment_mode);
        imageViewFloatMarker=(ImageView)view.findViewById(R.id.imageview_floating_marker_content_book_ride);
        circleImageViewPaymentModeSelected=(CircleImageView)view.findViewById(R.id.image_payment_mode_item_payment_mode);
        relativeLayoutPaymentButton.setVisibility(View.GONE);
        linearLayoutConfirm=(LinearLayout)view.findViewById(R.id.linearLayout_time_fare_proceed);
        textViewFare=(TextView)view.findViewById(R.id.textViewFare);
        textViewTime=(TextView)view.findViewById(R.id.textViewTime);
        linearLayoutConfirm.setVisibility(View.GONE);

        relativeLayoutCabsearch=(RelativeLayout)view.findViewById(R.id.relative_root_cab_search_layout);

        relativeLayoutCallButton=(RelativeLayout)view.findViewById(R.id.btn_call_drop_selection);
        relativeLayoutCancelButton=(RelativeLayout)view.findViewById(R.id.btn_cancel_fragment_home);
        relativeLayoutShowDriver=(RelativeLayout)view.findViewById(R.id.root_customer_confirmed_layout);
        relativeLayoutShareRide=(RelativeLayout)view.findViewById(R.id.btn_share_ride);
        relativeLayoutSupport=(RelativeLayout)view.findViewById(R.id.btn_support_fragment_home);
        relativeLayoutStartRide=(RelativeLayout)view.findViewById(R.id.root_customer_confirmed_fragment_home);

        circleImageViewDriverPic=(CircleImageView)view.findViewById(R.id.profile_pic_driver);
        circleImageViewBikePic=(CircleImageView)view.findViewById(R.id.bike_pic);
        circleImageViewCustomerPic=(CircleImageView)view.findViewById(R.id.profile_pic_user_customer_confirmed_fragment_home);
        getCircleImageViewDriverPic1=(CircleImageView)view.findViewById(R.id.profile_pic_user_customer_confirmed_fragment_home);

        textViewDriverName=(TextView)view.findViewById(R.id.textViewDriverName);
        textViewRideFARE=(TextView)view.findViewById(R.id.textViewShowFare);
        textViewCABNO=(TextView)view.findViewById(R.id.textViewVechileNo);
        textViewOTP=(TextView)view.findViewById(R.id.textViewOtp);

        btnSOS = (RelativeLayout) view.findViewById(R.id.sos_btn_fragment_home);
        btnSOS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(getContext())
                        .content("This SOS will also be informed to police, continue?")
                        .positiveText("Confirm")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                BookingService.getInstance().sos("1");
                                Toast.makeText(getContext(), "sos started..", Toast.LENGTH_LONG).show();
                            }
                        })
                        .negativeText("Cancel")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
        btnSOS.setVisibility(View.GONE);

        textViewCustomerName=(TextView)view.findViewById(R.id.name_user_customer_confirmed_fragment_home);
        textViewDrivername1=(TextView)view.findViewById(R.id.name_corider_customer_confirmed_home_fragment);


        relativeLayoutCallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeCall(driverPhoneno);
                //placeCall("9650072195");
            }
        });
        relativeLayoutCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                BookingService.getInstance().changeRideStatus("8");
                setDefaultUI();
            }
        });


    }

    private void setDefaultUI() {
        if (mMap != null) {


            btnSOS.setVisibility(View.GONE);
            relativeLayoutStartRide.setVisibility(View.GONE);
            relativeLayoutShowDriver.setVisibility(View.GONE);
            autoCompleteTextViewDrop.setClickable(true);
            autoCompleteTextViewPickup.setClickable(true);
            autoCompleteTextViewDrop.setText("Enter destination");
            toMapReady=true;
            resetMap();
        }
    }
    private void resetMap() {
        for (Polyline polyline : polylineList) {
            if (polyline != null) {
                polyline.remove();
            }
        }
        polylineList.clear();

        if (markerDest != null) {
            markerDest.remove();
        }
        if(markerPick!=null)
        {
            markerPick.remove();
        }
        if(markerDrop!=null)
            markerDrop.remove();


        markerDest = null;


        updateCamera();
    }



    protected void startIntentService(LatLng latLng, String which) {


        Intent intent = new Intent(getActivity(), GetPlaceNameFromLatLngIntentService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(Constants.RECEIVER, mResultReceiver);

        intent.putExtra("Lat",String.valueOf(latLng.latitude));
        intent.putExtra("Lon", String.valueOf(latLng.longitude));


        intent.putExtra(Constants.LOCATION_DATA_WHICH_EXTRA, which);
        getActivity().startService(intent);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(
                socketDataReciever);
        super.onPause();
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,this);
    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        if (mLastKnownLocation == null) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mLastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
        //startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if(toMapReady)
        {
            setMyLocationMarker(Prefs.getMyLat(getContext()), Prefs.getMyLon(getContext()));
            setOriginLocation(Prefs.getMyLat(getContext()), Prefs.getMyLon(getContext()));
            toMapReady=false;
        }
        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                if(SESSION_STATUS==STATUS_IDLE || SESSION_STATUS==STATUS_DROP_LOCATION_ENTERED)
                {
                    if(locationSearchType!=0)
                    {

                        if (locationSearchType == 1) {
                            latLngPickup = mMap.getCameraPosition().target;
                        }
                        else if(locationSearchType == 2) {
                            latLngDrop=mMap.getCameraPosition().target;
                        }
                    }
                }



            }
        });
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
               // startIntentService(latLngPickup, "pick_from_marker");
                if(SESSION_STATUS==STATUS_IDLE || SESSION_STATUS==STATUS_DROP_LOCATION_ENTERED)
                {
                    if(locationSearchType!=0)
                    {
                        if (locationSearchType == 1) {


                            startIntentService(latLngPickup, "pick_from_marker");
                        } else if (locationSearchType == 2) {
                            startIntentService(latLngDrop, "drop_from_marker");

                        }
                        imageViewFloatMarker.setVisibility(View.GONE);
                        locationSearchType = 0;
                    }
                }


            }
        });
       // setMyLocationMarker(latLngPickup.latitude,latLngPickup.longitude);
        //setOriginLocation(latLngPickup.latitude,latLngPickup.longitude);


        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if(SESSION_STATUS==STATUS_IDLE || SESSION_STATUS==STATUS_DROP_LOCATION_ENTERED)
                {
                    if(locationSearchType==0)
                    {
                        try {
                            if ((int) marker.getTag() == 1) {
                                imageViewFloatMarker.setVisibility(View.VISIBLE);
                                imageViewFloatMarker.setImageDrawable(getResources().getDrawable(R.drawable.img_pick_marker_float));
                                locationSearchType = 1;
                            } else if ((int) marker.getTag() == 2) {
                                imageViewFloatMarker.setVisibility(View.VISIBLE);
                                imageViewFloatMarker.setImageDrawable(getResources().getDrawable(R.drawable.img_drop_marker_float));
                                locationSearchType = 2;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return true;
                    }
                }



                return false;
            }
        });
    }
    private void setMyLocationMarker(double lat, double lon) {
        if (markerMyLocation != null) {
            markerMyLocation.remove();
        }
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_my_location);
        Bitmap smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 12), (int) DisplayUtility.dp2px(getContext(), 15), false);
        markerMyLocation = mMap.addMarker(new MarkerOptions().position(new LatLng(lat,
                lon)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));

        if (markerOrigin == null && markerDest == null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    markerMyLocation.getPosition(), 15));
        }
    }

    private void setOriginLocation(double lat, double lon) {
//        originInput.setText(place);
        if (markerOrigin != null) {
            markerOrigin.remove();
        }

        Bitmap bm = Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_origin_location_96dp);
        Bitmap smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 40), (int) DisplayUtility.dp2px(getContext(), 40), false);
        markerOrigin = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
        markerOrigin.setTag(1);
        if (markerDest == null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    markerOrigin.getPosition(), 15));
        } else {
            updateCamera();
        }
    }

    private void setPickUpLocation(double lat, double lon) {
//        originInput.setText(place);
        if (markerPick != null) {
            markerPick.remove();
        }

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.img_pick_marker);
        Bitmap smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(),20), (int) DisplayUtility.dp2px(getContext(),20), false);
        MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(lat, lon)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
        markerPick = mMap.addMarker(markerOptions);
        markerPick.setTag(1);
        if (markerPick == null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    markerPick.getPosition(), 20));
        }
        //updateCamera();
    }

    private void setDropLocation(double lat, double lon) {
//        originInput.setText(place);
        if (markerDrop != null) {
            markerDrop.remove();
        }

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.img_pick_marker);
        Bitmap smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(),20), (int) DisplayUtility.dp2px(getContext(),20), false);
        MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(lat, lon)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
        markerDrop = mMap.addMarker(markerOptions);
        markerDrop.setTag(1);
        if (markerDrop == null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    markerDrop.getPosition(), 20));
        } else {
            updateCamera();
        }
        //updateCamera();
    }


    private void setMarkerDest(double lat, double lon) {
//        originInput.setText(place);
        if (markerDest != null) {
            markerDest.remove();
        }


        Bitmap bm = Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_dest_location_96dp);
        Bitmap smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 40), (int) DisplayUtility.dp2px(getContext(), 40), false);
        markerDest = mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
        markerDest.setTag(2);
        if (markerOrigin == null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    markerDest.getPosition(), 15));
        } else {
            updateCamera();
        }
        //updateCamera();
    }


    private void updateCamera() {
        //Calculate the markers to get their position
        List<Marker> markers = new ArrayList<>();
        LatLngBounds.Builder b = new LatLngBounds.Builder();
        if (markerOrigin != null) {
            markers.add(markerOrigin);
        }

        if (markerDest != null) {
            markers.add(markerDest);
        }
        for (Marker m : markers) {
            b.include(m.getPosition());
        }
        LatLngBounds bounds = b.build();
        //Change the padding as per needed
        if (markers.size() > 1) {
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 150);
            mMap.animateCamera(cu);
        } else {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    markerOrigin.getPosition(), 15));
        }


//        getTravelDistanceTime();
    }



    @Override
   public void onLocationChanged(Location location) {
       /* mLastKnownLocation = location;
        if (markerMyLocation != null) {
            markerMyLocation.remove();
        }
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_my_location);
        Bitmap smallMarker = Bitmap.createScaledBitmap(bm, (int) DisplayUtility.dp2px(getContext(), 4), (int) DisplayUtility.dp2px(getContext(), 4), false);
        markerMyLocation = mMap.addMarker(new MarkerOptions().position(new LatLng(mLastKnownLocation.getLatitude(),
                mLastKnownLocation.getLongitude())).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));*/

    }

    @Override
    public void onRoutesReady(List<Direction.Route> routeList) {
        routeListGlobal = routeList;

        for (Polyline polyline : polylineList) {
            if (polyline != null) {
                polyline.remove();
            }
        }
        polylineList.clear();

        //routeSelected = 0;

        polylineList.add(mMap.addPolyline(routeList.get(0).getPolylineOptions()));
        if (markerOrigin != null && markerDest != null) {
            List<Marker> markers = new ArrayList<>();
            markers.add(markerOrigin);
            markers.add(markerDest);
            LatLngBounds.Builder b = new LatLngBounds.Builder();
            for (Marker m : markers) {
                b.include(m.getPosition());
            }
            LatLngBounds bounds = b.build();
            //Change the padding as per needed
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 150);
            mMap.animateCamera(cu);
        }
        if(SESSION_STATUS==STATUS_IDLE || SESSION_STATUS==STATUS_DROP_LOCATION_ENTERED)
        {
            total_distance=routeList.get(0).getDistance();
            timetext=routeList.get(0).getTimeText();
            total_time=routeList.get(0).getTime();
            distance_text=routeList.get(0).getDistanceText();
        }
        getFare(routeList.get(0).getDistance());

    }

    private void getFare(final double totalDistance) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + GET_RIDE_FARE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("getFare", "getFare" + response);
                if (response.contains("Success")){
                    try {
                        JSONObject jsonObject=new JSONObject(response);
                        est_fares =jsonObject.getString("Fare");
                        fare=Double.valueOf(est_fares);
                        total_tax=Double.valueOf(jsonObject.getString("Tax"));
                        total_fare=fare+total_tax;
                        textViewTime.setText("Estimated time: "+timetext);
                        textViewFare.setText("Estimated Fare: ₹"+Math.round(total_fare));
                        linearLayoutConfirm.setVisibility(View.VISIBLE);
                        //relativeLayoutPaymentButton.setVisibility(View.VISIBLE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.contains("Customer Current Details Not Coming")) {
                    toApplyStatus=true;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("getFare", "getFare" + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("distance", String.valueOf(totalDistance));
                Log.v("getFare", "params" + params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);

    }

    private void setPaymentModesRecyclerView() {
        paymentModeList.clear();
//        paymentModeList.add(new PaymentMode(2, "PhonePe", R.drawable.ic_phonepe_logo));
        paymentModeList.add(new PaymentMode(1, "Cash", R.drawable.img_rupee_icon_payment));
        paymentModeList.add(new PaymentMode(3, "paytm", R.drawable.ic_paytm));
        paymentModeRecyclerAdapter = new PaymentModeRecyclerAdapter(paymentModeList,DropLocationFragment.this);
        recyclerViewPaymentModes.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewPaymentModes.setItemAnimator(new DefaultItemAnimator());
        recyclerViewPaymentModes.setAdapter(paymentModeRecyclerAdapter);
    }



    @Override
    public void onPaymentModeSelected(int position, int paymentModeId) {
        selectedPaymentModeId = paymentModeId;
        recyclerViewPaymentModes.setVisibility(View.GONE);
        relativeLayoutPaymentButton.setVisibility(View.VISIBLE);
        circleImageViewPaymentModeSelected.setImageResource(paymentModeList.get(position).getImgResource());
    }

    @Override
    public void doBack() {
        if(recyclerViewShowDriver.getVisibility()==View.VISIBLE)
        {
            recyclerViewShowDriver.setVisibility(View.GONE);
            linearLayoutShowMapSelection.setVisibility(View.VISIBLE);
        }

        else if(linearLayoutConfirm.getVisibility()==View.VISIBLE)
        {
            for (Polyline polyline : polylineList) {
                if (polyline != null) {
                    polyline.remove();
                }
            }
            polylineList.clear();
            autoCompleteTextViewDrop.setText("");
            markerDest.remove();
            relativeLayoutPaymentButton.setVisibility(View.GONE);
            linearLayoutConfirm.setVisibility(View.GONE);
        }else
            getActivity().finish();


    }

    @Override
    public boolean onResult(@NonNull String dialogTag, int which, @NonNull Bundle extras) {
        if (dialogTag.equals("Account ALert")){
            if (which == BUTTON_POSITIVE){
                 getActivity().startActivity(new Intent(getActivity(), WalletActivity.class));
            } else if (which == BUTTON_NEGATIVE){
                Toast.makeText(getContext(), "cancelled!", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), "cancelled!", Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return false;
    }


    private class AddressResultReceiver extends ResultReceiver {
        private AddressResultReceiver(Handler handler) {
            super(handler);
        }



        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
            Log.v("getAdd", resultData.getString(Constants.RESULT_DATA_KEY));

            String add = resultData.getString(Constants.RESULT_DATA_KEY);

            String which = resultData.getString(Constants.RESULT_WHICH_KEY);
            //set address
            if (which != null) {
                if(which.equals("pick_from_marker"))
                {
                    autoCompleteTextViewPickup.setText(add);
                    setOriginLocation(latLngPickup.latitude,latLngPickup.longitude);
                    if(!autoCompleteTextViewPickup.getText().equals("") && !autoCompleteTextViewDrop.getText().equals(""))
                        try{
                            new Direction(DropLocationFragment.this, markerOrigin.getPosition(), markerDest.getPosition(),"").execute();
                        }catch (Exception e)
                        {

                        }


                }
                else if(which.equals("any"))
                {
                    autoCompleteTextViewPickup.setText(add);
                    setOriginLocation(latLngPickup.latitude,latLngPickup.longitude);
                    if(!autoCompleteTextViewPickup.getText().equals("") && !autoCompleteTextViewDrop.getText().equals(""))
                        try{
                            new Direction(DropLocationFragment.this, markerOrigin.getPosition(), markerDest.getPosition(),"").execute();
                        }catch (Exception e)
                        {

                        }
                }
                else if(which.equals("drop_from_marker"))
                {
                    autoCompleteTextViewDrop.setText(add);
                    setMarkerDest(latLngDrop.latitude,latLngDrop.longitude);
                    if(!autoCompleteTextViewPickup.getText().equals("") && !autoCompleteTextViewDrop.getText().equals(""))
                        try{
                            new Direction(DropLocationFragment.this, markerOrigin.getPosition(), markerDest.getPosition(),"").execute();
                        }catch (Exception e)
                        {

                        }

                }

            }

        }
    }
    private void getStatus() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + CUSTOMER_GURRENT_STATUS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("getStatus", "getStatus" + response);
                if (response.contains("Success")){
                    if (toApplyStatus){
                        JSONObject jsonObject= null;
                        try {
                            jsonObject = new JSONObject(response);
                            CustomerCurrentDetail customerCurrentDetail=new Gson().fromJson(jsonObject.getJSONObject("data").toString(), CustomerCurrentDetail.class);
                            int booking_status=new JSONObject(response).getInt("booking_status");
                            if(customerCurrentDetail.getBookingStatus()==1)
                            {

                                showRideConrimUI();
                                autoCompleteTextViewPickup.setText(customerCurrentDetail.getCustPickAddress());
                                autoCompleteTextViewDrop.setText(customerCurrentDetail.getCustDropAddress());
                                Prefs.setLiveBookingId(getContext(), String.valueOf(customerCurrentDetail.getBookingId()));
                                Picasso.with(getContext()).load(customerCurrentDetail.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(circleImageViewDriverPic);
                                Picasso.with(getContext()).load(customerCurrentDetail.getVehicleCopy()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(circleImageViewBikePic);
                                Picasso.with(getContext()).load(customerCurrentDetail.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(getCircleImageViewDriverPic1);
                                Picasso.with(getContext()).load(customerDetails.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(circleImageViewCustomerPic);

                                textViewDriverName.setText(customerCurrentDetail.getCustFirstName().split(" ")[0]);
                                textViewRideFARE.setText("₹"+Math.round( customerCurrentDetail.getEstimatedFare()+customerCurrentDetail.getEstimatedFareTax()));
                                textViewOTP.setText("OTP: "+customerCurrentDetail.getOtp());
                                driverPhoneno=customerCurrentDetail.getCustContactNo();
                                textViewCustomerName.setText(customerDetails.getCustFirstName());
                                textViewDrivername1.setText(customerCurrentDetail.getCustFirstName());
                                textViewCABNO.setText(customerCurrentDetail.getBikeNo());

                                setPickDropMarker(customerCurrentDetail.getActualPickLat(),customerCurrentDetail.getActualPickLon(),customerCurrentDetail.getActualDropLat(),customerCurrentDetail.getActualDropLon(),customerCurrentDetail.getFullRoute(),customerCurrentDetail.getStartIndex(),customerCurrentDetail.getEndIndex(),customerCurrentDetail.getCustEnterPickLan(),customerCurrentDetail.getCustEnterPickLon(),customerCurrentDetail.getCustEnterDropLan(),customerCurrentDetail.getCustEnterDropLon());



                            }
                            else if(customerCurrentDetail.getBookingStatus()==2)
                            {
                                showRideStartedUI();
                                Prefs.setLiveBookingId(getContext(), String.valueOf(customerCurrentDetail.getBookingId()));
                                Picasso.with(getContext()).load(customerCurrentDetail.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(circleImageViewDriverPic);
                                Picasso.with(getContext()).load(customerCurrentDetail.getVehicleCopy()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(circleImageViewBikePic);
                                Picasso.with(getContext()).load(customerCurrentDetail.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(getCircleImageViewDriverPic1);
                                Picasso.with(getContext()).load(customerDetails.getCustProfilePic()).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.userimage).error(R.drawable.userimage).into(circleImageViewCustomerPic);

                                textViewCustomerName.setText(customerDetails.getCustFirstName());
                                textViewDrivername1.setText(customerCurrentDetail.getCustFirstName());
                                textViewDriverName.setText(customerCurrentDetail.getCustFirstName().split(" ")[0]);
                                textViewRideFARE.setText("₹"+customerCurrentDetail.getEstimatedFare());
                                textViewOTP.setText("OTP: "+customerCurrentDetail.getOtp());
                                textViewCABNO.setText(customerCurrentDetail.getBikeNo());
                                autoCompleteTextViewPickup.setText(customerCurrentDetail.getCustPickAddress());
                                autoCompleteTextViewDrop.setText(customerCurrentDetail.getCustDropAddress());
                                //setOriginLocation(customerCurrentDetail.getCustEnterPickLan(),customerCurrentDetail.getCustEnterPickLon());
                                //setMarkerDest(customerCurrentDetail.getCustEnterDropLan(),customerCurrentDetail.getCustEnterDropLon());
                                setPickDropMarker(customerCurrentDetail.getActualPickLat(),customerCurrentDetail.getActualPickLon(),customerCurrentDetail.getActualDropLat(),customerCurrentDetail.getActualDropLon(),customerCurrentDetail.getFullRoute(),customerCurrentDetail.getStartIndex(),customerCurrentDetail.getEndIndex(),customerCurrentDetail.getCustEnterPickLan(),customerCurrentDetail.getCustEnterPickLon(),customerCurrentDetail.getCustEnterDropLan(),customerCurrentDetail.getCustEnterDropLon());
                                //new Direction(DropLocationFragment.this, markerOrigin.getPosition(), markerDest.getPosition(),"").execute();

                            }
                            else if(booking_status==6)
                            {
                                startActivity(new Intent(getActivity(), FeedbackByCustomerActivity.class));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    toApplyStatus=true;


                } else if (response.contains("Customer Current Details Not Coming")) {
                    toApplyStatus=true;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                toApplyStatus=true;
                Log.e("getStatus", "getStatus" + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cust_id", String.valueOf(customerDetails.getCustId()));
                params.put("customer_access_token", customerDetails.getCustomerAccessToken());
                Log.v("getStatus", "params" + params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MyApp.getInstance().addToRequestQueue(stringRequest);
    }




    private void showRideStartedUI() {
        tool_bar_title.setText("On Trip");
        SESSION_STATUS=STATUS_RIDE_STARTED;
        recyclerViewShowDriver.setVisibility(View.GONE);
        relativeLayoutCabsearch.setVisibility(View.GONE);
        linearLayoutShowMapSelection.setVisibility(View.VISIBLE);
        autoCompleteTextViewDrop.setClickable(false);
        autoCompleteTextViewPickup.setClickable(false);
        linearLayoutConfirm.setVisibility(View.GONE);
        relativeLayoutStartRide.setVisibility(View.VISIBLE);
        relativeLayoutShowDriver.setVisibility(View.GONE);

        btnSOS.setVisibility(View.VISIBLE);
    }


}
